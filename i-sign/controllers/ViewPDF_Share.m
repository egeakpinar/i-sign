//
//  ViewPDF_Share.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 16/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ViewPDF_Share.h"
#import "FileExplorer.h"
#import "Debug.h"
#import "Helper.h"
#import "OLGhostAlertView.h"
#import "BDKNotifyHUD.h"

#import <MessageUI/MFMailComposeViewController.h>

@interface ViewPDF_Share ()<UIActionSheetDelegate , UIAlertViewDelegate, MFMailComposeViewControllerDelegate>  {
    NSMutableArray *arr_root_folders;
    UIActionSheet *action_sheet_save;
    UIAlertView *alert_new_folder;
}

@property(nonatomic, retain) ID_PDFView *view_pdf;
@property(nonatomic, retain) NSData *pdf_output_digital;
@property(nonatomic, retain) ViewPDFVC *vc_parent;

@end

@implementation ViewPDF_Share

+ (ViewPDF_Share *) sharedWithParent:(ViewPDFVC *)vc_parent {
    static ViewPDF_Share *_instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [[ViewPDF_Share alloc] init];
    });
    _instance.vc_parent = vc_parent;
    return _instance;
}

- (void) btnSaveLocalTouchWithPDFView:(ID_PDFView *)pdf_view PDFBuffer:(NSData *)buffer_output_digital    {
    assert(pdf_view);
    _view_pdf = pdf_view;
    _pdf_output_digital = buffer_output_digital;
    arr_root_folders = [[FileExplorer shared] getInodesUnderFolder:nil foldersOnly:YES];
    
    // Remove samples folder, user shouldn't be able to save into it
    Inode *inode_to_remove = nil;
    for(Inode *inode in arr_root_folders)   {
        if([inode.name isEqualToString:SAMPLES_FOLDER_NAME])    {
            inode_to_remove = inode;
        }
    }
    [arr_root_folders removeObject:inode_to_remove];

    // Note: Cancel button should be at the top so it's always visible
    // Workaround: Apparently there's an Apple bug here which messes up cancelButtonIndex. Add cancel button as the last button
    action_sheet_save = [[UIActionSheet alloc] initWithTitle:@"Save location" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];

    // Add 'New folder'
    [action_sheet_save addButtonWithTitle:NSLocalizedString(@"New folder...", @"New folder option in action sheet")];    
    // Add 'Documents'
    [action_sheet_save addButtonWithTitle:NSLocalizedString(@"Documents", @"Documents folder under root")];
    if(arr_root_folders)    {
        for(Inode *inode in arr_root_folders)   {
            [action_sheet_save addButtonWithTitle:inode.name];
        }
    }
    // Add 'Cancel' button as the last item (due to an Apple bug)
    if(!IS_IPAD) {
        action_sheet_save.cancelButtonIndex = [action_sheet_save addButtonWithTitle:NSLocalizedString(@"Cancel", @"Default title for 'Cancel' in UIActionSheet")];
    }
    [action_sheet_save showFromToolbar:_vc_parent.navigationController.toolbar];
}


- (void) btnEmailTouchWithPDFView:(ID_PDFView *)pdf_view PDFBuffer:(NSData *)buffer_output_digital    {
    _view_pdf = pdf_view;
    _pdf_output_digital = buffer_output_digital;
    if([MFMailComposeViewController canSendMail])   {
        // Yes, email it
        MFMailComposeViewController *vc_mail = [[MFMailComposeViewController alloc] init];
        [vc_mail setMailComposeDelegate:self];
        
        // Populate mail message
        // Get PDF
        NSData *data_pdf = nil;
        if(_pdf_output_digital) {
            data_pdf = _pdf_output_digital;
        }
        else    {
            data_pdf = [_view_pdf getSignedPDF];
        }
        
        if(!data_pdf)   {
            [Helper displayMessageWithTitle:NSLocalizedString(@"Hmm", @"Title for warning message alerts") message:NSLocalizedString(@"Couldn't use the open PDF file, please ensure you have signed it. This app won't let you send PDF files if you don't sign them.", @"Warning message") delegate:nil tag:-1 buttonTitle:nil];
            return;
        }
        // TODO: Refactor default PDF naming
        NSString *out_filename = [NSString stringWithFormat:@"%@_%d.pdf", [_view_pdf.pdf_name stringByDeletingPathExtension] , (int)[Helper get_timestamp]];
        
        [vc_mail setSubject:[NSString stringWithFormat:NSLocalizedString(@"Emailing signed document - %@", @"Subject for email when emailing a signed document"),out_filename]];
        [vc_mail setMessageBody:NSLocalizedString(@"Powered by i-Sign", @"Email body when sending a signed document") isHTML:NO];
        
        [vc_mail addAttachmentData:data_pdf mimeType:@"application/pdf" fileName:out_filename];
        
        [_vc_parent setShould_hide_toast:YES];
        [_vc_parent presentViewController:vc_mail animated:YES completion:nil];
    }
    else    {
        [Helper displayMessageWithTitle:NSLocalizedString(@"Hmm", @"Title for warning message alerts") message:NSLocalizedString(@"It seems your device's email settings are not configured", @"Warning message when mail settings are not configured") delegate:nil tag:-1 buttonTitle:nil];
    }
}

#pragma mark - UIActionSheetDelegate callbacks

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex    {
    if(buttonIndex == [actionSheet cancelButtonIndex])  {
        logg(@"cancelled");
        return;
    }
    NSString *out_folder;
    logg(@"button index %d", buttonIndex);
    buttonIndex -= 2;
    if(buttonIndex == -2)   {
        logg(@"new folder");
        // New folder
        if(!alert_new_folder)   {
            alert_new_folder = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Specify folder name", @"Instruction to specify folder name") message:nil delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel button in alert view") otherButtonTitles: NSLocalizedString(@"Save", @"Save button in alert view"), nil];
            alert_new_folder.tag = 0;
        }
        [alert_new_folder setAlertViewStyle:UIAlertViewStylePlainTextInput];
        [alert_new_folder show];
        return;
    }
    else if(buttonIndex == -1)    {
        logg(@"documents");
        // Documents
        out_folder = [Helper documentsPath];

    }
    else    {
        Inode *selected_inode = [arr_root_folders objectAtIndex:buttonIndex];
        logg(@"inode %@", selected_inode.name);
        out_folder = [[selected_inode url] path];
    }
    
    // Save
    [self saveToFolder:out_folder];
}

#pragma mark - UIAlertViewDelegate callbacks

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if(alertView.tag == 0)  {
        if(buttonIndex == [alertView cancelButtonIndex])    {
            // Show the action sheet again
            [action_sheet_save showFromToolbar:_vc_parent.navigationController.toolbar];
        }
        else    {
            // Must be the 'Save' button
            UITextField *txt = [alertView textFieldAtIndex:0];
            assert(txt);
            NSString *folder_name = txt.text;
            
            // Check if folder already exists
            if([[FileExplorer shared] rootFolderExists:folder_name])    {
                UIAlertView *alert_problem = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Title for alert view") message:[NSString stringWithFormat:NSLocalizedString(@"Folder '%@' already exists", @"User tried to create a folder with existing name"),folder_name] delegate:self cancelButtonTitle:NSLocalizedString(@"OK", @"OK button in alert view") otherButtonTitles:nil];
                alert_problem.tag = 1;
                [alert_problem show];
            }
            else    {
                // Create folder
                if(![[FileExplorer shared] createRootFolder:folder_name])  {
                    err(@"Failed to create folder");
                    return;
                }
                
                // Save
                NSString *out_folder = [[Helper documentsPath] stringByAppendingPathComponent:folder_name];
                [self saveToFolder:out_folder];
            }
        }
    }
    else if(alertView.tag == 1) {
        [alert_new_folder show];
    }
    else    {
        warn(@"Unrecognised alert view with tag %d", alertView.tag);
    }
}

#pragma mark - Convenience methods

// Assuming folder already exists
- (void) saveToFolder:(NSString *) folder   {
    NSString *out_filename = [NSString stringWithFormat:@"%@_%d.pdf", [_view_pdf.pdf_name stringByDeletingPathExtension] , (int)[Helper get_timestamp]];
    NSString *out_filepath = [folder stringByAppendingPathComponent:out_filename];
    
    OLGhostAlertView *alert_ghost;
    BOOL is_success = NO;
    if(_pdf_output_digital) {
        logg(@"Saving digital file");
        is_success = [Helper saveBuffer:_pdf_output_digital toPath:out_filepath];
    }
    else    {
        logg(@"Saving handwritten file");
        is_success = [_view_pdf saveFileNamed:out_filename toPath:out_filepath];
    }
    
    // Show popup
    if(is_success)  {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:NSLocalizedString(@"Success", @"Alert view title for success") message:[NSString stringWithFormat:NSLocalizedString(@"Saved to %@", @"Saved PDF to filename"),out_filename] timeout:1.5f dismissible:YES];
    }
    else    {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Alert view title for error") message:[NSString stringWithFormat:NSLocalizedString(@"Failed to save to %@", @"Failed to save PDF to filename"),out_filename] timeout:1.5f dismissible:YES];
    }
    [alert_ghost show];
}

#pragma mark - MFMailComposeViewControllerDelegate callbacks

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error  {
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    BOOL is_success = false;
    NSString *message = nil;
    if(result == MFMailComposeResultSaved)  {
        is_success = YES;
        message = NSLocalizedString(@"Email saved in drafts" , @"Info message when email saved in drafts by user");
    }
    else if(result == MFMailComposeResultSent)  {
        is_success = YES;
        message = NSLocalizedString(@"Email sent", @"Info message when email sent by user");
    }
    else if(result == MFMailComposeResultFailed)    {
        message = NSLocalizedString(@"Failed to send email, please try again", @"Error message when email failed to be sent due to error");
    }
    else if(result == MFMailComposeResultCancelled) {
        message = NSLocalizedString(@"Failed to send email, please try again", @"Error message when email failed to be sent due to user cancelling");
    }
    else    {
        message = NSLocalizedString(@"An unknown error occurred, please try again", @"Error message when email failed to be sent due to unknown error");
    }
    
    UIImage *img_hud = [UIImage imageNamed:is_success?@"checkmark":@"icon-warning"];
    BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:message];
    notify_hud.center = CGPointMake(_vc_parent.view.center.x, _vc_parent.view.center.y - 20);
    [_vc_parent.view addSubview:notify_hud];
    [notify_hud presentWithDuration:1.0f speed:0.5f inView:_vc_parent.view completion:^{
        [notify_hud removeFromSuperview];
    }];
}



@end
