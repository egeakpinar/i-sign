//
//  HandwrittenSignatureVC.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "HandwrittenSignatureVC.h"
#import "SignView1.h"
#import <QuartzCore/QuartzCore.h>
#import "OLGhostAlertView.h"

@interface HandwrittenSignatureVC ()    {
    OLGhostAlertView *alert_ghost;
}

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation_item;
@property (weak, nonatomic) IBOutlet SignView1 *view_sign;
@property (weak, nonatomic) IBOutlet UIView *view_canvas;

#pragma mark - UI callbacks
- (IBAction)btnNavClearTouch:(id)sender;
- (IBAction)btnNavDoneTouch:(id)sender;


@end

@implementation HandwrittenSignatureVC

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    [_view_canvas.layer setShadowOpacity:0.25f];
    
    // Setup navigation bar buttons
    [_navigation_item setTitle:NSLocalizedString(@"Sign below", nil)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setView_sign:nil];
    [self setNavigation_item:nil];
    [self setView_canvas:nil];
    [super viewDidUnload];
}

- (void) viewDidAppear:(BOOL)animated   {
    [super viewDidAppear:animated];
    
    if(!alert_ghost)    {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:NSLocalizedString(@"Sign on the canvas",nil) message:NSLocalizedString(@"Hit 'Done' when finished, or 'Clear' to start again",nil) timeout:1.5f dismissible:YES];
    }
    [alert_ghost show];
}

- (void) viewWillDisappear:(BOOL)animated   {
    [super viewWillDisappear:animated];

    [alert_ghost hide];
}

#pragma mark - UI callbacks
- (IBAction)btnNavClearTouch:(id)sender {
    [_view_sign clear];
}

- (IBAction)btnNavDoneTouch:(id)sender {
    if(_delegate &&[_delegate conformsToProtocol:@protocol(HandwrittenSignatureParent)])   {
        UIImage *img_signature = [_view_sign getCroppedImage];
        [_delegate receiveSignature:img_signature];
    }
    else    {
        warn(@"Couldn't pass signature image to delegate because delegate is either not set or is not conformant");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
