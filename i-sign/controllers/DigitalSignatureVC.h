//
//  DigitalSignatureVC.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 08/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"

@protocol DigitalSignatureVCDelegate <NSObject>

-(void) digitalSignatureSucceeded:(NSData *)pdf_output;
- (void) digitalSignatureFailedWithMessage:(NSString *)message;

@end


@interface DigitalSignatureVC : VC

#pragma mark - Properties
@property(nonatomic, retain) UINavigationController *navigationController;    // Required to show pairing GUI for baimobile reader (Note that this hides default property)

@property(nonatomic, copy) NSURL *pdfToSignURL;
@property(nonatomic, assign) id<DigitalSignatureVCDelegate> delegate;
@end

