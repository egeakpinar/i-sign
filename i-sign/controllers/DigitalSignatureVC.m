//
//  DigitalSignatureVC.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 08/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "DigitalSignatureVC.h"
#import <QuartzCore/QuartzCore.h>
#import "UIViewController+MJPopupViewController.h"
#import "ID_Keypad.h"

#if !(TARGET_IPHONE_SIMULATOR)
#import <SAM/SAM.h>
#import "SmartCardPKCS7SignatureHandler.h"
#else
@protocol SmartCardPKCS7SignatureHandlerDelegate<NSObject> @end
#endif

@interface DigitalSignatureVC ()<UITextFieldDelegate, ID_KeypadProtocol, SmartCardPKCS7SignatureHandlerDelegate>   {
    ID_Keypad *_keypad;
    int current_progress;
    int PIN_attempts_left;
}

#pragma mark - UI elements
// View - Status
@property (weak, nonatomic) IBOutlet UIView *view_status;
@property (weak, nonatomic) IBOutlet UIImageView *img_status_animation;
@property (weak, nonatomic) IBOutlet UILabel *lbl_status;
@property (weak, nonatomic) IBOutlet UIButton *btn_pair;
// View - PIN
@property (weak, nonatomic) IBOutlet UIView *view_PIN;
@property (weak, nonatomic) IBOutlet UILabel *lbl_enter_PIN;
@property (weak, nonatomic) IBOutlet UITextField *txt_PIN;
@property (weak, nonatomic) IBOutlet UILabel *lbl_attempt_remaining;
@property (weak, nonatomic) IBOutlet UIView *view_progress;


#pragma mark - UI callbacks
- (IBAction)btnPairTouch:(id)sender;

#pragma mark - Private properties
@property(nonatomic, retain) NSThread *thread_cc;

#pragma mark - Private method declarations
- (void) submitPINFailed;

@end

@implementation DigitalSignatureVC

typedef enum    {
    animation_insert_reader,
    animation_reader_ok,
    animation_reader_error,
    animation_insert_card,
    animation_card_ok,
    animation_card_error
} animation_state;

#pragma mark - Default init methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Localisation
    [_lbl_enter_PIN setText:NSLocalizedString(@"Enter PIN", nil)];
    [_txt_PIN setPlaceholder:NSLocalizedString(@"PIN code", nil)];
    [_lbl_attempt_remaining setText:[NSString stringWithFormat:NSLocalizedString(@"Attempts remaining: %d", nil), 3]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setImg_status_animation:nil];
    [self setLbl_status:nil];
    [self setBtn_pair:nil];
    [self setLbl_enter_PIN:nil];
    [self setTxt_PIN:nil];
    [self setLbl_attempt_remaining:nil];
    [self setView_PIN:nil];
    [self setView_status:nil];
    [self setView_progress:nil];
    [super viewDidUnload];
}

- (void) viewWillDisappear:(BOOL)animated   {
    if(_keypad)  {
        [_keypad hide];
    }
    
    if(_delegate && [_delegate isKindOfClass:[UIViewController class]])   {
        [(UIViewController *)_delegate viewWillAppear:NO];
    }
    
#if !(TARGET_IPHONE_SIMULATOR)
    
    // We're letting ID_Manager run for now
//    [[IDManager sharedManager] shutdown];

    // Unregister from card notifications ([CC stop] stops them anyways but it's safe to unregister)
//    [[NSNotificationCenter defaultCenter] removeObserver:self];
#endif
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar setTranslucent:YES];
    [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController setToolbarHidden:NO];
    
    current_progress = 0;
    [self setProgress:0];
    
    [_txt_PIN setText:@""];
    [_keypad reset];
    [self hidePINScreen];   // By default

#if !(TARGET_IPHONE_SIMULATOR)
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(ccStatusChange:) name:CardReaderStatusChangedNotification object:nil];
    if (![IDManager sharedManager]) {
    SAMError *error = nil;
    [IDManager sharedManagerWithReaders:[NSArray arrayWithObjects:@"com.ewcdma.i-DENTITY",
                                         @"com.ewcdma.eCarder",
                                         @"com.idtechproducts.ismart",
                                         @"com.precisebiometrics.ccidcontrol",
                                         @"com.precisebiometrics.ccidinterrupt",
                                         @"com.precisebiometrics.ccidbulk",
                                         @"com.precisebiometrics.sensor",
                                         @"com.baimobile.reader",
                                         @"com.ftsafe.CRD",
                                         @"com.ftsafe.iR301", nil] andCards:[NSArray arrayWithObject:[NSDictionary dictionaryWithObjectsAndKeys:@"eid", @"name", [NSNumber numberWithInt:4], @"pin_digits", @"3b980040000000000101ad1310", @"atr_pattern", @"ffff00ff00000000fffffffff0", @"atr_mask", nil]] andParameters:[NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], IsDemoKey, nil] error:&error];
    } else {
//        [[IDManager sharedManager] update];
    }
    
#else   // SIMULATOR
    [self showPINScreen];
#endif
}

-(void) viewDidAppear:(BOOL)animated    {
    [super viewDidAppear:animated];
    
#if !(TARGET_IPHONE_SIMULATOR)
    [self updateUIStatesWithReaderStatus:[IDManager sharedManager].readerStatus readerErrorMessage:@"" cardStatus:[IDManager sharedManager].cardStatus cardErrorMessages:@""];
    
#endif
    
}


#pragma mark - IDManager methods

#if !(TARGET_IPHONE_SIMULATOR)
- (void) ccStatusChange:(NSNotification *) notification {
    logg(@"+ ccStatusChange - StandbyVC");

    NSDictionary *dict = [notification userInfo];
    long card_status = [[dict objectForKey:@"card_status"] unsignedIntValue];
    long reader_status = [[dict objectForKey:@"reader_status"] unsignedIntValue];

    NSString *card_error_message=nil , *reader_error_message=nil;
    if(card_status == ID_CARD_CONNECT_ERROR)    {
        card_error_message = [dict objectForKey:@"card_status_error_message"];
    }
    if(reader_status == ID_READER_PROBLEM)    {
        reader_error_message = [dict objectForKey:@"reader_status_error_message"];
    }

    [self updateUIStatesWithReaderStatus:reader_status readerErrorMessage:reader_error_message cardStatus:card_status cardErrorMessages:card_error_message];
}


// Updates UI based on state information
- (void) updateUIStatesWithReaderStatus:(ID_CODE) reader_status readerErrorMessage:(NSString *) reader_error_message cardStatus:(ID_CODE) card_status cardErrorMessages:(NSString *) card_error_message  {
    animation_state state;
    BOOL should_examine_card = YES;
    switch (reader_status) {
        case ID_READER_ABSENT:
            should_examine_card = NO;
            [_lbl_status setText:NSLocalizedString(@"Waiting for reader",nil)];
            state = animation_insert_reader;

            break;
        case ID_READER_PROBLEM:
            should_examine_card = NO;
            state = animation_reader_error;
            [_lbl_status setText:NSLocalizedString(@"Error accessing reader", nil)];

            break;
        case ID_READER_ACCESS_SUCCESSFUL:
            // Look at card status

            break;
        case ID_READER_PRESENT:
        case ID_READER_TRYING:
            should_examine_card = NO;
            [_lbl_status setText:NSLocalizedString(@"Reader detected, working",)];
            state = animation_reader_ok;

            break;
        default:
            break;
    }
    if(should_examine_card) {
        switch (card_status) {
            case ID_CARD_PRESENT:
                logg(@"ID_CARD_PRESENT");
               // [self hidePINScreen];   // Ensure it's hidden
                state = animation_card_ok;
                [_lbl_status setText:NSLocalizedString(@"Card detected, working",)];
                [self showPINScreen];
                break;
            case ID_CARD_ABSENT:
                logg(@"ID_CARD_ABSENT");
                [self hidePINScreen];   // Ensure it's hidden
                state = animation_insert_card;
                [_lbl_status setText:NSLocalizedString(@"Waiting for card",)];

                break;
            case ID_CARD_CONNECT_ERROR:
                logg(@"ID_CARD_CONNECT_ERROR");
                [self hidePINScreen];   // Ensure it's hidden
                if(card_error_message)
                    [_lbl_status setText:[NSString stringWithFormat:@"%@ (%@)", NSLocalizedString(@"Error reading card", nil),card_error_message]];
                else
                    [_lbl_status setText:NSLocalizedString(@"Error reading card",nil)];
                state = animation_card_error;


                break;
            case ID_CARD_ACCESS_SUCCESSFUL:
                // Go to PIN screen
                [self showPINScreen];
                state = animation_card_ok;
                [_lbl_status setText:NSLocalizedString(@"Card read successfully", nil)];
                [self showPINScreen];

                break;
            default:
                break;
        }
    }
    else {
        [self hidePINScreen];   // Ensure it's hidden
    }

    [self updateStateAnimation:state];
}

// Note: This method is buggy when called before view is loaded (call it in viewDidAppear)
- (void) updateStateAnimation:(animation_state) state   {
    [_img_status_animation setAnimationDuration:2.0f];

    [_img_status_animation setAnimationImages:nil];
    [_img_status_animation setImage:nil];

    if(!IS_IPAD) {
        switch (state) {
            case animation_insert_reader:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-insertion-reader-1"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-2"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-reader-3"],nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_reader_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"iphone-insertion-reader-4"]];
                [_img_status_animation stopAnimating];

                break;
            case animation_reader_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-reader-error-2"],
                                                           [UIImage imageNamed:@"iphone-reader-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_insert_card:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-insertion-card-1"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-2"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],
                                                           [UIImage imageNamed:@"iphone-insertion-card-3"],nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_card_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"iphone-insertion-card-4"]];
                [_img_status_animation stopAnimating];

                break;
            case animation_card_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"iphone-card-error-2"],
                                                           [UIImage imageNamed:@"iphone-card-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];

                break;
            default:
                warn(@"Unrecognised animation state %d", state);
                break;
        }
    }
    else    {
        switch (state) {
            case animation_insert_reader:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-insertion-reader-1"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-2"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-reader-3"],nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_reader_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"ipad-insertion-reader-4"]];
                [_img_status_animation stopAnimating];

                break;
            case animation_reader_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-reader-error-2"],
                                                           [UIImage imageNamed:@"ipad-reader-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_insert_card:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-insertion-card-1"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-2"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],
                                                           [UIImage imageNamed:@"ipad-insertion-card-3"],nil]];
                [_img_status_animation startAnimating];

                break;
            case animation_card_ok:
                [_img_status_animation setImage:[UIImage imageNamed:@"ipad-insertion-card-4"]];
                [_img_status_animation stopAnimating];
                [self showPINScreen];

                break;
            case animation_card_error:
                [_img_status_animation setAnimationImages:[NSArray arrayWithObjects:
                                                           [UIImage imageNamed:@"ipad-card-error-2"],
                                                           [UIImage imageNamed:@"ipad-card-error-4"],
                                                           nil]];
                [_img_status_animation startAnimating];

                break;
            default:
                warn(@"Unrecognised animation state %d", state);
                break;
        }
    }
}
#endif

#pragma mark - UI callbacks

// Pairing bluetooth reader (baimobile)
- (IBAction)btnPairTouch:(id)sender {
    deb(@"+ btnPairTouch");
    warn(@"Unimplemented");
    /*
    if(self.navigationController)   {
        [self.view setHidden:YES];
        [[[CardCommunication instance] reader] showPairingGUI:self.navigationController callback:pairingCallback];
    }
    else    {
        err(@"Cannot show pairing GUI, navigation controller is not set");
    }*/
}
/*
// TODO: Update UI accordingly here
void pairingCallback(BOOL successful)   {
    // Not implemented
    // Note: Careful with logging here, this is a C style method
}*/

- (void) cancelNumpad:(id) sender   {
    [_txt_PIN resignFirstResponder];
}

#pragma mark - Custom methods
-(void)moveProgress:(NSNumber *)pct {
    [self setProgress:pct];
}

// TODO: Refactor this
- (void) setProgress:(NSNumber *)percentage    {
    logg(@"+ set progress %d", [percentage intValue]);
    if(![NSThread isMainThread])    {
        [self performSelectorOnMainThread:@selector(setProgress:) withObject:percentage waitUntilDone:NO];
        return;
    }
    
    int perc = [percentage intValue];
    if(perc < 0) perc = 0;
    if(perc > 100) perc = 100;
    
    float progress_bar_width = 198.0f;
    CGRect frame_original = _view_progress.frame;
    float new_width = progress_bar_width * (perc / 100.0f);
    [UIView transitionWithView:_view_progress duration:1.0f options:UIViewAnimationOptionCurveEaseOut animations:^{
        [_view_progress setFrame:CGRectMake(frame_original.origin.x, frame_original.origin.y, new_width, frame_original.size.height)];
    } completion:^(BOOL finished) {
        if(perc == 100)   {
            [_keypad hide];
        }
    }];
}

- (void) showPINScreen  {
    if(![NSThread isMainThread])    {
        [self performSelectorOnMainThread:@selector(showPINScreen) withObject:nil waitUntilDone:NO];
        return;
    }
    
    // TODO: Update PIN attempts here, based on value returned from SAM
    if(_view_PIN.alpha < 1)   {
        [UIView transitionWithView:self.view duration:0.5f options:UIViewAnimationOptionCurveLinear animations:^{
            [_view_status setAlpha:0.0f];
            [_view_PIN setAlpha:1.0f];
        } completion:^(BOOL finished) {
            [_txt_PIN becomeFirstResponder];
        }];
    }
}

- (void) hidePINScreen  {
    if(![NSThread isMainThread])    {
        [self performSelectorOnMainThread:@selector(hidePINScreen) withObject:nil waitUntilDone:NO];
        return;
    }
    if(_view_PIN.alpha > 0)   {
        [_txt_PIN resignFirstResponder];
        [UIView transitionWithView:self.view duration:0.5f options:UIViewAnimationOptionCurveLinear animations:^{
            [_view_status setAlpha:1.0f];
            [_view_PIN setAlpha:0.0f];
        } completion:nil];
    }
    if(_keypad)  {
        [_keypad hide];

        // Reset PIN
        [_keypad reset];
        [_txt_PIN setText:@""];
    }
}


- (void) submitPIN  {
    NSString *PIN = _txt_PIN.text;
    
    // Validate PIN requirements here
    if(![Helper stringValid:PIN])    {
        [Helper displayMessageWithTitle:NSLocalizedString(@"Oops", @"Alert message title for error") message:NSLocalizedString(@"PIN cannot be empty", @"Error popup, message description when input PIN is empty") delegate:nil tag:-1 buttonTitle:nil];
        [_keypad disableSubmission:NO animated:YES];
        return;
    }
    
    // PIN valid
    // Submit PIN in the background
    [self performSelectorInBackground:@selector(submitPIN_1:) withObject:PIN];
}

// Submitting PIN in the background
- (void) submitPIN_1:(NSString *) PIN   {
    deb(@"+ submitPIN_1 - PIN %@", PIN);
    assert(![NSThread isMainThread]);
#if !(TARGET_IPHONE_SIMULATOR)
    SAMError *error = nil;
    PIN_attempts_left = [[IDManager sharedManager] verifyPin:PIN error:&error];
    if(error)   {
        err(@"Error verifying PIN, %d attempts left", PIN_attempts_left);
        [self performSelectorOnMainThread:@selector(submitPINFailed) withObject:nil waitUntilDone:NO];
    }
    else    {
        logg(@"PIN verified, %d attempts left", PIN_attempts_left);
        
        // TODO: This is not ideal
        if(PIN_attempts_left == 3)  {
            [self signDocument:PIN];
        }
        else    {
            [self performSelectorOnMainThread:@selector(submitPINFailed) withObject:nil waitUntilDone:NO];
        }
    }
#endif
}

// Called after submit PIN fails
// Should be called on main thread
- (void) submitPINFailed    {
    assert([NSThread isMainThread]);
    [Helper displayMessageWithTitle:NSLocalizedString(@"Oops", @"Title for error popup when PIN fails") message:NSLocalizedString(@"PIN invalid, please try again", @"Error message for popup when PIN verification fails") delegate:nil tag:-1 buttonTitle:nil];
    [_lbl_attempt_remaining setText:[NSString stringWithFormat:@"%d",PIN_attempts_left]];
    [_keypad disableSubmission:NO animated:NO];
}

#if !(TARGET_IPHONE_SIMULATOR)
- (void) signDocument:(NSString *)verified_pin   {
    NSString* certfile = [[NSBundle mainBundle] pathForResource:@"patty2" ofType:@"p12"];
    
    NSData *pdf = [NSData dataWithContentsOfURL:self.pdfToSignURL];
    SmartCardPKCS7SignatureHandler *sign_handler = [[SmartCardPKCS7SignatureHandler alloc] init:certfile password:@"test" pin:verified_pin delegate:self];
    PDFDoc *doc = [[PDFDoc alloc] initWithBuf:pdf buf_size:pdf.length];
    SignatureHandlerId sigHandlerId = [doc AddSignatureHandler:sign_handler];
    
    TRNField* sigField = nil;
    FieldIterator *iter = [doc GetFieldIterator];
    while ([iter HasNext]) {
        TRNField *field = [iter Current];
        if (e_signature == [field GetType]) {
            logg(@"signature field found: %@", [field GetName]);
            // TODO: This is wrong, reference by name should be enough
            sigField = [[doc GetFieldIteratorWithName:[field GetName]] Current];
            break;
        }
        [iter Next];
    }
    Widget *widgetAnnot = nil;
    // Note: Using PNG gives error
    NSString *stamp_image_file = nil;
    if (!sigField) {
        stamp_image_file = [[NSBundle mainBundle] pathForResource:@"logo-jpg" ofType:@"jpg"];
        assert(stamp_image_file);
        warn(@"Signature field not found, creating a new one");
        
        // Create new signature form field in the PDFDoc.
        sigField = [doc FieldCreateWithString: @"Signature1" type: e_signature field_value: @"" def_field_value: @""];
        
        // Assign the form field as an annotation widget to the PDFDoc so that a signature appearance can be added.
        Page* page1 = [doc GetPage: 1];
        widgetAnnot = [Widget Create: [doc GetSDFDoc] pos: [[PDFRect alloc] initWithX1: 20 y1: 600 x2: 220 y2: 640] field: sigField];
        [page1 AnnotPushBack: widgetAnnot];
        [widgetAnnot SetPage: page1];
        Obj* widgetObj = [widgetAnnot GetSDFObj];
        [widgetObj PutNumber: @"F" value: 132];
        [widgetObj PutName: @"Type" name: @"Annot"];
    }
    else   {
        stamp_image_file = [[NSBundle mainBundle] pathForResource:@"logo-jpg-wide" ofType:@"jpg"];
        assert(stamp_image_file);
        widgetAnnot = [[Widget alloc] initWithD:[sigField GetSDFObj]];
    }
    
    // Tell PDFNetC to use the SignatureHandler created to sign the new signature form field.
    Obj* sigDict = [sigField UseSignatureHandler: sigHandlerId];
    
    // Add more information to the signature dictionary.
    [sigDict PutName: @"SubFilter" name: @"adbe.pkcs7.detached"];
    [sigDict PutString: @"Name" value: @"i-DENTITY"];
    [sigDict PutString: @"Location" value: @"Brussels"];
    [sigDict PutString: @"Reason" value: @"Document verification."];
    
    // Add visual stamp
    ElementWriter* apWriter = [[ElementWriter alloc] init];
    ElementBuilder* apBuilder = [[ElementBuilder alloc] init];
    [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
    Image* sigImg = [Image CreateWithFile: [doc GetSDFDoc] filename: stamp_image_file encoder_hints: [[Obj alloc]init]];
    double w = [sigImg GetImageWidth], h = [sigImg GetImageHeight];
    
    Element* apElement = [apBuilder CreateImageWithCornerAndScale: sigImg x: 0 y: 0 hscale: w vscale: h];
    [apWriter WritePlacedElement: apElement];
    Obj* apObj = [apWriter End];
    [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
    [apObj PutName: @"Subtype" name: @"Form"];
    [apObj PutName: @"Type" name: @"XObject"];
    [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
    apElement = [apBuilder CreateFormWithObj: apObj];
    [apWriter WritePlacedElement: apElement];
    apObj = [apWriter End];
    [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
    [apObj PutName: @"Subtype" name: @"Form"];
    [apObj PutName: @"Type" name: @"XObject"];
    [widgetAnnot SetAppearance: apObj annot_state: e_normal app_state: nil];
    [widgetAnnot RefreshAppearance];
    
    Obj* widgetObj = [widgetAnnot GetSDFObj];
    [widgetObj PutNumber: @"F" value: 132];
    [widgetObj PutName: @"Type" name: @"Annot"];
    
    // Add this sigDict as DocMDP in Perms dictionary from root
    Obj* root = [doc GetRoot];
    Obj* perms = [root PutDict: @"Perms"];
    // add the sigDict as DocMDP (indirect) in Perms
    [perms Put: @"DocMDP" obj: sigDict];
    
    // add the additional DocMDP transform params
    Obj* refObj = [sigDict PutArray: @"Reference"];
    Obj* transform = [refObj PushBackDict];
    [transform PutName: @"TransformMethod" name: @"DocMDP"];
    [transform PutName: @"Type" name: @"SigRef"];
    Obj* transformParams = [transform PutDict: @"TransformParams"];
    [transformParams PutNumber: @"P" value: 1]; // Set permissions as necessary.
    [transformParams PutName: @"Type" name: @"TransformParams"];
    [transformParams PutName: @"V" name: @"1.2"];
    
    // Save the PDFDoc. Once the method below is called, PDFNetC will also sign the document using the information
    // provided.
    NSData *buffer = [doc SaveToBuf:0];
    assert(buffer);
    
    NSLog(@"Finished certifying PDF document.");
    [_keypad disableSubmission:NO animated:NO];
    
    if(_delegate && [_delegate respondsToSelector:@selector(digitalSignatureSucceeded:)])    {
        [_delegate digitalSignatureSucceeded:buffer];
    }
}
#endif



#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField    {
    deb(@"+ text field should begin editing");
    // Show our custom keypad instead
    if(!_keypad) {
        _keypad = [ID_Keypad new];
        [_keypad setDelegate:self];
    }
    [_keypad show];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    assert(false);
    return NO;
}

#pragma mark - ID_KeypadProtocol callbacks
- (void)keypad:(ID_Keypad *)keypad inputChangedTo:(NSString *)result    {
    deb(@"input changed to %@",result);
    
    [_txt_PIN setText:result];
}

- (void)keypadInputCancelled    {
    deb(@"keypad input cancelled");
}

- (void)keypadInputSubmitted    {
    deb(@"keypad input submitted");
    
    [self submitPIN];
    [_keypad disableSubmission:YES animated:YES];
}

#pragma mark - SmartCardPKCS7SignatureHandlerDelegate callbacks

- (void)signatureProgressedTo:(NSNumber *)pct   {
    [self setProgress:pct];
}

@end
