//
//  Dummy.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 12/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Dummy.h"
#import "Debug.h"
#import "Helper.h"

#import "PKCS7SignatureHandler.h"
#import "CardCommunication.h"

@implementation Dummy

- (void) doIt   {
    [self initContext];
    
    // debb - Generate PKCS7 certificate
    /*
     CardCommunication *cc = [CardCommunication instance];
     CardReader *reader = [cc reader];
     Card *card = reader.card;
     if(!card)   {
     err(@"card is nil");
     return;
     }
     
     NSURL *file_url = [[NSBundle mainBundle] URLForResource:@"icon-x" withExtension:@"png"];
     NSData *data = [NSData dataWithContentsOfURL:file_url];
     
     if(reader.type != feitian)  {
     NSThread *thread_sdk = [[CardCommunication instance] thread_background];
     if(reader.type != bai)  {
     [card performSelector:@selector(getPKCS7:) onThread:thread_sdk withObject:data waitUntilDone:YES];
     }
     else    {
     [card performSelector:@selector(getPKCS7:) onThread:thread_sdk withObject:data waitUntilDone:NO];
     }
     }
     else    {   // Feitian requires everything on main thread
     
     NSMutableData *data_signature_pkcs7 = [card getPKCS7:data];
     if(!data_signature_pkcs7)    {
     err(@"Failed to get PKCS7 bytes");
     }
     
     }
     
     NSMutableData *data_signature = card.data_signature_pkcs7;
     if(!data_signature) {
     err(@"Data signature nil");
     //        return;
     }
     
     // We have the data, now create a SignatureHandler for PDFTron
     CustomSignatureHandler *sign_handler = [[CustomSignatureHandler alloc] initWithPrivateKey:card.mp_pkey andX509Certificate:card.mp_x509 andStack:card.mp_ca];
     
     [sign_handler setData_signature:data_signature];*/
    
    SignatureHandler2 *sign_handler = [SignatureHandler2 alloc];

    CardCommunication *cc = [CardCommunication instance];
    CardReader *reader = [cc reader];
    Card *card = reader.card;
    [sign_handler setCard:card];
    [sign_handler setReader:reader];
    
    sign_handler = [sign_handler init];
    
    NSString *pdf_path = _pdfToSignURL.path;
    //    pdf_path = @"Documents/out_1364986693.pdf";
    deb(@"pdf path %@",pdf_path);
    
    [self signPDF:pdf_path withSignature:sign_handler];
    
    // debb
}

- (void) initContext    {
    // Initialize PDFNetC
    [PDFNet Initialize: 0];
    
    // Initialize OpenSSL library
    CRYPTO_malloc_init();
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
}

- (BOOL) signPDF:(NSString *)path withSignature:(SignatureHandler *)sign_handler    {
    BOOL result = YES;
    // TODO
    // Note: PDFTron doesn't like PNG's
    NSString *imagefile = [[NSBundle mainBundle] URLForResource:@"jpg1" withExtension:@"jpg"].path;
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    NSString *outfile = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"signed_out%d.pdf",[Helper get_timestamp]]]; //add our image to the path
    
    deb(@"image file %@" , imagefile);
    deb(@"outfile %@", outfile);
    
    @try {
        NSLog(@"Signing PDF document: '%@'.", path);
        
        // Open an existing PDF
        PDFDoc* doc = [[PDFDoc alloc] initWithFilepath: path];
        
        // Find signature field
        int count_sign_field = 0;
        NSString *sign_field_name = nil;
        FieldIterator *it = [doc GetFieldIterator];
        while(it.HasNext) {
            TRNField *field = it.Current;
            if(field.GetType == e_signature)    {
                count_sign_field += 1;
                if(!sign_field_name)    {
                    sign_field_name = field.GetName;
                }
                logg(@"Signature field found: %@", field.GetName);
            }
            it.Next;
        }
        if(count_sign_field <= 0) {
            err(@"No signature field found!");
            return NO;
        }
        else if(count_sign_field > 1) {
            warn(@"More than one signature field found, defaulting to first one");
        }
        TRNField *sigField = [doc GetField:sign_field_name];
        
        // Add the SignatureHandler instance to PDFDoc, making sure to keep track of it using the ID returned.
        SignatureHandlerId sigHandlerId = [doc AddSignatureHandler: sign_handler];
        
        // Obtain the signature form field from the PDFDoc via Annotation.
        Widget* widgetAnnot = [[Widget alloc] initWithD: [sigField GetSDFObj]];
        
        // Tell PDFNetC to use the SignatureHandler created to sign the new signature form field.
        Obj* sigDict = [sigField UseSignatureHandler: sigHandlerId];
        
        // Add more information to the signature dictionary.
        [sigDict PutName: @"SubFilter" name: @"adbe.pkcs7.detached"];
        [sigDict PutString: @"Name" value: @"John Doe"];
        [sigDict PutString: @"Location" value: @"Brussels, Belgium"];
        [sigDict PutString: @"Reason" value: @"Document verification. Powered by i-Sign."];
        
        // Add the signature appearance
        ElementWriter* apWriter = [[ElementWriter alloc] init];
        ElementBuilder* apBuilder = [[ElementBuilder alloc] init];
        [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
        Image* sigImg = [Image CreateWithFile: [doc GetSDFDoc] filename: imagefile encoder_hints: [[Obj alloc]init]];
        double w = [sigImg GetImageWidth], h = [sigImg GetImageHeight];
        
        Element* apElement = [apBuilder CreateImageWithCornerAndScale: sigImg x: 0 y: 0 hscale: w vscale: h];
        [apWriter WritePlacedElement: apElement];
        Obj* apObj = [apWriter End];
        [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
        [apObj PutName: @"Subtype" name: @"Form"];
        [apObj PutName: @"Type" name: @"XObject"];
        [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
        apElement = [apBuilder CreateFormWithObj: apObj];
        [apWriter WritePlacedElement: apElement];
        apObj = [apWriter End];
        [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
        [apObj PutName: @"Subtype" name: @"Form"];
        [apObj PutName: @"Type" name: @"XObject"];
        [widgetAnnot SetAppearance: apObj annot_state: e_normal app_state: nil];
        [widgetAnnot RefreshAppearance];
        
        Obj* widgetObj = [widgetAnnot GetSDFObj];
        [widgetObj PutNumber: @"F" value: 132];
        [widgetObj PutName: @"Type" name: @"Annot"];
        
        // Save the PDFDoc. Once the method below is called, PDFNetC will also sign the document using the information
        // provided.
        [doc SaveToFile: outfile flags: 0];
        
        NSLog(@"Finished signing PDF document.");
    }
    @catch (NSException* e) {
        NSLog(@"Exception: %@ - %@\n", [e name], [e reason]);
        result = NO;
    }
    
    return result;
}
@end
