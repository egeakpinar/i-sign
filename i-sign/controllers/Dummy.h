//
//  Dummy.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 12/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CustomSignatureHandler.h"
#import "SignatureHandler1.h"
#import "SignatureHandler2.h"
#import <PDFTron/Headers/ObjC/PDFNetOBJC.h>

@interface Dummy : NSObject

- (void) doIt;
@property(nonatomic, copy) NSURL *pdfToSignURL;

@end
