//
//  ExplorerVC.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VC.h"
#import "Folder.h"

@interface ExplorerVC : VC

#pragma mark - Public properties
@property(nonatomic, retain, readonly) Folder *rootFolder;

#pragma mark - Public methods
- (id) initWithRootFolder:(Folder *) rootFolder;

@end
