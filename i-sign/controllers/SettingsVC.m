//
//  SettingsVC.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 26/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "SettingsVC.h"
#import "Cell_Switch.h"
#import <MessageUI/MFMailComposeViewController.h>
#import "BDKNotifyHUD.h"
#import "Logger.h"
#import "Common.h"

@interface SettingsVC ()<UITableViewDataSource, UITableViewDelegate, Cell_Switch_Delegate, MFMailComposeViewControllerDelegate>

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet UITableView *tbl_settings;
@property (weak, nonatomic) IBOutlet UINavigationBar *navbar;
@property (weak, nonatomic) IBOutlet UINavigationItem *navigation_item;

@end

@implementation SettingsVC


#pragma mark - Cell_Switch_Delegate methods

- (void) cellSwitch:(Cell_Switch *)cell valueChanged:(id) sender    {
    if(cell.tag == 1)  {
        // Extension toggle
        
        NSUserDefaults *user_defaults = [NSUserDefaults standardUserDefaults];
        [user_defaults setObject:[NSNumber numberWithBool:cell.is_on] forKey:SETTING_SHOW_FILE_EXTENSIONS];
        if(![user_defaults synchronize])    {
            warn(@"Failed to save user defaults");
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:NOTIFICATION_FILE_EXTENSION_SETTING_CHANGED object:nil];
    }
    else    {
        warn(@"Unexpected cell tag %d in Cell Switch delegate",cell.tag);
    }
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    int section = indexPath.section;
    int row = indexPath.row;
    if(section == 4)    {   // Support
        if(row == 0)    {
            [self sendLog];
        }
        else if(row == 1)   {
            [self sendFeedback];
        }
    }
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

#pragma mark - UITableViewDataSource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView   {
    return 5;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section    {
    switch (section) {
        case 0:
            return 2;
        case 1:
            return 1;
        case 2:
            return 3;
        case 3:
            return 3;
        case 4:
            return 2;
        default:
            warn(@"Unexpected section index for table %d", section);
    }
    return 0;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 0:
            return NSLocalizedString(@"General", @"Title for 'General' section in settings");
        case 1:
            return NSLocalizedString(@"Digital signature", @"Title for 'Digital signature' section in settings");
        case 2:
            return NSLocalizedString(@"Handwritten signature", @"Title for 'Handwritten signature' section in settings");
        case 3:
            return NSLocalizedString(@"Misc.", @"Title for 'Misc.' section in settings");
            
        case 4:
            return NSLocalizedString(@"Support", @"Title for 'Support' section in settings");
        default:
            warn(@"Unexpected section index %d", section);
            return @"";
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath  {
    UITableViewCell *cell = nil;
    
    int section = indexPath.section;
    int row = indexPath.row;
    if(section == 0)    {
        if(row == 0)    {
            // Release version
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"Style1"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Style1"];
            }
            
            [cell.textLabel setText:NSLocalizedString(@"Release version", @"Release version display in settings")];
            NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
            [cell.detailTextLabel setText:version];
        }
        else if(row == 1)   {
            // File extension toggle
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Switch"];
            assert(cell);
            Cell_Switch *cell_s = (Cell_Switch *)cell;            
            [cell_s setTitle:NSLocalizedString(@"File extension", @"File extension toggle in settings")];
            cell_s.tag = 1;
            cell_s.delegate = self;
            
            NSUserDefaults *user_defaults = [NSUserDefaults standardUserDefaults];
            NSNumber *num_setting = [user_defaults objectForKey:SETTING_SHOW_FILE_EXTENSIONS];
            if(num_setting) {
                [cell_s setIs_on:[num_setting boolValue]];
            }
            else {
                // By default, show file extensions
                [cell_s setIs_on:YES];
            }            
        }
        else    {
            warn(@"Unexpected row %d in section %d", row, section);
        }
    }
    else if(section == 1)   {
        // Digital signature
        if(row == 0)    {
            cell = [tableView dequeueReusableCellWithIdentifier:@"Style1"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Style1"];
            }
            
            [cell.textLabel setText:NSLocalizedString(@"PKCS12 Certificates", @"Row in settings")];
            [cell.detailTextLabel setText:NSLocalizedString(@"Edit", @"Edit action for PKCS12 certificates in settings")];
        }
        else    {
            warn(@"Unexpected row %d section %d", row, section);
        }
    }
    else if(section == 2)   {
        // Handwritten signature
        if(row == 0)    {
            // My signature
            cell = [tableView dequeueReusableCellWithIdentifier:@"Style1"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Style1"];
            }
            
            [cell.textLabel setText:NSLocalizedString(@"My Signature", @"Row in settings")];
            [cell.detailTextLabel setText:NSLocalizedString(@"Edit", @"Edit action for My Signature in settings")];
        }
        else if(row == 1)   {
            // My initials
            cell = [tableView dequeueReusableCellWithIdentifier:@"Style1"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:@"Style1"];
            }
            
            [cell.textLabel setText:NSLocalizedString(@"My Initials", @"Row in settings")];
            [cell.detailTextLabel setText:NSLocalizedString(@"Edit", @"Edit action for My Initials in settings")];
        }
        else if(row == 2)   {
            // Email footer
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Switch"];
            assert(cell);
            Cell_Switch *cell_s = (Cell_Switch *)cell;
            [cell_s setTitle:NSLocalizedString(@"Email footer", @"Email footer toggle in settings")];
            cell_s.tag = -1;
            cell_s.delegate = self;
        }
        else    {
            warn(@"Unexpected row %d section %d", row, section);
        }
    }
    else if(section == 3)   {
        // Misc.
        cell = [tableView dequeueReusableCellWithIdentifier:@"Cell_Switch"];
        assert(cell);
        Cell_Switch *cell_s = (Cell_Switch *)cell;
        cell_s.tag = -1;
        
        if(row == 0)    {   // Geolocation switch
            [cell_s setTitle:NSLocalizedString(@"Geolocation", @"Geolocation toggle in settings")];
        }
        else if(row == 1)   {   // Timestamp switch
            [cell_s setTitle:NSLocalizedString(@"Timestamp", @"Timestamp toggle in settings")];
        }
        else if(row == 2)   {   // Auto save signed file switch
            [cell_s setTitle:NSLocalizedString(@"Auto-save signed file", @"Auto save signed file toggle in settings")];
        }
        else    {
            warn(@"Unexpected row %d section %d", row, section);
        }
        
        return cell_s;
    }
    else if(section == 4)   {
        // Support
        if(row == 0)    {
            // Send log

            cell = [tableView dequeueReusableCellWithIdentifier:@"Standard"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Standard"];
            }
            
            [cell.textLabel setText:NSLocalizedString(@"Send log", @"Send log button in settings")];
        }
        else if(row == 1)   {
            // Send feedback
            
            cell = [tableView dequeueReusableCellWithIdentifier:@"Standard"];
            if(!cell)   {
                cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Standard"];
            }
            
            [cell.textLabel setText:@"Send feedback"];
            [cell.textLabel setText:NSLocalizedString(@"Send feedback", @"Send feedback button in settings")];            
        }
        else    {
            warn(@"Unexpected row %d in section %d", row, section);
        }
    }
    return cell;
}

#pragma mark - MFMailComposeViewControllerDelegate callbacks

- (void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error  {
    [controller dismissViewControllerAnimated:YES completion:nil];
    
    BOOL is_success = false;
    NSString *message = nil;
    if(result == MFMailComposeResultSaved)  {
        is_success = YES;
        message = NSLocalizedString(@"Email saved in drafts" , @"Info message when log/feedback email saved in drafts by user");
    }
    else if(result == MFMailComposeResultSent)  {
        is_success = YES;
        message = NSLocalizedString(@"Email sent, thank you", @"Info message when log/feedback email sent by user");
    }
    else if(result == MFMailComposeResultFailed)    {
        message = NSLocalizedString(@"Failed to send email, please try again", @"Error message when log/feedback email failed to be sent due to error");
    }
    else if(result == MFMailComposeResultCancelled) {
        message = NSLocalizedString(@"Failed to send email, please try again", @"Error message when log/feedback email failed to be sent due to user cancelling");
    }
    else    {
        message = NSLocalizedString(@"An unknown error occurred, please try again", @"Error message when log/feedback email failed to be sent due to unknown error");
    }
    
    UIImage *img_hud = [UIImage imageNamed:is_success?@"checkmark":@"icon-warning"];
    BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:message];
    [self.view addSubview:notify_hud];
    notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
    [notify_hud presentWithDuration:1.0f speed:0.5f inView:self.view completion:^{
        [notify_hud removeFromSuperview];
    }];
}

#pragma mark - Custom methods

- (void) sendLog    {
    // Note: Reusing vc_mail is problematic
    MFMailComposeViewController *vc_mail = [MFMailComposeViewController new];
    [vc_mail setMailComposeDelegate:self];
    
    // Populate mail message
    
    // Add logs
    NSArray *arr_logs = [[Logger shared] logFilePaths];
    if(!arr_logs || [arr_logs count] <= 0)  {
        err(@"No log found");
        [Helper displayMessageWithTitle:NSLocalizedString(@"Oops", @"Title for error popup") message:NSLocalizedString(@"Couldn't find log file to send", @"Error description when no log file found to email") delegate:nil tag:-1 buttonTitle:nil];
        return;
    }
    for(NSString *log_path in arr_logs) {
        NSData *data_log = [NSData dataWithContentsOfFile:log_path];
        assert(data_log);
        NSData *compressed_data_log = [Common gzipData:data_log];
        assert(compressed_data_log);
        NSString *file_name = [[log_path lastPathComponent] stringByAppendingString:@".gz"];
        [vc_mail addAttachmentData:compressed_data_log mimeType:@"text" fileName:file_name];
    }
    
    [vc_mail setSubject:NSLocalizedString(@"iOS_i-Sign Log file", @"Subject for email when sending log")];
    [vc_mail setToRecipients:[NSArray arrayWithObject:@"support@i-dentity.be"]];
    [vc_mail setMessageBody:NSLocalizedString(@"Log attached", @"Email body when sending log") isHTML:NO];
    [self presentViewController:vc_mail animated:YES completion:nil];
}

- (void) sendFeedback   {
    // Note: Reusing vc_mail is problematic
    MFMailComposeViewController *vc_mail = [MFMailComposeViewController new];
    [vc_mail setMailComposeDelegate:self];
    
    // Populate mail message
    
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [vc_mail setMessageBody:[NSString stringWithFormat:NSLocalizedString(@"i-Sign v%@ Feedback\n\n", @"Email body when sending feedback"),version] isHTML:NO];

    
    [vc_mail setSubject:NSLocalizedString(@"iOS_i-Sign Feedback", @"Subject for email when sending feedback")];
    [vc_mail setToRecipients:[NSArray arrayWithObject:@"feedback@i-dentity.be"]];
    [self presentViewController:vc_mail animated:YES completion:nil];
}

#pragma mark - UI callbacks
- (void) navbarRightTouch:(id)sender    {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Default methods
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    // Register cell for reuse
    [_tbl_settings registerNib:[UINib nibWithNibName:@"Cell_Switch" bundle:nil] forCellReuseIdentifier:@"Cell_Switch"];
    
    // Configure navigation bar
    [_navigation_item setTitle:NSLocalizedString(@"Settings", @"Title for settings view")];
    [_navigation_item setRightBarButtonItem:[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(navbarRightTouch:)]];
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    [_tbl_settings reloadData]; // So it shows the latest settings
    [_tbl_settings setContentOffset:CGPointMake(0, 0)];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setTbl_settings:nil];
    [self setNavbar:nil];
    [self setNavigation_item:nil];
    [super viewDidUnload];
}
@end
