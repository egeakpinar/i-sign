//
//  ViewPDFVC.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 05/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ViewPDFVC.h"
#import "Debug.h"
#import "ID_PDFView.h"
#import "UIViewController+MJPopupViewController.h"
#import "HandwrittenSignatureVC.h"
#import "DigitalSignatureVC.h"
#import "SettingsVC.h"
#import "OLGhostAlertView.h"
#import "ViewPDF_Share.h"
#import "BDKNotifyHUD.h"

@interface ViewPDFVC ()<UIActionSheetDelegate , HandwrittenSignatureParent, DigitalSignatureVCDelegate, ID_PDFViewDelegate> {
    HandwrittenSignatureVC *vc_handwritten;
    DigitalSignatureVC *vc_digital;
    SettingsVC *vc_settings;
    BOOL should_hide_toast;
    OLGhostAlertView *alert_ghost;
}

@property(nonatomic, retain) ID_PDFView *view_pdf;
@property(nonatomic, retain) UIScrollView *scroll_thumbs;   // Scroll view for thumbnails
@property(nonatomic, retain) NSData *pdf_output_digital;    // Digitally signed PDF output

@end

@implementation ViewPDFVC

@synthesize should_hide_toast = should_hide_toast;

#pragma mark - Properties
- (void) setPdf_url:(NSURL *)pdf_url    {
    _pdf_url = pdf_url;
    [_view_pdf setPdf_url:_pdf_url];
    _pdf_output_digital = nil;
}

- (void) setPdf_name:(NSString *)pdf_name   {
    _pdf_name = pdf_name;
    [_view_pdf setPdf_name:_pdf_name];
}


#pragma mark - Default init methods

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Add background
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"back-gray"]]];
    
    // Add navigation bar buttons
    float icon_width = 30;
    float icon_height = 30;
    
    UIImage *img_sign = [UIImage imageNamed:@"icon-pen"];
    UIButton *btn_sign = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_sign setFrame:CGRectMake(0, 0, icon_width, icon_height)];
    [btn_sign setBackgroundImage:img_sign forState:UIControlStateNormal];
    [btn_sign addTarget:self action:@selector(btnSignTouch:event:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *nav_btn_sign = [[UIBarButtonItem alloc] initWithCustomView:btn_sign];
    
    UIImage *img_save = [UIImage imageNamed:@"icon-save"];
    UIButton *btn_save = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_save setFrame:CGRectMake(0, 0, icon_width, icon_height)];
    [btn_save setBackgroundImage:img_save forState:UIControlStateNormal];
    [btn_save addTarget:self action:@selector(btnSaveTouch:event:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *nav_btn_save = [[UIBarButtonItem alloc] initWithCustomView:btn_save];

    UIImage *img_menu = [UIImage imageNamed:@"icon-menu"];
    UIButton *btn_menu = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn_menu setFrame:CGRectMake(0, 0, icon_width, icon_height)];
    [btn_menu setBackgroundImage:img_menu forState:UIControlStateNormal];
    [btn_menu addTarget:self action:@selector(btnMenuTouch:event:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *nav_btn_menu = [[UIBarButtonItem alloc] initWithCustomView:btn_menu];
    
    UIBarButtonItem *item_space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    item_space.width = 15;
    NSArray *arr_right_top_buttons = [NSArray arrayWithObjects:item_space, nav_btn_menu, item_space, nav_btn_save,item_space, nav_btn_sign, nil];
    [self.navigationItem setRightBarButtonItems:arr_right_top_buttons animated:NO];
 
    // Configure toolbar, add a thumbnail scroller
    [self.navigationController setToolbarHidden:NO];
    float toolbar_width = self.navigationController.toolbar.frame.size.width;
    _scroll_thumbs = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, toolbar_width - 30, 30)];

    // Add flexible space to center thumbs
    UIBarButtonItem *item_space_flexible = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *toolbar_btn_scroll = [[UIBarButtonItem alloc] initWithCustomView:_scroll_thumbs];
    [self setToolbarItems:[NSArray arrayWithObjects:item_space_flexible,toolbar_btn_scroll,item_space_flexible,nil]];
    

    // Add a PDFView
    _view_pdf = [[ID_PDFView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) andScrollViewForThumbs:_scroll_thumbs];
    [_view_pdf setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth];
    [_view_pdf setDelegate_pdfview:self];
    [_view_pdf setClipsToBounds:YES];
    if(_pdf_url)    {
        [_view_pdf setPdf_url:_pdf_url];
    }
    if(_pdf_name)   {
        [_view_pdf setPdf_name:_pdf_name];
    }
    [_view_pdf setScale_signature:0.25f];
    [self.view addSubview:_view_pdf];
    
    // Add gesture recognisers
    UITapGestureRecognizer *recogniser_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
    recogniser_tap.numberOfTapsRequired = 1;
    [self.view addGestureRecognizer:recogniser_tap];
    

    // Initialise instance variables
    should_hide_toast = NO;
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];
    
    // Configure bars' transparency
    [self.navigationController.toolbar setTranslucent:YES];
    [self.navigationController.navigationBar setTranslucent:YES];

    // Hide toolbar and navigation bar
    [self.navigationController setToolbarHidden:YES animated:NO];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void) viewDidAppear:(BOOL)animated   {
    [super viewDidAppear:animated];

    if(!should_hide_toast)  {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:_pdf_name message:nil timeout:0.75f dismissible:YES];
        [alert_ghost show];
    }
    should_hide_toast = NO;
}

- (void) viewWillDisappear:(BOOL)animated   {
    [super viewWillDisappear:animated];
    
    [alert_ghost hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}

- (void) viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    // Configure width of thumbs scroll
    float toolbar_width = self.navigationController.toolbar.frame.size.width;
    CGRect prev_frame = _scroll_thumbs.frame;
    prev_frame.size.width = toolbar_width - 30;
    [_scroll_thumbs setFrame:prev_frame];

    [_view_pdf parentFrameUpdatedTo:self.view.frame];
    
    // TODO: Keypad should be sent setNeedsLayout here
}

#pragma mark - UI callbacks


//
// Recogniser callbacks
//
- (void) handleTap:(UITapGestureRecognizer *)recogniser {
    if([recogniser state] == UIGestureRecognizerStateEnded) {
        // Toggle bars
        BOOL is_hidden = self.navigationController.isNavigationBarHidden;
        if(is_hidden)   {
            [self.navigationController setToolbarHidden:NO animated:YES];
            [self.navigationController setNavigationBarHidden:NO animated:YES];
        }
        else    {
            [self.navigationController setToolbarHidden:YES animated:NO];
            [self.navigationController setNavigationBarHidden:YES animated:NO];
        }
    }
}

//
// Navigation bar
//

- (void) btnSignTouch:(id) sender event:(UIEvent *)event   {
    [self becomeFirstResponder];

    UIView *view_touch = nil;
    for(UITouch *touch in [event allTouches])   {
        if([touch phase] == UITouchPhaseEnded)  {
            view_touch = touch.view;
            break;
        }
    }
    assert(view_touch);
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    UIMenuItem *menu_item_handwritten = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Handwritten",@"Handwritten signature option button") action:@selector(btnSignHandwrittenTouch:)];
    UIMenuItem *menu_item_digital = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Digital", @"Digital signature option button") action:@selector(btnSignDigitalTouch:)];
    
    menuController.menuItems = [NSArray arrayWithObjects:menu_item_handwritten, menu_item_digital, nil];
    [menuController setTargetRect:view_touch.frame inView:self.navigationController.navigationBar];
    [menuController setMenuVisible:YES animated:YES];
}

- (void) btnSaveTouch:(id) sender event:(UIEvent *)event   {
    [self becomeFirstResponder];
    
    UIView *view_touch = nil;
    for(UITouch *touch in [event allTouches])   {
        if([touch phase] == UITouchPhaseEnded)  {
            view_touch = touch.view;
            break;
        }
    }
    assert(view_touch);
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    UIMenuItem *menu_item_save_local = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Local",@"Save PDF locally menu button") action:@selector(btnSaveLocalTouch:)];
    UIMenuItem *menu_item_save_remote = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Remote",@"Save PDF remotely menu button") action:@selector(btnSaveRemoteTouch:)];
    
    menuController.menuItems = [NSArray arrayWithObjects:menu_item_save_local, menu_item_save_remote, nil];
    [menuController setTargetRect:view_touch.frame inView:self.navigationController.navigationBar];
    [menuController setMenuVisible:YES animated:YES];
}

- (void) btnMenuTouch:(id) sender event:(UIEvent *)event   {
    [self becomeFirstResponder];
    
    UIView *view_touch = nil;
    for(UITouch *touch in [event allTouches])   {
        if([touch phase] == UITouchPhaseEnded)  {
            view_touch = touch.view;
            break;
        }
    }
    assert(view_touch);
    
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    UIMenuItem *menu_item_email = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Email",@"Emailed saved PDF, menu button") action:@selector(btnEmailTouch:)];
    UIMenuItem *menu_item_settings = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Settings",@"Settings, menu button") action:@selector(btnSettingsTouch:)];
    
    menuController.menuItems = [NSArray arrayWithObjects:menu_item_email, menu_item_settings,nil];
    [menuController setTargetRect:view_touch.frame inView:self.navigationController.navigationBar];
    [menuController setMenuVisible:YES animated:YES];
}

//
// Signature callbacks
//

- (void) btnSignHandwrittenTouch:(id)sender  {
    if(_pdf_output_digital) {
        UIImage *img_hud = [UIImage imageNamed:@"icon-warning"];
        NSString *popup_message = NSLocalizedString(@"The document is already signed digitally, you can't add a handwritten signature now",@"Error message when a PDF is already signed digitally and user tries to access handwritten signature screen");
        BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:popup_message];
        [self.view addSubview:notify_hud];
        notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
        [notify_hud presentWithDuration:2.0f speed:0.5f inView:self.view completion:^{
            [notify_hud removeFromSuperview];
        }];
        return;
    }
    
    if(!vc_handwritten) {
        vc_handwritten = [[HandwrittenSignatureVC alloc] initWithNibName:@"HandwrittenSignature" bundle:nil];
        [vc_handwritten setDelegate:self];
    }
    should_hide_toast = YES;
    [self presentViewController:vc_handwritten animated:YES completion:nil];
}

- (void) btnSignDigitalTouch:(id)sender {
    
    if(_pdf_output_digital) {
        UIImage *img_hud = [UIImage imageNamed:@"checkmark"];
        NSString *popup_message = NSLocalizedString(@"The document is already signed",@"Error message when a PDF is already signed digitally and user tries to access digital signature screen");
        BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:popup_message];
        [self.view addSubview:notify_hud];
        notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
        [notify_hud presentWithDuration:2.0f speed:0.5f inView:self.view completion:^{
            [notify_hud removeFromSuperview];
        }];
        return;
    }
    

#if (TARGET_OS_IPHONE)
    should_hide_toast = YES;
    if(!vc_digital)   {
        if(IS_IPAD) {
            vc_digital = [[DigitalSignatureVC alloc] initWithNibName:@"DigitalSignatureView_ipad" bundle:nil];
        }
        else    {
            vc_digital = [[DigitalSignatureVC alloc] initWithNibName:@"DigitalSignatureView" bundle:nil];            
        }
        [vc_digital setDelegate:self];
        [vc_digital setNavigationController:self.navigationController];
    }
    [vc_digital setPdfToSignURL:_pdf_url];
    [self presentPopupViewController:vc_digital animationType:MJPopupViewAnimationSlideBottomBottom];
#endif
}

- (void) btnSaveLocalTouch:(id) sender   {
    [[ViewPDF_Share sharedWithParent:self] btnSaveLocalTouchWithPDFView:_view_pdf PDFBuffer:_pdf_output_digital];
}

- (void) btnEmailTouch:(id) sender  {
    [[ViewPDF_Share sharedWithParent:self] btnEmailTouchWithPDFView:_view_pdf PDFBuffer:_pdf_output_digital];
}

- (void) btnSettingsTouch:(id) sender   {
    should_hide_toast = YES;
    if(!vc_settings)   {
        vc_settings = [[SettingsVC alloc] initWithNibName:@"SettingsView" bundle:nil];
    }
    [self presentViewController:vc_settings animated:YES completion:nil];
}

- (void) btnSaveRemoteTouch:(id) sender   {
    deb(@"+ btnSaveRemoteTouch");
    [Helper displayMessageWithTitle:@"Info" message:@"Under development" delegate:nil tag:-1 buttonTitle:nil];
}

//
// Other UI callbacks
//
- (void) btnPrevTouch:(id) sender   {
    logg(@"+ btnPrevTouch");
    [_view_pdf prevPage];
}

- (void) btnNextTouch:(id) sender   {
    logg(@"+ btnNextTouch");
    [_view_pdf nextPage];
}

#pragma mark - HandwrittenSignatureParent callbacks

- (void)receiveSignature:(UIImage *)signature   {
    logg(@"+ receive signature");
    if(!signature)  {
        err(@"Received signature is nil");
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:NSLocalizedString(@"Error", @"Error title for alert") message:NSLocalizedString(@"You haven't signed the canvas", @"Error message when user dismisses signature screen without signing") timeout:2.0f dismissible:YES];
        [alert_ghost show];
    }
    else    {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:NSLocalizedString(@"Signature done", @"Title for alert after user finishes drawing signature") message:NSLocalizedString(@"Now double click to attach your signature as many times as you'd like", @"Instrucion message for attaching signature") timeout:2.0f dismissible:YES];
        [alert_ghost show];    
    }
    
    [_view_pdf setImg_signature:signature];
}

#pragma mark - DigitalSignatureVCDelegate callbacks

- (void)digitalSignatureSucceeded:(NSData *)pdf_output  {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    _pdf_output_digital = pdf_output;
    
    if(!pdf_output) {
        UIImage *img_hud = [UIImage imageNamed:@"icon-warning"];
        NSString *popup_message = NSLocalizedString(@"Signature failed - Unknown error", @"Popup message after digital signature is successful and PDF is saved");
        BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:popup_message];
        [self.view addSubview:notify_hud];
        notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
        [notify_hud presentWithDuration:2.0f speed:0.5f inView:self.view completion:^{
            [notify_hud removeFromSuperview];
        }];
        return;
    }

    UIImage *img_hud = [UIImage imageNamed:@"checkmark"];
    BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:NSLocalizedString(@"Signed successfully, you can save or email it.", @"Popup message after digital signature is successful")];
    [self.view addSubview:notify_hud];
    notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
    [notify_hud presentWithDuration:2.0f speed:0.5f inView:self.view completion:^{
        [notify_hud removeFromSuperview];
    }];
    
}

- (void) digitalSignatureFailedWithMessage:(NSString *)message  {
    [self dismissPopupViewControllerWithanimationType:MJPopupViewAnimationSlideTopBottom];
    
    UIImage *img_hud = [UIImage imageNamed:@"icon-warning"];
    NSString *popup_message;
    if(message) {
        popup_message = [NSString stringWithFormat:NSLocalizedString(@"Signature failed - %@", @"Popup message after digital signature fails"),message];
    }
    else    {
        popup_message = NSLocalizedString(@"Signature failed - Unknown error", @"Popup message after digital signature is successful and PDF is saved");
    }
    BDKNotifyHUD *notify_hud = [BDKNotifyHUD notifyHUDWithImage:img_hud text:popup_message];
    [self.view addSubview:notify_hud];
    notify_hud.center = CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/2.0f);
    [notify_hud presentWithDuration:2.0f speed:0.5f inView:self.view completion:^{
        [notify_hud removeFromSuperview];
    }];
}

#pragma mark - ID_PDFViewDelegate methods

- (void)pdfViewSignatureClickedAtPoint:(CGPoint)point   {
    deb(@"+ pdfViewSignatureClicked");
    [Debug print_point:point];
    
    // Show signature menu
    UIMenuController *menuController = [UIMenuController sharedMenuController];
    UIMenuItem *menu_item_handwritten = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Handwritten",@"Handwritten signature option button") action:@selector(btnSignHandwrittenTouch:)];
    UIMenuItem *menu_item_digital = [[UIMenuItem alloc] initWithTitle:NSLocalizedString(@"Digital", @"Digital signature option button") action:@selector(btnSignDigitalTouch:)];
    
    menuController.menuItems = [NSArray arrayWithObjects:menu_item_handwritten, menu_item_digital, nil];
    [menuController setTargetRect:CGRectMake(point.x, point.y, 2, 2) inView:_view_pdf];
    [menuController setMenuVisible:YES animated:YES];
}



@end
