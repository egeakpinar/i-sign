//
//  ViewPDFVC.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 05/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VC.h"

@interface ViewPDFVC : VC

#pragma mark - Public properties
@property(nonatomic, copy) NSURL *pdf_url;
@property(nonatomic, copy) NSString *pdf_name;
@property(nonatomic, assign) BOOL should_hide_toast;

@end
