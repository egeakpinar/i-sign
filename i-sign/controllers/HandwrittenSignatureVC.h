//
//  HandwrittenSignatureVC.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"

@protocol HandwrittenSignatureParent <NSObject>

- (void) receiveSignature:(UIImage *)signature;

@end

@interface HandwrittenSignatureVC : VC

@property(nonatomic, retain) id<HandwrittenSignatureParent> delegate;

@end
