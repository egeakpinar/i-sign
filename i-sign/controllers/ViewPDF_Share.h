//
//  ViewPDF_Share.h
//  i-sign
//
//  Encapsulates share functionalities of ViewPDFVC (for cleaner code)
//
//  Created by Ismail Ege AKPINAR on 16/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ID_PDFView.h"
#import "ViewPDFVC.h"

@interface ViewPDF_Share : NSObject

+ (ViewPDF_Share *) sharedWithParent:(ViewPDFVC *)vc_parent;

- (void) btnSaveLocalTouchWithPDFView:(ID_PDFView *)pdf_view PDFBuffer:(NSData *)buffer_output_digital;
- (void) btnEmailTouchWithPDFView:(ID_PDFView *)pdf_view PDFBuffer:(NSData *)buffer_output_digital;

@end
