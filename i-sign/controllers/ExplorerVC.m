//
//  ExplorerVC.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ExplorerVC.h"
#import "FileExplorer.h"
#import "Debug.h"
#import "Common.h"
#import "Inode.h"
#import "File.h"
#import "InodeCell.h"
#import "ViewPDFVC.h"
#import "UIViewController+MJPopupViewController.h"

#import "SettingsVC.h"

#import "GMGridView.h"
#import "GMGridViewCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ExplorerCell.h"
#import "ExplorerCell_Folder.h"
#import "GradientBackground.h"

#import "OLGhostAlertView.h"


@interface ExplorerVC ()<GMGridViewActionDelegate, GMGridViewDataSource, FileExplorerDelegate, UIActionSheetDelegate>    {
    UINib *nib_explorer_cell;
    UINib *nib_explorer_cell_folder;
    NSInteger _lastDeleteItemIndexAsked;
    GradientBackground *background_gradient;
    OLGhostAlertView *alert_ghost;
    SettingsVC *vc_settings;
}

@property(nonatomic, retain) NSMutableArray *arr_inodes;
@property(nonatomic, retain) ViewPDFVC *vc_view_pdf;
@property(nonatomic, assign) float grid_item_dimension;
@property(nonatomic, assign) ID_sort_by sort_by;
@property(nonatomic, assign) int position_selected_cell;

// Directory watching
@property(nonatomic, assign) BOOL should_reload;
@property(nonatomic, assign) BOOL already_subscribed;

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet GMGridView *grid_files;


@end

@implementation ExplorerVC

#pragma mark - Default methods

- (id) initWithRootFolder:(Folder *) rootFolder  {
    if(self = [super initWithNibName:@"Explorer" bundle:nil]) {
        if(!rootFolder) {
            _rootFolder = nil;
            self.title = @"i-Sign";
        }
        else    {
            _rootFolder = rootFolder;
            self.title = _rootFolder.name;
        }

        // Init instance variables
        nib_explorer_cell = [UINib nibWithNibName:@"ExplorerCell" bundle:nil];
        nib_explorer_cell_folder = [UINib nibWithNibName:@"ExplorerCell_Folder" bundle:nil];
        _sort_by = ID_sort_by_name;

    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.

    // Configure grid view
    [self viewWillLayoutSubviews];  // Bug fix: Otherwise, autoresizing doesn't start at the beginning
    _grid_files.backgroundColor = [UIColor clearColor];
    _grid_files.centerGrid = NO;
    _grid_files.actionDelegate = self;
    _grid_files.dataSource = self;

    // Have a nice background
//    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"back-gray-light"]]];
    [self.view setBackgroundColor:[UIColor whiteColor]];
    UIImageView *img_view_logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-notext"]];
    [img_view_logo setCenter:CGPointMake(self.view.frame.size.width/2.0f, self.view.frame.size.height/1.1f)];
    [img_view_logo setAutoresizingMask: UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin];
    [self.view addSubview:img_view_logo];
    [self.view sendSubviewToBack:img_view_logo];
    // Note: Gradient is added in viewWillLayoutSubviews

    // Set navigation bar
    
     // Right (Settings)
        // Icon is small so always use the high res one
    UIImage *img_gear = [UIImage imageNamed:@"icon-gear-white@2x"];
    UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setFrame:CGRectMake(0, 0, img_gear.size.width * 0.75f, img_gear.size.height * 0.75f)];
    [btn setBackgroundImage:img_gear forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(navbarRightTouch:) forControlEvents:UIControlEventTouchUpInside];
        // Insert space for padding
    UIBarButtonItem *bar_item1 = [[UIBarButtonItem alloc] initWithCustomView:btn];
    UIBarButtonItem *bar_item2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    [bar_item2 setWidth:10.0f];
    self.navigationItem.rightBarButtonItems = [NSArray arrayWithObjects:bar_item2, bar_item1, nil];
    
    if(!_rootFolder)    {
         // Left (Add)
            // Icon is small so always use the high res one
        UIImage *img_plus = [UIImage imageNamed:@"icon-plus-white@2x"];
        UIButton *btn_plus = [UIButton buttonWithType:UIButtonTypeCustom];
        [btn_plus setFrame:CGRectMake(0, 0, btn.frame.size.width, btn.frame.size.height)];
        [btn_plus setBackgroundImage:img_plus forState:UIControlStateNormal];
        [btn_plus addTarget:self action:@selector(navbarLeftTouch:) forControlEvents:UIControlEventTouchUpInside];
            // Insert space for padding
        UIBarButtonItem *bar_item_left1 = [[UIBarButtonItem alloc] initWithCustomView:btn_plus];
        UIBarButtonItem *bar_item_left2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        [bar_item_left2 setWidth:10.0f];
        self.navigationItem.leftBarButtonItems = [NSArray arrayWithObjects:bar_item_left2, bar_item_left1, nil];
    }
    
    // Set toolbar items
    UIBarButtonItem *space_left = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *space_right = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];

    NSMutableArray *arr_toolbar_titles = [[NSMutableArray alloc] init];
    [arr_toolbar_titles insertObject:NSLocalizedString(@"Name",@"Sorting files by Name") atIndex:ID_sort_by_name];
    [arr_toolbar_titles insertObject:NSLocalizedString(@"Date",@"Sorting files by Date") atIndex:ID_sort_by_date];
    [arr_toolbar_titles insertObject:NSLocalizedString(@"Size",@"Sorting files by Size") atIndex:ID_sort_by_size];
    [arr_toolbar_titles insertObject:NSLocalizedString(@"Type",@"Sorting files by Type") atIndex:ID_sort_by_type];

    UISegmentedControl *segmented_control = [[UISegmentedControl alloc] initWithItems:arr_toolbar_titles]
    ;
    [segmented_control setSegmentedControlStyle:UISegmentedControlStyleBar];
    [segmented_control setSelectedSegmentIndex:0];
    [segmented_control addTarget:self action:@selector(sortingChanged:) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *toolbar_item = [[UIBarButtonItem alloc] initWithCustomView:segmented_control];

    [self setToolbarItems:[NSArray arrayWithObjects:space_left, toolbar_item, space_right, nil]];

    // Initialise instance variables
    _should_reload = YES;
    _already_subscribed = NO;
    _position_selected_cell = -1;

    // Register for relevant notifications
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setting_file_extension_changed:) name:NOTIFICATION_FILE_EXTENSION_SETTING_CHANGED object:nil];
    
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];

    // Subscribe to FileExplorer changes
    if(!_already_subscribed) {
        [[FileExplorer shared] addDelegate:self];
        _already_subscribed = YES;
    }

    if(_should_reload)  {
        _arr_inodes = [[FileExplorer shared] getInodesUnderFolder:_rootFolder foldersOnly:NO sortBy:_sort_by];
        [_grid_files reloadData];
        _should_reload = NO;
    }

    // Un-highlight previously selected cell
    if(_position_selected_cell >= 0)    {
        ExplorerCell *cell = (ExplorerCell *)[_grid_files cellForItemAtIndex:_position_selected_cell];
        [cell.layer setShadowOpacity:0.0f];
        _position_selected_cell = -1;
    }

    [self.navigationController setToolbarHidden:NO animated:NO];
}

- (void) viewDidAppear:(BOOL)animated   {
    [super viewDidAppear:animated];

    if(_rootFolder) {
        alert_ghost = [[OLGhostAlertView alloc] initWithTitle:_rootFolder.name message:nil timeout:0.75f dismissible:YES];
        [alert_ghost show];
    }
}

- (void) viewWillDisappear:(BOOL)animated   {
    [super viewWillDisappear:animated];

    [alert_ghost hide];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setGrid_files:nil];
    [super viewDidUnload];
}

- (void) viewWillLayoutSubviews {
    int number_of_horizontal_items;
    int item_space_ratio = 4;
    if(IS_IPAD) {
        item_space_ratio = 2;
        if([Helper isLandscapeOrientation]) {
            number_of_horizontal_items = 5;
        }
        else    {
            number_of_horizontal_items = 4;
        }
    }
    else if(IS_IPHONE5) {
        if([Helper isLandscapeOrientation]) {
            number_of_horizontal_items = 4.5;
        }
        else    {
            number_of_horizontal_items = 3;
        }
    }
    else {
        if([Helper isLandscapeOrientation]) {
            number_of_horizontal_items = 4;
        }
        else    {
            number_of_horizontal_items = 3;
        }
    }
    float horizontal_space = self.view.frame.size.width;
    int number_of_units = (2 + (number_of_horizontal_items -1) + ((number_of_horizontal_items) * (item_space_ratio)));
    int unit1 = (int)(horizontal_space / (float)number_of_units);
    int product = unit1 * number_of_units;
    float difference = horizontal_space - product;
    float item_spacing = unit1;
    float border_spacing = unit1 + (difference /2.0f);
    _grid_files.minEdgeInsets = UIEdgeInsetsMake(border_spacing, border_spacing, border_spacing, border_spacing);
    _grid_files.itemSpacing = item_spacing;
    _grid_item_dimension = item_space_ratio * unit1;
    [_grid_files reloadData];

    // Add gradient to background
    if(background_gradient) {
        [background_gradient removeFromSuperview];
    }
    background_gradient = [[GradientBackground alloc] initWithFrame:self.view.frame];
    [self.view addSubview:background_gradient];
    [self.view sendSubviewToBack:background_gradient];
}

#pragma mark GMGridViewActionDelegate

- (void)GMGridView:(GMGridView *)gridView didTapOnItemAtIndex:(NSInteger)position   {
    // Highlight selected item
    GMGridViewCell *cell = [_grid_files cellForItemAtIndex:position];
    if(cell)    {
        [cell.layer setShadowColor:[UIColor whiteColor].CGColor];
        [cell.layer setShadowOpacity:0.5f];
        _position_selected_cell = position;
    }

    Inode *curr_inode = [_arr_inodes objectAtIndex:position];
    if([curr_inode isFolder])   {
        // Go one level deeper
        ExplorerVC *vc_explorer = [[ExplorerVC alloc] initWithRootFolder:(Folder*)curr_inode];
        [self.navigationController pushViewController:vc_explorer animated:YES];
    }
    else    {
        file_types type = ((File *)curr_inode).type;
        if(type == PDF) {
            if(!_vc_view_pdf)   {
                _vc_view_pdf = [[ViewPDFVC alloc] init];
            }
            [_vc_view_pdf setPdf_url:curr_inode.url];
            [_vc_view_pdf setPdf_name:curr_inode.name];
            [self.navigationController pushViewController:_vc_view_pdf animated:YES];
        }
        else    {
            logg(@"Selected file type is not supported");
        }
    }
}

- (void)GMGridViewDidTapOnEmptySpace:(GMGridView *)gridView
{
    NSLog(@"Tap on empty space");
}

#pragma mark GMGridViewDataSource

- (NSInteger)numberOfItemsInGMGridView:(GMGridView *)gridView
{
    return [_arr_inodes count];
}

- (CGSize)GMGridView:(GMGridView *)gridView sizeForItemsInInterfaceOrientation:(UIInterfaceOrientation)orientation
{
    return CGSizeMake(_grid_item_dimension, _grid_item_dimension * 1.5f);
}

- (GMGridViewCell *)GMGridView:(GMGridView *)gridView cellForItemAtIndex:(NSInteger)index
{
    Inode *current_inode = [_arr_inodes objectAtIndex:index];

    CGSize size = [self GMGridView:gridView sizeForItemsInInterfaceOrientation:[[UIApplication sharedApplication] statusBarOrientation]];

    GMGridViewCell *cell = [gridView dequeueReusableCell];

    if (!cell)  {
        cell = [[GMGridViewCell alloc] initWithFrame:CGRectMake(0, 0, size.width, size.height)];
    }

    ExplorerCell *explorer_cell = nil;
    if(current_inode.isFolder)  {
        explorer_cell = (ExplorerCell *)[[nib_explorer_cell_folder instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    else    {
        explorer_cell = (ExplorerCell *)[[nib_explorer_cell instantiateWithOwner:self options:nil] objectAtIndex:0];
    }
    [explorer_cell setFrame:CGRectMake(0, 0, size.width, size.height)];
    [explorer_cell setInode:current_inode];
    cell.contentView = explorer_cell;

    return cell;
}


- (BOOL)GMGridView:(GMGridView *)gridView canDeleteItemAtIndex:(NSInteger)index
{
    return NO;
}

#pragma mark - UI callbacks
- (void) navbarLeftTouch:(id) sender    {
    UIActionSheet *action_sheet_add = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Add", @"Title for 'Add menu' action sheet") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Cancel button in 'Add menu' action sheet") destructiveButtonTitle:nil otherButtonTitles:NSLocalizedString(@"My signature", @"Action sheet row in 'Add' menu"),NSLocalizedString(@"My initials", @"Action sheet row in 'Add' menu"), NSLocalizedString(@"Folder", @"Action sheet row in 'Add' menu"), NSLocalizedString(@"Remote storage", @"Action sheet row in 'Add' menu"), nil];
    action_sheet_add.tag = 1;
    [action_sheet_add showFromToolbar:self.navigationController.toolbar];
}

- (void) navbarRightTouch:(id) sender   {
    if(!vc_settings)   {
        vc_settings = [[SettingsVC alloc] initWithNibName:@"SettingsView" bundle:nil];
    }
    [self presentViewController:vc_settings animated:YES completion:nil];
}

- (void) sortingChanged:(UISegmentedControl *) control {
    _sort_by = (ID_sort_by)control.selectedSegmentIndex;
    _arr_inodes = [[FileExplorer shared] getInodesUnderFolder:_rootFolder foldersOnly:NO sortBy:_sort_by];
    [_grid_files reloadData];
}

#pragma mark - FileExplorerDelegate callbacks
- (void) directoryDidChange  {
    if(self.view.superview) {
        // View is visible, reload data now
        _arr_inodes = [[FileExplorer shared] getInodesUnderFolder:_rootFolder foldersOnly:NO sortBy:_sort_by];
        [_grid_files performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
    }
    else    {
        // View is not visible, mark for reload
        _should_reload = YES;
    }
}

#pragma mark - Notification observers

- (void) setting_file_extension_changed:(NSNotification *)notification  {
    [_grid_files reloadData];
}

#pragma mark - UIActionSheetDelegate methods

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex    {
    if(actionSheet.tag == 1)    {   // Add menu
        if(buttonIndex == [actionSheet cancelButtonIndex])  {
            return;
        }
        
        int index = buttonIndex - actionSheet.firstOtherButtonIndex;
        switch (index) {
            case 0:
                deb(@"My signature");
                break;
            case 1:
                deb(@"My initials");
                break;
            case 2:
                deb(@"Folder");
                break;
            case 3: {
                deb(@"Remote storage");
                // Show another action sheet with list of storage options
                UIActionSheet *action_sheet = [[UIActionSheet alloc] initWithTitle:NSLocalizedString(@"Select remote storage provider", @"Title for 'remote providers' menu") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"Action sheet row for cancelling 'remote providers' menu") destructiveButtonTitle:nil otherButtonTitles:@"Google", @"Dropbox", nil];
                action_sheet.tag = 2;
                [action_sheet showFromToolbar:self.navigationController.toolbar];
                break;
            }
            default:
                break;
        }
    }
}
@end