//
//  Debug.h
//  YAction
//
//  Created by Ismail Ege AKPINAR on 26/12/2012.
//  Copyright (c) 2013 i-DENTITY. All rights reserved.
//



#import <Foundation/Foundation.h>

@interface Debug : NSObject

+ (void) print_view_frame:(UIView *)view;
+ (void) print_frame:(CGRect)frame;
+ (void) print_point:(CGPoint)point;

@end
