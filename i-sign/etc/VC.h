//
//  VC.h
//  checkid
//  This is an abstract UIViewController extension, with general use properties and methods
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

// NOTE: These includes will make the app linking slower, you might want to remove them for production
#import "Debug.h"
#import "Helper.h"
#import "Common.h"

@interface VC : UIViewController

@end
