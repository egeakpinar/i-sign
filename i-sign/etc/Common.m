//
//  Common.m
//  YAction
//
//  Created by Ismail Ege AKPINAR on 06/01/2013.
//  Copyright (c) 2013 Quiet Riots. All rights reserved.
//

#import "Common.h"
#import "Debug.h"
#import "Helper.h"
#import <zlib.h>

@implementation Common

+ (BOOL) is_first_load  {
    NSUserDefaults *user_defaults = [NSUserDefaults standardUserDefaults];
    if(user_defaults && [user_defaults objectForKey:@"is_loaded"])  {
        return NO;
    }
    else {
        [user_defaults setObject:[NSNumber numberWithBool:YES] forKey:@"is_loaded"];
        if(![user_defaults synchronize])    {
            warn(@"Could not save is_loaded to user defaults");
        }
    }
    return YES;
}


+ (unsigned char *) stringToByteArray:(NSString *)input outputLength:(int *)outputLength    {
    unsigned char *output = NULL;
    
    const char *buf = [input UTF8String];
    NSMutableData *data = [NSMutableData data];
    if (buf)    {
        uint32_t len = strlen(buf);
		
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )    {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else    {
                break;
            }
        }
	}
    if(data && [data length] > 0)   {
        output = (unsigned char*) malloc(sizeof(unsigned char) * data.length);
        [data getBytes:output length:data.length];
    }
    else    {
        warn(@"Failed to get bytes from string ||%@||", input);
    }
    
    *outputLength = sizeof(unsigned char) * data.length;
    
    return output;
}

+ (NSData *) stringToData:(NSString *)input {
    unsigned char *output = NULL;
    
    const char *buf = [input UTF8String];
    NSMutableData *data = [NSMutableData data];
    if (buf)    {
        uint32_t len = strlen(buf);
		
        char singleNumberString[3] = {'\0', '\0', '\0'};
        uint32_t singleNumber = 0;
        for(uint32_t i = 0 ; i < len; i+=2)
        {
            if ( ((i+1) < len) && isxdigit(buf[i]) && (isxdigit(buf[i+1])) )    {
                singleNumberString[0] = buf[i];
                singleNumberString[1] = buf[i + 1];
                sscanf(singleNumberString, "%x", &singleNumber);
                uint8_t tmp = (uint8_t)(singleNumber & 0x000000FF);
                [data appendBytes:(void *)(&tmp) length:1];
            }
            else    {
                break;
            }
        }
	}
    if(data && [data length] > 0)   {
        return data;
    }
    else    {
        warn(@"Failed to get bytes from string ||%@||", input);
    }
    
    return nil;
}

+ (CGSize) pdfSize:(CGPDFPageRef) pdf   {
    CGRect pageRect = CGPDFPageGetBoxRect(pdf, kCGPDFMediaBox);
    return  pageRect.size;
}

+ (UIImage *)image:(UIImage *)img croppedAt:(CGRect)rect    {
    if (img.scale > 1.0f) {
        rect = CGRectMake(rect.origin.x * img.scale,
                          rect.origin.y * img.scale,
                          rect.size.width * img.scale,
                          rect.size.height * img.scale);
    }
    
    CGImageRef imageRef = CGImageCreateWithImageInRect(img.CGImage, rect);
    UIImage *result = [UIImage imageWithCGImage:imageRef scale:img.scale orientation:img.imageOrientation];
    CGImageRelease(imageRef);
    return result;
}

+ (NSData *) gzipData:(NSData *)input   {
	if ([input length] == 0) return input;
	
	z_stream strm;
	
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.total_out = 0;
	strm.next_in=(Bytef *)[input bytes];
	strm.avail_in = [input length];
	
	// Compresssion Levels:
	//   Z_NO_COMPRESSION
	//   Z_BEST_SPEED
	//   Z_BEST_COMPRESSION
	//   Z_DEFAULT_COMPRESSION
	
	if (deflateInit2(&strm, Z_DEFAULT_COMPRESSION, Z_DEFLATED, (15+16), 8, Z_DEFAULT_STRATEGY) != Z_OK) return nil;
	
	NSMutableData *compressed = [NSMutableData dataWithLength:16384];  // 16K chunks for expansion
	
	do {
		
		if (strm.total_out >= [compressed length])
			[compressed increaseLengthBy: 16384];
		
		strm.next_out = [compressed mutableBytes] + strm.total_out;
		strm.avail_out = [compressed length] - strm.total_out;
		
		deflate(&strm, Z_FINISH);
		
	} while (strm.avail_out == 0);
	
	deflateEnd(&strm);
	
	[compressed setLength: strm.total_out];
	return [NSData dataWithData:compressed];
}
@end
