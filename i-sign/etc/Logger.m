//
//  Logger.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 29/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Logger.h"

#import <Lumberjack/DDASLLogger.h>
#import <Lumberjack/DDTTYLogger.h>
#import <Lumberjack/DDFileLogger.h>

@interface Logger()<DDLogFileManager>

@property(nonatomic, retain) DDFileLogger *file_logger;

@end

@implementation Logger

static Logger *_instance;
+ (Logger *) shared   {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _instance = [Logger new];
    });
    return _instance;
}

- (void) start {
    // Initialise DDLog
    [DDLog addLogger:[DDASLLogger sharedInstance]];
    [DDLog addLogger:[DDTTYLogger sharedInstance]];
    _file_logger = [DDFileLogger new];
    [DDLog addLogger:_file_logger];
}

-(NSArray *) logFilePaths   {
    return [_file_logger.logFileManager unsortedLogFilePaths];
}

#pragma mark - DDLogFileManager callbacks

@end
