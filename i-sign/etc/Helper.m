//
//  Helper.m
//
//  Created by Ismail Ege AKPINAR on 06/11/2012.
//

#import "Helper.h"
#import "Debug.h"

@implementation Helper

#pragma mark - UI methods

static UIActivityIndicatorView *activity_indicator_view;
static UIView *activity_background_view;
+(void) show_activity_indicator {
    if(!activity_indicator_view)    {
        activity_indicator_view = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        activity_indicator_view.hidesWhenStopped = YES;
        
        activity_background_view = [[UIView alloc] initWithFrame:CGRectMake(0, [Helper status_bar_height], [Helper screen_width], [Helper screen_height])];
        [activity_background_view setUserInteractionEnabled:NO];
        [activity_background_view setBackgroundColor:[UIColor colorWithRed:1.0f green:1.0f blue:1.0f alpha:0.5f]];
    }

    activity_indicator_view.frame = CGRectMake(round(([Helper screen_width] - 25) / 2), round(([Helper screen_height] - 25) / 2), 25, 25);

    [[[[UIApplication sharedApplication] delegate] window] addSubview:activity_background_view];    
    [[[[UIApplication sharedApplication] delegate] window] addSubview:activity_indicator_view];
    [activity_indicator_view startAnimating];
}

// Make sure you call it from main thread for fastest effect
+(void) hide_activity_indicator {
    if(!activity_indicator_view)    {
        err(@"Trying to hide an uninitialized activity indicator");
    }
    else if(![activity_indicator_view isAnimating])  {
        warn(@"Trying to hide an idle activity indicator");
    }
    [activity_indicator_view stopAnimating];
    [activity_indicator_view removeFromSuperview];
    [activity_background_view removeFromSuperview];
}

+ (void) addSubviewToTop:(UIView *)view {
    int count = [[[UIApplication sharedApplication] windows] count];
    if(count <= 0)  {
        warn(@"addSubviewToTop failed to access application windows");
    }
    UIWindow *top_window = [[[UIApplication sharedApplication] windows] objectAtIndex:count-1];
    [top_window addSubview:view];
    [top_window bringSubviewToFront:view];
}

#pragma mark - String methods

+ (BOOL) string:(NSString *)string contains:(NSString *)substring  {
    NSRange aRange = [string rangeOfString:substring];
    if (aRange.location ==NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

+ (BOOL) string:(NSString *)string containsi:(NSString *)substring  {
    string = [string lowercaseString];
    substring = [substring lowercaseString];
    NSRange aRange = [string rangeOfString:substring];
    if (aRange.location ==NSNotFound) {
        return NO;
    } else {
        return YES;
    }
}

+ (NSString *) string_trimmed:(NSString *)input     {   // Trims white spaces at both ends of given string
    return [input stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

// Replaces blanks with underscore, converts to lowercase and trims
+ (NSString *) stringFixed:(NSString *)input    {
    NSString *res = [input lowercaseString];
    res = [Helper string_trimmed:res];
    res = [res stringByReplacingOccurrencesOfString:@" " withString:@"_"];
    return res;
}

+ (BOOL) stringValid:(NSString *)str    {
    if(!str || [str isEqualToString:@""])   {
        return NO;
    }
    else if([[Helper string_trimmed:str] isEqualToString:@""])    {
        return NO;
    }
    else    {
        // Iterate all characters to see if it has a non-white space character
        // TODO: Do it later
    }
    return YES;
}
#pragma mark - Layout methods

+ (CGFloat) status_bar_height   {
    float device_height = [UIScreen mainScreen].bounds.size.height;
    float app_screen_height = [UIScreen mainScreen].applicationFrame.size.height;
    return device_height - app_screen_height;
}


+ (CGFloat) screen_height   {
    return [UIScreen mainScreen].applicationFrame.size.height;
}

+ (CGFloat) screen_width   {
    return [UIScreen mainScreen].applicationFrame.size.width;
}

+ (CGFloat) device_height   {
    return [UIScreen mainScreen].bounds.size.height;
}

+ (CGFloat) device_width   {
    return [UIScreen mainScreen].bounds.size.width;
}

+ (CGFloat) keyboard_height {
    return 216.0f;
}

+ (CGFloat) navbar_height   {
    return 44.0f;
}

+ (CGFloat) tabbar_height   {
    return 49.0f;
}

+ (CGFloat) toolbarHeight   {
    return 44.0f;
}

+ (BOOL) isLandscapeOrientation {
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    return UIInterfaceOrientationIsLandscape(orientation);
}

#pragma mark - Misc. methods

+(double) get_timestamp {
    return [[NSDate date] timeIntervalSince1970];
}


+ (NSString *) month_to_string:(int)month   {
    switch (month) {
        case 1:
            return @"Jan";
        case 2:
            return @"Feb";
        case 3:
            return @"Mar";
        case 4:
            return @"Apr";
        case 5:
            return @"May";
        case 6:
            return @"Jun";
        case 7:
            return @"Jul";
        case 8:
            return @"Aug";
        case 9:
            return @"Sep";
        case 10:
            return @"Oct";
        case 11:
            return @"Nov";
        case 12:
            return @"Dec";
        default:
            err(@"Unexpected month index %d", month);
            return nil;
    }
}

+ (void)save_image:(UIImage*)image with_name:(NSString*)imageName {
    NSData *imageData = UIImagePNGRepresentation(image); //convert image into .png format.
    
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
    
    NSString *fullPath = [documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png", imageName]]; //add our image to the path
    
    //    logg(@"saving to %@", fullPath);
    if(![fileManager createFileAtPath:fullPath contents:imageData attributes:nil])  {
        err(@"Failed to save");
    }
}

+(void) display_message_with_title:(NSString *)title message:(NSString *)message and_delegate:(id<UIAlertViewDelegate>) delegate    {
    if(message == nil || [message isEqualToString:@""])  message = @"Unknown error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

+(void) display_message_with_title:(NSString *)title message:(NSString *)message and_delegate:(id<UIAlertViewDelegate>) delegate  tag:(int)tag  {
    if(message == nil || [message isEqualToString:@""])  message = @"Unknown error";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = tag;
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}

+(void) displayMessageWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>) delegate tag:(int)tag buttonTitle:(NSString *)button_title    {
    if(message == nil || [message isEqualToString:@""])  message = @"Unknown error";
    if(button_title == nil || [button_title isEqualToString:@""])  button_title = @"OK";
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:message delegate:delegate cancelButtonTitle:button_title otherButtonTitles:nil, nil];
    alert.tag = tag;
    
    [alert performSelectorOnMainThread:@selector(show) withObject:nil waitUntilDone:NO];
}


#pragma mark - File management
+ (NSString *) documentsPath    {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}

+ (BOOL) saveBuffer:(NSData *)data toPath:(NSString *)path  {
    NSFileManager *fileManager = [NSFileManager defaultManager];//create instance of NSFileManager
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    
    if(![fileManager createFileAtPath:path contents:data attributes:nil])  {
        err(@"Failed to save");
        return NO;
    }
    return YES;
}

@end
