//
//  SignatureHandler1.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 12/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "SignatureHandler2.h"
#import "Debug.h"

#import <PDFTron/Headers/ObjC/PDFNetOBJC.h>

// OpenSSL includes
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/ssl.h>
#import <openssl/engine.h>

// Override SignatureHandler
@interface SignatureHandler2 ()  {
    SHA_CTX m_sha_ctx;
    EVP_PKEY* mp_pkey;      // private key
    X509* mp_x509;          // signing certificate
    STACK_OF(X509)* mp_ca;  // certificate chain up to the CA
}

- (NSString*) GetName;
- (void) AppendData: (NSData*)data;
- (BOOL) Reset;
- (NSData*) CreateSignature;
- (id) init: (NSString*) pfxfile password: (NSString*) password;
- (void) dealloc;

@end // interface PKCS7SignatureHandler

@implementation SignatureHandler2
- (NSString*) GetName
{
    return (@"Adobe.PPKLite");
}
- (void) AppendData: (NSData*)data
{
    SHA1_Update(&m_sha_ctx, [data bytes], [data length]);
    return;
}
- (BOOL) Reset
{
    SHA1_Init(&m_sha_ctx);
    return (YES);
}
- (NSData*) CreateSignature
{
    unsigned char sha_buffer[SHA_DIGEST_LENGTH];
    memset((void*) sha_buffer, 0, SHA_DIGEST_LENGTH);
    SHA1_Final(sha_buffer, &m_sha_ctx);
    
    PKCS7* p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);
    
    PKCS7_SIGNER_INFO* p7Si = PKCS7_add_signature(p7, mp_x509, mp_pkey, EVP_sha1());
    PKCS7_add_attrib_content_type(p7Si, OBJ_nid2obj(NID_pkcs7_data));
    PKCS7_add0_attrib_signing_time(p7Si, NULL);
    PKCS7_add1_attrib_digest(p7Si, (const unsigned char*) sha_buffer, SHA_DIGEST_LENGTH);
    PKCS7_add_certificate(p7, mp_x509);
    
    int c = 0;
    deb(@"number of certificates %d", sk_X509_num(mp_ca));
    for ( ; c < sk_X509_num(mp_ca); c++) {
        X509* cert = sk_X509_value(mp_ca, c);
        PKCS7_add_certificate(p7, cert);
    }
    PKCS7_set_detached(p7, 1);
    PKCS7_content_new(p7, NID_pkcs7_data);
    
    PKCS7_SIGNER_INFO_sign(p7Si);
    
    int p7Len = i2d_PKCS7(p7, NULL);
    NSMutableData* signature = [NSMutableData data];
    unsigned char* p7Buf = (unsigned char*) malloc(p7Len);
    if (p7Buf != NULL) {
        unsigned char* pP7Buf = p7Buf;
        i2d_PKCS7(p7, &pP7Buf);
        [signature appendBytes: (const void*) p7Buf length: p7Len];
        free(p7Buf);
    }
    PKCS7_free(p7);
    
    return (signature);
}

static int(^signBlock)(int type, unsigned char *m, unsigned int m_len,
                       unsigned char *sigret, unsigned int *siglen, RSA *rsa) = nil;
int rsa_sign (int type, unsigned char *m, unsigned int m_len,
              unsigned char *sigret, unsigned int *siglen, RSA *rsa) {
    
    return signBlock(type, m, m_len, sigret, siglen, rsa);
    
    
}

- (id) init {
    if(self = [super init]) {
        
        // Intervention
        signBlock = ^int(int type, unsigned char *m, unsigned int m_len, unsigned char *sigret, unsigned int *siglen, RSA *rsa) {
            NSData *toSign = [NSData dataWithBytes:m length:m_len];
            NSLog(@"to sign %@", toSign);
            deb(@"overriden sign method executed");
            
            NSInteger status;
            NSData *signature = [_card signData:toSign];
            [signature getBytes:sigret length:[signature length]];
            *siglen = [signature length];
            return 1;
        };
        
        OpenSSL_add_all_algorithms();
        OpenSSL_add_all_ciphers();
        OpenSSL_add_all_digests();
        SSL_load_error_strings();
        
        // Override default signing method of RSA
        RSA_METHOD* method = (RSA_METHOD*)RSA_get_default_method();
        
        ENGINE_load_cryptodev();
        ENGINE_load_openssl();
        ENGINE* eng = ENGINE_by_id("openssl");
        
        method->flags |= RSA_FLAG_SIGN_VER;
        method->flags |= RSA_METHOD_FLAG_NO_CHECK;
        method->rsa_sign = (int (*)(int type,
                                    const unsigned char *m, unsigned int m_length,
                                    unsigned char *sigret, unsigned int *siglen, const RSA *rsa))rsa_sign;
        
        
        ENGINE_set_RSA(eng, method);
        
        // e: Intervention
        
        
        NSString *pfxfile = [[NSBundle mainBundle] URLForResource:@"pdftron" withExtension:@"pfx"].path;
        NSString *password = @"password";
        
        FILE* fp = fopen([pfxfile cStringUsingEncoding: NSASCIIStringEncoding], "rb");
        if (fp == NULL)
            @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot open private key." userInfo: nil]);
        
        PKCS12* p12 = d2i_PKCS12_fp(fp, NULL);
        fclose(fp);
        
        if (p12 == NULL)
            @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);
        
        mp_pkey = NULL;
        mp_x509 = NULL;
        mp_ca = NULL;
        int parseResult = PKCS12_parse(p12, [password cStringUsingEncoding: NSASCIIStringEncoding], &mp_pkey, &mp_x509, &mp_ca);
        PKCS12_free(p12);
        
        if (parseResult == 0)
            @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);
        
        [self Reset];
        
        // Intervention
        
        // Populate these structs in the card
        [_card getPKCS7:nil];
        mp_pkey = _card.mp_pkey;
        mp_x509 = _card.mp_x509;
//        mp_ca = _card.mp_ca;
        if(!mp_ca || !mp_pkey || !mp_x509)  {
            err(@"Failed to populate structs!");
        }
        deb(@"Yep, populated structs");
        
        // e: Intervention
    }
    
    return self;
}
- (void) dealloc
{
    sk_X509_free(mp_ca);
    X509_free(mp_x509);
    EVP_PKEY_free(mp_pkey);
}
@end // implementation PKCS7SignatureHandler