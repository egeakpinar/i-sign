//
//  CustomSignatureHandler.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 10/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

// OpenSSL includes
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>


#import <PDFTron/Headers/Objc/PDFNetOBJC.h>

@interface CustomSignatureHandler : SignatureHandler

#pragma mark - Public properties
@property(nonatomic, retain) NSData *data_signature;

- (id) initWithPrivateKey:(EVP_PKEY *)mp_pkey andX509Certificate:(X509 *)mp_x509 andStack:(STACK_OF(X509)*) mp_ca;
@end
