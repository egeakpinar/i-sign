//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2012 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------
// The following sample demonstrates the basic usage of PDFNetC's digital signature API. In order to add a digital
// signature to a PDF using PDFNetC, the following steps must be performed:
//
//     1. Extend and implement a SignatureHandler which will be used for both adding and verifying a digital signature
//        for a PDF file.
//     1. Initialize PDFNet by calling PDFNet::Initialize.
//     2. Create or load a PDF using the PDFDoc class.
//     3. Create an instance of the implemented SignatureHandler in the heap. It is important that this SignatureHandler
//        remains alive throughout the PDFDoc's livelihood as the signing process will not happen until the document is
//        serialized. PDFNet will be responsible for freeing and destroying this SignatureHandler instance.
//     4. Add the SignatureHandler instance to PDFDoc by calling PDFDoc::AddSignatureHandler. At this point, make sure
//        that this SignatureHandler instance remains alive until the PDFDoc is destroyed. PDFDoc::AddSignatureHandler
//        returns an ID that can be used to indicate that this SignatureHandler will be used to sign a field.
//     5. Create a form field for the PDFDoc. Make sure this form field is of type e_signature.
//     6. Call the method Field::UseSignatureHandler to assign a SignatureHandler to use for signing this field.
//     7. Once the SignatureHandler is assigned, serializing the PDFDoc will also sign the PDF. This can be done by
//        saving the PDFDoc to memory or disk (using PDFDoc::Save method).
//
// Additional processing can be done between steps 6 and 7. The call to Field::UseSignatureHandler returns an instance
// of SDF dictionary which represents the signature dictionary (or the /V entry of the form field). It is possible to
// add more information to this signature dictionary like Name, Reason, Location, etc.
//
//----------------------------------------------------------------------------------------------------------------------

// To build and run this sample, please specify OpenSSL include & lib paths to the Makefile
//
// If OpenSSL is installed elsewhere, it may be necessary to add the path to the headers in the $(INCLUDE) variable as
// well as the location of either libcrypto.a or libcrypto.so/libcrypto.dylib.

// Note for iOS development: This code can be used to digitally sign PDFs in iOS devices. When using the code signing
// part of this code, it will be necessary to compile OpenSSL for iOS.

#ifdef OPENSSL_PATH_NOT_SET
#error "OpenSSL include and library directories not set in Makefile. Project requires OpenSSL v1.0.1+."
#endif // OPENSSL_PATH_NOT_SET

#import <CoreFoundation/CoreFoundation.h>

#import "PDFTron/Headers/ObjC/PDFNetOBJC.h"

// OpenSSL includes
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>

// Override SignatureHandler
@interface PKCS7SignatureHandler : SignatureHandler
{
    SHA_CTX m_sha_ctx;
    EVP_PKEY* mp_pkey;      // private key
    X509* mp_x509;          // signing certificate
    STACK_OF(X509)* mp_ca;  // certificate chain up to the CA
}
- (NSString*) GetName;
- (void) AppendData: (NSData*)data;
- (BOOL) Reset;
- (NSData*) CreateSignature;
- (id) init: (NSString*) pfxfile password: (NSString*) password;
- (void) dealloc;
@end // interface PKCS7SignatureHandler

@implementation PKCS7SignatureHandler
- (NSString*) GetName
{
    return (@"Adobe.PPKLite");
}
- (void) AppendData: (NSData*)data
{
    SHA1_Update(&m_sha_ctx, [data bytes], [data length]);
    return;
}
- (BOOL) Reset
{
    SHA1_Init(&m_sha_ctx);
    return (YES);
}
- (NSData*) CreateSignature
{
    unsigned char sha_buffer[SHA_DIGEST_LENGTH];
    memset((void*) sha_buffer, 0, SHA_DIGEST_LENGTH);
    SHA1_Final(sha_buffer, &m_sha_ctx);
    
    PKCS7* p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);
    
    PKCS7_SIGNER_INFO* p7Si = PKCS7_add_signature(p7, mp_x509, mp_pkey, EVP_sha1());
    PKCS7_add_attrib_content_type(p7Si, OBJ_nid2obj(NID_pkcs7_data));
    PKCS7_add0_attrib_signing_time(p7Si, NULL);
    PKCS7_add1_attrib_digest(p7Si, (const unsigned char*) sha_buffer, SHA_DIGEST_LENGTH);
    PKCS7_add_certificate(p7, mp_x509);
    
    int c = 0;
    for ( ; c < sk_X509_num(mp_ca); c++) {
        X509* cert = sk_X509_value(mp_ca, c);
        PKCS7_add_certificate(p7, cert);
    }
    PKCS7_set_detached(p7, 1);
    PKCS7_content_new(p7, NID_pkcs7_data);
    
    PKCS7_SIGNER_INFO_sign(p7Si);
    
    int p7Len = i2d_PKCS7(p7, NULL);
    NSMutableData* signature = [NSMutableData data];
    unsigned char* p7Buf = (unsigned char*) malloc(p7Len);
    if (p7Buf != NULL) {
        unsigned char* pP7Buf = p7Buf;
        i2d_PKCS7(p7, &pP7Buf);
        [signature appendBytes: (const void*) p7Buf length: p7Len];
        free(p7Buf);
    }
    PKCS7_free(p7);
    
    return (signature);
}
- (id) init: (NSString*) pfxfile password: (NSString*) password;
{    
    self = [super init];
    FILE* fp = fopen([pfxfile cStringUsingEncoding: NSASCIIStringEncoding], "rb");
    if (fp == NULL)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot open private key." userInfo: nil]);
    
    PKCS12* p12 = d2i_PKCS12_fp(fp, NULL);
    fclose(fp);
    
    if (p12 == NULL)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);
    
    mp_pkey = NULL;
    mp_x509 = NULL;
    mp_ca = NULL;
    int parseResult = PKCS12_parse(p12, [password cStringUsingEncoding: NSASCIIStringEncoding], &mp_pkey, &mp_x509, &mp_ca);
    PKCS12_free(p12);
    
    if (parseResult == 0)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);
    
    [self Reset];
    
    return (self);
}
- (void) dealloc
{
    sk_X509_free(mp_ca);
    X509_free(mp_x509);
    EVP_PKEY_free(mp_pkey);
}
@end // implementation PKCS7SignatureHandler

//
// This functions add an approval signature to the PDF document. The original PDF document contains a blank form field
// that is prepared for a user to sign. The following code demonstrate how to sign this document using PDFNet.
//
BOOL SignPDF()
{
    BOOL result = YES;
    NSString* infile = @"../../TestFiles/doc_to_sign.pdf";
    NSString* outfile = @"../../TestFiles/Output/signed_doc.pdf";
    NSString* certfile = @"../../TestFiles/pdftron.pfx";
    NSString* imagefile = @"../../TestFiles/signature.jpg";
    
    @try {
        NSLog(@"Signing PDF document: '%@'.", infile);
        
        // Open an existing PDF
        PDFDoc* doc = [[PDFDoc alloc] initWithFilepath: infile];
        
        // Create a new instance of the SignatureHandler.
        PKCS7SignatureHandler* pdftronSigHandler = [[PKCS7SignatureHandler alloc] init: certfile password: @"password"];
        
        // Add the SignatureHandler instance to PDFDoc, making sure to keep track of it using the ID returned.
        SignatureHandlerId sigHandlerId = [doc AddSignatureHandler: pdftronSigHandler];
        
        // Obtain the signature form field from the PDFDoc via Annotation.
        TRNField* sigField = [doc GetField: @"Signature1"];
//        Widget* widgetAnnot = [[Widget alloc] initWidgetWithSDFObj: [sigField GetSDFObj]];    egee
        Widget* widgetAnnot = [[Widget alloc] initWithD:[sigField GetSDFObj]];
        // Tell PDFNetC to use the SignatureHandler created to sign the new signature form field.
        Obj* sigDict = [sigField UseSignatureHandler: sigHandlerId];
        
        // Add more information to the signature dictionary.
        [sigDict PutName: @"SubFilter" name: @"adbe.pkcs7.detached"];
        [sigDict PutString: @"Name" value: @"PDFTron"];
        [sigDict PutString: @"Location" value: @"Vancouver, BC"];
        [sigDict PutString: @"Reason" value: @"Document verification."];
        
        // Add the signature appearance
        ElementWriter* apWriter = [[ElementWriter alloc] init];
        ElementBuilder* apBuilder = [[ElementBuilder alloc] init];
        [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
        Image* sigImg = [Image CreateWithFile: [doc GetSDFDoc] filename: imagefile encoder_hints: [[Obj alloc]init]];
        double w = [sigImg GetImageWidth], h = [sigImg GetImageHeight];
        
        Element* apElement = [apBuilder CreateImageWithCornerAndScale: sigImg x: 0 y: 0 hscale: w vscale: h];
        [apWriter WritePlacedElement: apElement];
        Obj* apObj = [apWriter End];
        [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
        [apObj PutName: @"Subtype" name: @"Form"];
        [apObj PutName: @"Type" name: @"XObject"];
        [apWriter WriterBeginWithSDFDoc: [doc GetSDFDoc] compress: YES];
        apElement = [apBuilder CreateFormWithObj: apObj];
        [apWriter WritePlacedElement: apElement];
        apObj = [apWriter End];
        [apObj PutRect: @"BBox" x1: 0 y1: 0 x2: w y2: h];
        [apObj PutName: @"Subtype" name: @"Form"];
        [apObj PutName: @"Type" name: @"XObject"];
        [widgetAnnot SetAppearance: apObj annot_state: e_normal app_state: nil];
        [widgetAnnot RefreshAppearance];
        
        Obj* widgetObj = [widgetAnnot GetSDFObj];
        [widgetObj PutNumber: @"F" value: 132];
        [widgetObj PutName: @"Type" name: @"Annot"];
        
        // Save the PDFDoc. Once the method below is called, PDFNetC will also sign the document using the information
        // provided.
        [doc SaveToFile: outfile flags: 0];
        
        NSLog(@"Finished signing PDF document.");
    }
    @catch (NSException* e) {
        NSLog(@"Exception: %@ - %@\n", [e name], [e reason]);
        result = NO;
    }
    
    return result;
}

//
// Adds a certification signature to the PDF document. Certifying a document is like notarizing a document. Unlike
// approval signatures, there can be only one certification per PDF document. Only the first signature in the PDF
// document can be used as the certification signature. The process of certifying a document is almost exactly the same
// as adding approval signatures with the exception of certification signatures requires an entry in the "Perms"
// dictionary.
//
BOOL CertifyPDF()
{
    BOOL result = YES;
    NSString* infile = @"../../TestFiles/newsletter.pdf";
    NSString* outfile = @"../../TestFiles/Output/newsletter_certified.pdf";
    NSString* certfile = @"../../TestFiles/pdftron.pfx";
    
    @try {
        NSLog(@"Certifying PDF document: '%@',", infile);
        
        // Open an existing PDF
        PDFDoc* doc = [[PDFDoc alloc] initWithFilepath: infile];
        
        // Create a new instance of the SignatureHandler.
        PKCS7SignatureHandler* pdftronSigHandler = [[PKCS7SignatureHandler alloc] init: certfile password: @"password"];
        
        // Add the SignatureHandler instance to PDFDoc, making sure to keep track of it using the ID returned.
        SignatureHandlerId sigHandlerId = [doc AddSignatureHandler: pdftronSigHandler];
        
        // Create new signature form field in the PDFDoc.
        TRNField* sigField = [doc FieldCreateWithString: @"Signature1" type: e_signature field_value: @"" def_field_value: @""];
        
        // Assign the form field as an annotation widget to the PDFDoc so that a signature appearance can be added.
        Page* page1 = [doc GetPage: 1];
        Widget* widgetAnnot = [Widget Create: [doc GetSDFDoc] pos: [[PDFRect alloc] initWithX1: 20 y1: 600 x2: 220 y2: 640] field: sigField];
        [page1 AnnotPushBack: widgetAnnot];
        [widgetAnnot SetPage: page1];
        Obj* widgetObj = [widgetAnnot GetSDFObj];
        [widgetObj PutNumber: @"F" value: 132];
        [widgetObj PutName: @"Type" name: @"Annot"];
        
        // Tell PDFNetC to use the SignatureHandler created to sign the new signature form field.
        Obj* sigDict = [sigField UseSignatureHandler: sigHandlerId];
        
        // Add more information to the signature dictionary.
        [sigDict PutName: @"SubFilter" name: @"adbe.pkcs7.detached"];
        [sigDict PutString: @"Name" value: @"PDFTron"];
        [sigDict PutString: @"Location" value: @"Vancouver, BC"];
        [sigDict PutString: @"Reason" value: @"Document verification."];
        
        // Appearance can be added to the widget annotation. Please see the "SignPDF()" function for details.
        
        // Add this sigDict as DocMDP in Perms dictionary from root
        Obj* root = [doc GetRoot];
        Obj* perms = [root PutDict: @"Perms"];
        // add the sigDict as DocMDP (indirect) in Perms
        [perms Put:@"DocMDP" obj:sigDict];
//        [perms Put:@"DocMDP" value:sigDict];  egee
        
        // add the additional DocMDP transform params
        Obj* refObj = [sigDict PutArray: @"Reference"];
        Obj* transform = [refObj PushBackDict];
        [transform PutName: @"TransformMethod" name: @"DocMDP"];
//        [transform PutName: @"TransformMethod" value: @"DocMDP"]; egee
        [transform PutName: @"Type" name: @"SigRef"];
//        [transform PutName: @"Type" value: @"SigRef"];            egee
        Obj* transformParams = [transform PutDict: @"TransformParams"];
        [transformParams PutNumber: @"P" value: 1]; // Set permissions as necessary.
        [transformParams PutName: @"Type" name: @"TransformParams"];
        [transformParams PutName: @"V" name: @"1.2"];
//        [transformParams PutName: @"Type" value: @"TransformParams"]; egee
//        [transformParams PutName: @"V" value: @"1.2"];    egee
        
        // Save the PDFDoc. Once the method below is called, PDFNetC will also sign the document using the information
        // provided.
        [doc SaveToFile: outfile flags: 0];
        
        NSLog(@"Finished certifying PDF document.");
    }
    @catch (NSException* e) {
        NSLog(@"Exception: %@ - %@\n", [e name], [e reason]);
        result = NO;
    }
    
    return result;
}
/*
int main()   {
//    NSAutoreleasePool* pool = [[NSAutoreleasePool alloc] init];
    
    // Initialize PDFNetC
    [PDFNet Initialize: 0];
    
    // Initialize OpenSSL library
    CRYPTO_malloc_init();
    ERR_load_crypto_strings();
    OpenSSL_add_all_algorithms();
    
    BOOL result = YES;
    
    if (!SignPDF())
        result = NO;
    
    if (!CertifyPDF())
        result = NO;
    
    // Release OpenSSL resource usage
    ERR_free_strings();
    EVP_cleanup();
    
//    [pool release];
    
    if (!result) {
        NSLog(@"Tests failed.");
        return 1;
    }
    
    NSLog(@"All tests passed.");
    
    return 0;
}
*/
