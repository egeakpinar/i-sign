//
//  SignatureHandler2.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 12/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "PDFNetOBJC.h"
#import "Card_eid.h"
#import "CardReader.h"

@interface SignatureHandler2 : SignatureHandler

@property(nonatomic, retain) Card_eid *card;
@property(nonatomic, retain) CardReader *reader;


@end
