//
//  PKCS7SignatureHandler.h
//  SignPDF1
//
//  Created by Ismail Ege AKPINAR on 11/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "PDFNetOBJC.h"

@interface PKCS7SignatureHandler : SignatureHandler

- (id) init: (NSString*) pfxfile password: (NSString*) password;

@end
