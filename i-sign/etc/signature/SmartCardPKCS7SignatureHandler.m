//
//  SmartCardPKCS7SignatureHandler.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 22/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "SmartCardPKCS7SignatureHandler.h"

#if !(TARGET_IPHONE_SIMULATOR)
// OpenSSL includes
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/pkcs12.h>
#include <openssl/pkcs7.h>
#include <openssl/rsa.h>
#include <openssl/sha.h>
#include <openssl/engine.h>

#import <SAM/SAM.h>

@interface SmartCardPKCS7SignatureHandler() {
    SHA_CTX m_sha_ctx;
    EVP_PKEY* mp_pkey;      // private key
    X509* mp_x509;          // signing certificate
    STACK_OF(X509)* mp_ca;  // certificate chain up to the CA
    NSString *pin;
    int progress_current;
}

@property(nonatomic, retain) id<SmartCardPKCS7SignatureHandlerDelegate>delegate;

@end

@implementation SmartCardPKCS7SignatureHandler

static int(^signBlock)(int type, unsigned char *m, unsigned int m_len,
                       unsigned char *sigret, unsigned int *siglen, RSA *rsa) = nil;

int rsa_sign (int type, unsigned char *m, unsigned int m_len,
              unsigned char *sigret, unsigned int *siglen, RSA *rsa) {

    return signBlock(type, m, m_len, sigret, siglen, rsa);


}

static NSData *pkcs7;

- (NSString*) GetName
{
    return (@"Adobe.PPKLite");
}

- (void) AppendData: (NSData*)d
{
    SHA1_Update(&m_sha_ctx, [d bytes], [d length]);
    return;
}

- (BOOL) Reset
{
    NSLog(@"Reset");
    SHA1_Init(&m_sha_ctx);

    return (YES);
}



PKCS7_SIGNER_INFO *_PKCS7_add_signature(PKCS7 *p7, X509 *x509, EVP_PKEY *pkey,
                                        const EVP_MD *dgst)
{
	PKCS7_SIGNER_INFO *si = NULL;

	if (dgst == NULL)
    {
		int def_nid;
		if (EVP_PKEY_get_default_digest_nid(pkey, &def_nid) <= 0)
			goto err;
		dgst = EVP_get_digestbynid(def_nid);
		if (dgst == NULL)
        {
			PKCS7err(PKCS7_F_PKCS7_ADD_SIGNATURE,
                     PKCS7_R_NO_DEFAULT_DIGEST);
			goto err;
        }
    }

	if ((si=PKCS7_SIGNER_INFO_new()) == NULL) goto err;

	if (!PKCS7_SIGNER_INFO_set(si,x509,pkey,dgst)) goto err;

	if (!PKCS7_add_signer(p7,si)) goto err;
	return(si);
err:
	if (si)
		PKCS7_SIGNER_INFO_free(si);
	return(NULL);
}

- (NSData*) CreateSignature
{
    NSLog(@"CreateSignature");
    unsigned char sha_buffer[SHA_DIGEST_LENGTH];
    memset((void*) sha_buffer, 0, SHA_DIGEST_LENGTH);
    SHA1_Final(sha_buffer, &m_sha_ctx);


    PKCS7* p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);
    PKCS7_SIGNER_INFO* p7Si = _PKCS7_add_signature(p7, mp_x509, mp_pkey, EVP_sha1());
    PKCS7_add_attrib_content_type(p7Si, OBJ_nid2obj(NID_pkcs7_data));
    PKCS7_add0_attrib_signing_time(p7Si, NULL);
    PKCS7_add1_attrib_digest(p7Si, (const unsigned char*) sha_buffer, SHA_DIGEST_LENGTH);
    PKCS7_add_certificate(p7, mp_x509);

    int c = 0;
    for ( ; c < sk_X509_num(mp_ca); c++) {
        X509* cert = sk_X509_value(mp_ca, c);
        PKCS7_add_certificate(p7, cert);
    }
    PKCS7_set_detached(p7, 1);
    PKCS7_content_new(p7, NID_pkcs7_data);

    PKCS7_SIGNER_INFO_sign(p7Si);

    int p7Len = i2d_PKCS7(p7, NULL);
    NSMutableData* signature = [NSMutableData data];
    unsigned char* p7Buf = (unsigned char*) malloc(p7Len);
    if (p7Buf != NULL) {
        unsigned char* pP7Buf = p7Buf;
        i2d_PKCS7(p7, &pP7Buf);
        [signature appendBytes: (const void*) p7Buf length: p7Len];
        free(p7Buf);
    }
    PKCS7_free(p7);

    pkcs7 = signature;    // egee

    return (signature);
}

- (id) init: (NSString*) pfxfile password: (NSString*) password pin:(NSString *)p delegate:(id<SmartCardPKCS7SignatureHandlerDelegate>)delegate
{
    self = [super init];
    
    _delegate = delegate;
    
    // General initialisation
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        CRYPTO_malloc_init();
        ERR_load_crypto_strings();
        OpenSSL_add_all_algorithms();
        RSA_METHOD* method = (RSA_METHOD*)RSA_get_default_method();

        ENGINE_load_cryptodev();
        ENGINE_load_openssl();
        ENGINE* eng = ENGINE_by_id("openssl");

        method->flags |= RSA_FLAG_SIGN_VER;
        method->flags |= RSA_METHOD_FLAG_NO_CHECK;
        method->rsa_sign = (int (*)(int type,
                                    const unsigned char *m, unsigned int m_length,
                                    unsigned char *sigret, unsigned int *siglen, const RSA *rsa))rsa_sign;

        ENGINE_set_RSA(eng, method);
    });

    pin = p;

    FILE* fp = fopen([pfxfile cStringUsingEncoding: NSASCIIStringEncoding], "rb");
    if (fp == NULL)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot open private key." userInfo: nil]);

    PKCS12* p12 = d2i_PKCS12_fp(fp, NULL);
    fclose(fp);

    if (p12 == NULL)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);

    mp_pkey = NULL;
    mp_x509 = NULL;
    mp_ca = NULL;
    int parseResult = PKCS12_parse(p12, [password cStringUsingEncoding: NSASCIIStringEncoding], &mp_pkey, &mp_x509, &mp_ca);
    PKCS12_free(p12);

    signBlock = ^int(int type, unsigned char *m, unsigned int m_len, unsigned char *sigret, unsigned int *siglen, RSA *rsa) {
        NSData *toSign = [NSData dataWithBytes:m length:m_len];
        //NSData *toSign = hash;
        NSLog(@"to sign %@", toSign);

        SAMError *error = nil;

        NSInteger status;
        NSData *signature = [[IDManager sharedManager] sign:pin pinStatus:&status certificateIndex:NONREPUDIATION_CERTIFICATE toSign:toSign error:&error];

        [self incrementProgress:15];
        
        [signature getBytes:sigret length:[signature length]];
        *siglen = [signature length];
        return 1;
    };

    RSA_METHOD* method = (RSA_METHOD*)RSA_get_default_method();

    ENGINE_load_cryptodev();
    ENGINE_load_openssl();
    ENGINE* eng = ENGINE_by_id("openssl");

    method->flags |= RSA_FLAG_SIGN_VER;
    method->flags |= RSA_METHOD_FLAG_NO_CHECK;
    method->rsa_sign = (int (*)(int type,
                                const unsigned char *m, unsigned int m_length,
                                unsigned char *sigret, unsigned int *siglen, const RSA *rsa))rsa_sign;

    ENGINE_set_RSA(eng, method);

    SAMError *error = nil;

    [self progressTo:20];
    NSData *signer = [[IDManager sharedManager] getCertificate:NONREPUDIATION_CERTIFICATE error:&error];
    assert(signer);
    const unsigned char* certData = (const unsigned char*)[signer bytes];
    mp_x509 = d2i_X509(NULL, &certData, signer.length);
    assert(mp_x509);

    [self progressTo:35];
    NSArray *cas = [[IDManager sharedManager] rootCertificatesList:&error];


    mp_ca = sk_X509_new_null();
    for (NSData *cert in cas) {
        const unsigned char* cd = (const unsigned char*)[cert bytes];
        X509 *x = d2i_X509(NULL, &cd, cert.length);
        sk_X509_push(mp_ca, x);
    }
    [self progressTo:70];
    
    if (parseResult == 0)
        @throw ([NSException exceptionWithName: @"PDFNet Exception" reason: @"Cannot parse private key." userInfo: nil]);

    [self Reset];

    return (self);
}
- (void) dealloc
{
    sk_X509_free(mp_ca);
    X509_free(mp_x509);
    EVP_PKEY_free(mp_pkey);
}

#pragma mark - Custom methods

- (void) incrementProgress:(int)increment   {
    int new_progress = progress_current + increment;
    [self progressTo:new_progress];
}

- (void) progressTo:(int)new_progress   {
    if(_delegate && [_delegate respondsToSelector:@selector(signatureProgressedTo:)])   {
        [_delegate signatureProgressedTo:[NSNumber numberWithInt:new_progress]];
    }
    progress_current = new_progress;
}
@end
#endif
