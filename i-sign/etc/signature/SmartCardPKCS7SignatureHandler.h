//
//  SmartCardPKCS7SignatureHandler.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 22/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PDFTron/Headers/ObjC/PDFNetOBJC.h>

@protocol SmartCardPKCS7SignatureHandlerDelegate;

@interface SmartCardPKCS7SignatureHandler : SignatureHandler

#pragma mark - Public methods
- (NSString*) GetName;
- (void) AppendData: (NSData*)data;
- (BOOL) Reset;
- (NSData*) CreateSignature;
- (id) init: (NSString*) pfxfile password: (NSString*) password pin:(NSString*)pin delegate:(id<SmartCardPKCS7SignatureHandlerDelegate>) delegate;

@end

@protocol SmartCardPKCS7SignatureHandlerDelegate <NSObject>
@optional

-(void)signatureProgressedTo:(NSNumber*)pct;

@end