//
//  CustomSignatureHandler.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 10/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "CustomSignatureHandler.h"


#import "Debug.h"

@implementation CustomSignatureHandler  {
    SHA_CTX _m_sha_ctx;
    EVP_PKEY* _mp_pkey;      // private key
    X509* _mp_x509;          // signing certificate
    STACK_OF(X509)* _mp_ca;  // certificate chain up to the CA
}

- (NSString*) GetName   {
    return (@"Adobe.PPKLite");    
}

- (void) AppendData : (NSData*) data    {
    SHA1_Update(&_m_sha_ctx, [data bytes], [data length]);
    return;    
}

/*
- (void) AppendData:(NSData *)data  {
    warn(@"Call to unimplemented AppendData");
}*/

/*- (BOOL) ResetSample  {
    SHA1_Init(&m_sha_ctx);
    return YES;
}*/

- (BOOL) Reset  {
    SHA1_Init(&_m_sha_ctx);
    return YES;
}

- (NSData*) CreateSignature {
    unsigned char sha_buffer[SHA_DIGEST_LENGTH];
    memset((void*) sha_buffer, 0, SHA_DIGEST_LENGTH);
    SHA1_Final(sha_buffer, &_m_sha_ctx);
    
    PKCS7* p7 = PKCS7_new();
    PKCS7_set_type(p7, NID_pkcs7_signed);
    
    PKCS7_SIGNER_INFO* p7Si = PKCS7_add_signature(p7, _mp_x509, _mp_pkey, EVP_sha1());
    PKCS7_add_attrib_content_type(p7Si, OBJ_nid2obj(NID_pkcs7_data));
    PKCS7_add0_attrib_signing_time(p7Si, NULL);
    PKCS7_add1_attrib_digest(p7Si, (const unsigned char*) sha_buffer, SHA_DIGEST_LENGTH);
    PKCS7_add_certificate(p7, _mp_x509);
    
    int c = 0;
    for ( ; c < sk_X509_num(_mp_ca); c++) {
        X509* cert = sk_X509_value(_mp_ca, c);
        PKCS7_add_certificate(p7, cert);
    }
    PKCS7_set_detached(p7, 1);
    PKCS7_content_new(p7, NID_pkcs7_data);
    
    PKCS7_SIGNER_INFO_sign(p7Si);
    
    int p7Len = i2d_PKCS7(p7, NULL);
    NSMutableData* signature = [NSMutableData data];
    unsigned char* p7Buf = (unsigned char*) malloc(p7Len);
    if (p7Buf != NULL) {
        unsigned char* pP7Buf = p7Buf;
        i2d_PKCS7(p7, &pP7Buf);
        [signature appendBytes: (const void*) p7Buf length: p7Len];
        free(p7Buf);
    }
    PKCS7_free(p7);
    
    return signature;    
}
/*
- (NSData *) CreateSignature    {
    if(!_data_signature)    {
        err(@"CreateSignature called but data is nil");
    }
    warn(@"Unimplemented method called, CreateSignature");
    return _data_signature;
}*/

- (id) initWithPrivateKey:(EVP_PKEY *)mp_pkey andX509Certificate:(X509 *)mp_x509 andStack:(STACK_OF(X509)*) mp_ca   {
    if(self = [super init]) {

        CRYPTO_malloc_init();
        ERR_load_crypto_strings();
        OpenSSL_add_all_algorithms();
        
        _mp_pkey = mp_pkey;
        _mp_x509 = mp_x509;
        _mp_ca = mp_ca;
        [self Reset];
    }
    return self;
}

- (void) dealloc    {
    sk_X509_free(_mp_ca);
    X509_free(_mp_x509);
    EVP_PKEY_free(_mp_pkey);
}


/*- (id) init {
    if(self = [super init]) {
        // Populate mp_pkey, mp_x509, mp_ca
    }
    return self;
}

- (id) initWithCptr: (void*) cptr   {
    self = [self init];
    return self;
}


- (void) dealloc    {
    warn(@"Call to unimplemented dealloc");
}

/*- (void*) getCptr   {
    warn(@"Call to unimplemented getCptr");
    return nil;
    return [super getCptr];
}

- (void) unsetCPtr  {
    warn(@"Call to unimplemented unsetCPtr");
    [super unsetCPtr];
}*/


@end
