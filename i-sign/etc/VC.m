//
//  VC.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "VC.h"

@interface VC ()

@end

@implementation VC

#pragma mark - Default init methods

- (void) viewDidLoad    {
    [super viewDidLoad];
    
    // Common navigation bar properties
    [self.navigationItem setHidesBackButton:NO];
}

- (void) viewWillAppear:(BOOL)animated  {
    [super viewWillAppear:animated];

    // Configure bars' default appearance
    [self.navigationController setToolbarHidden:YES];
    [self.navigationController.toolbar setTranslucent:NO];
    [self.navigationController.navigationBar setTranslucent:NO];
}

#pragma mark - Orientation methods

// NOTE: Prior to iOS6, this method should be implemented to support additional interface orientations. plist file isn't enough
- (BOOL) shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation  {
    if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationPortrait)   {
        return YES;
    }
    return NO;
}

@end
