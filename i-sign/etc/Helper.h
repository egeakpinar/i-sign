//
//  Helper.h
//
//  Created by Ismail Ege AKPINAR on 06/11/2012.
//

#define IS_IPHONE5 ( fabs( ( double )[ [ UIScreen mainScreen ] bounds ].size.height - ( double )568 ) < DBL_EPSILON )

#ifdef UI_USER_INTERFACE_IDIOM()
    #define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#else
    #define IS_IPAD (false)
#endif

#define DEGREES_TO_RADIANS(d) (d * M_PI / 180)

#import <Foundation/Foundation.h>

@interface Helper : NSObject



#pragma mark - UI methods
+(void) show_activity_indicator;
+ (void) hide_activity_indicator;
+ (void) addSubviewToTop:(UIView *)view;   // Adds a subview above any other view, including keyboard

#pragma mark - String methods
+ (BOOL) string:(NSString *)string contains:(NSString *)substring; // Case SENSITIVE
+ (BOOL) string:(NSString *)string containsi:(NSString *)substring; // Case insensitive
+ (NSString *) string_trimmed:(NSString *)input;  // Trims white spaces at both ends of given string
+ (NSString *) stringFixed:(NSString *)input;  // Replaces blanks with underscore, converts to lowercase, trims
+ (BOOL) stringValid:(NSString *)str;   // Returns YES if string has content that's not only white spaces

#pragma mark - Layout methods
+ (CGFloat) status_bar_height;
+ (CGFloat) screen_height;  // Usable screen given to app
+ (CGFloat) screen_width;   // Usable screen given to app
+ (CGFloat) device_height;  // Entire screen of the device
+ (CGFloat) device_width;   // Entire screen of the device
+ (CGFloat) keyboard_height;    // Height of default keyboard
+ (CGFloat) navbar_height;    // Height of default navbar
+ (CGFloat) tabbar_height;    // Height of default tabbar
+ (CGFloat) toolbarHeight;  // Height of default toolbar
+ (BOOL) isLandscapeOrientation;   // YES if device is in landscape orientation, NO if portrait

#pragma mark - Misc. methods
+(double) get_timestamp;
+ (NSString *) month_to_string:(int)month;
+ (void)save_image:(UIImage*)image with_name:(NSString*)imageName;
+(void) display_message_with_title:(NSString *)title message:(NSString *)message and_delegate:(id<UIAlertViewDelegate>) delegate;
+(void) display_message_with_title:(NSString *)title message:(NSString *)message and_delegate:(id<UIAlertViewDelegate>) delegate tag:(int)tag;
+(void) displayMessageWithTitle:(NSString *)title message:(NSString *)message delegate:(id<UIAlertViewDelegate>) delegate tag:(int)tag buttonTitle:(NSString *)button_title;

#pragma mark - File management
+ (NSString *) documentsPath;
+ (BOOL) saveBuffer:(NSData *)data toPath:(NSString *)path; // Path should contain extension as well

@end

