//
//  FileExplorer.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Folder.h"
#import "Common.h"

@protocol FileExplorerDelegate <NSObject>

- (void) directoryDidChange;

@end

@interface FileExplorer : NSObject

#pragma mark - Public methods
+ (FileExplorer *) shared;

// Adds delegate for FileExplorerDelegate notifications
// TODO: This is not ideal because all delegates are notified of changes (including those that
// should not be interested)
- (void) addDelegate:(id<FileExplorerDelegate>)delegate;

// Returns all entries under given folder as an array of Inode objects
// Sorts by name as default
// If folder is nil, defaults to /Documents
- (NSMutableArray *) getInodesUnderFolder:(Folder *)folder foldersOnly:(BOOL) foldersOnly;


// Returns all entries under given folder as an array of Inode objects, sorted by specified parameter
// If folder is nil, defaults to /Documents
- (NSMutableArray *) getInodesUnderFolder:(Folder *)folder foldersOnly:(BOOL) foldersOnly sortBy:(ID_sort_by)sort_by;

// Returns YES if a folder with given name under root (i.e. Documents) exists
// NO otherwise
- (BOOL) rootFolderExists:(NSString *)folderName;

// Tries to create a folder with given name under root (i.e. Documents)
// Returns YES on success, NO otherwise
// Note: Doesn't check if folder already exists
// Note: It works for one level, doesn't create intermediate directories
- (BOOL) createRootFolder:(NSString *)folderName;


@end
