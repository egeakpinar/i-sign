//
//  Common.h
//  YAction
//
//  Created by Ismail Ege AKPINAR on 06/01/2013.
//  Copyright (c) 2013 Quiet Riots. All rights reserved.
//

#import <Foundation/Foundation.h>

// Project wide constants
#define SAMPLES_FOLDER_NAME @"PDF templates"
#define SETTING_SHOW_FILE_EXTENSIONS @"SETTING_SHOW_FILE_EXTENSIONS"
#define NOTIFICATION_FILE_EXTENSION_SETTING_CHANGED @"NOTIFICATION_FILE_EXTENSION_SETTING_CHANGED"

typedef enum {
    ID_sort_by_name,
    ID_sort_by_date,
    ID_sort_by_size,
    ID_sort_by_type
} ID_sort_by;

@interface Common : NSObject

// YES if this is the first time this app is launched
+ (BOOL) is_first_load;

+ (unsigned char *) stringToByteArray:(NSString *)input outputLength:(int *)outputLength;
+ (NSData *) stringToData:(NSString *)input;
+ (CGSize) pdfSize:(CGPDFPageRef) pdf;
+ (UIImage *) image:(UIImage *)img croppedAt:(CGRect)rect;
+ (NSData *) gzipData:(NSData *)input;

@end
