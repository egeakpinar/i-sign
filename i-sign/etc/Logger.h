//
//  Logger.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 29/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>

#import <Lumberjack/DDLog.h>
#import <TestFlightSDK1.2.4/TestFlight.h>

#define BETA_MODE 1

// Default - DDLogging
#ifdef DEBUG
    static const int ddLogLevel = LOG_LEVEL_WARN;
#else
    static const int ddLogLevel = LOG_LEVEL_VERBOSE;
#endif

#define logg(s, ...) DDLogInfo((@"%s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define deb( s, ...) DDLogVerbose((@"[DEBUGGING] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define warn(s, ...) DDLogWarn((@"[WARN] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define err(s, ...) DDLogError((@"[ERROR] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)


// TestFlight Beta mode - Use TestFlight's logging if in beta mode
#if BETA_MODE
#define logg(s, ...) TFLog((@"%s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define deb(s, ...) TFLog((@"[DEBUGGING] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define warn(s, ...) TFLog((@"[WARN] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#define err(s, ...) TFLog((@"[ERROR] %s [Line %d] " s), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__)
#endif

// Dev mode - Custom logging
#ifdef DEBUG

#define DEBUG_LOGGING 1
#define INFO_LOGGING 1
#define WARNING_LOGGING 1
#define ERROR_LOGGING 1

#if DEBUG_LOGGING
#define deb( s, ... ) NSLog( @"*** DEBUGGING *** %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define deb( s, ... )
#endif

#if INFO_LOGGING
#define logg( s, ... ) NSLog( @"%@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define logg( s, ... )
#endif

#if WARNING_LOGGING
#define warn( s, ... ) NSLog( @"Warning -- %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define warn( s, ... )
#endif

#if ERROR_LOGGING
#define err( s, ... ) NSLog( @"ERROR -- %@", [NSString stringWithFormat:(s), ##__VA_ARGS__] )
#else
#define err( s, ... )
#endif

#endif

@interface Logger : NSObject

+(Logger *) shared;   // Singleton instance
-(void) start;
-(NSArray *) logFilePaths;

@end
