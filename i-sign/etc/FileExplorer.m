//
//  FileExplorer.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "FileExplorer.h"
#import "Debug.h"
#import "Inode.h"
#import "File.h"
#import "Folder.h"

// Watching directory
#include <sys/types.h>
#include <sys/event.h>
#include <sys/time.h>
#include <fcntl.h>
#include <unistd.h>


@interface FileExplorer()

#pragma mark - Private properties
@property(nonatomic, assign) int dirFD;
@property(nonatomic, assign) int kq;
@property(nonatomic, assign) CFFileDescriptorRef dirKQRef;
@property(nonatomic, retain) NSMutableArray *arr_delegates;

@end

@implementation FileExplorer

+ (FileExplorer *) shared   {
    static FileExplorer *_instance;
    if(!_instance)  {
        _instance = [[FileExplorer alloc] init];
        _instance.dirFD = -1;
        _instance.kq = -1;
        _instance.dirKQRef = NULL;

        _instance.arr_delegates = [[NSMutableArray alloc] init];
    }
    return _instance;
}

- (void) addDelegate:(id<FileExplorerDelegate>)delegate {
    assert(_arr_delegates);
    [_arr_delegates addObject:delegate];
}

- (NSMutableArray *) getInodesUnderFolder:(Folder *)folder foldersOnly:(BOOL) foldersOnly   {
    return [self getInodesUnderFolder:folder foldersOnly:foldersOnly sortBy:ID_sort_by_name];
}

- (NSMutableArray *) getInodesUnderFolder:(Folder *)folder foldersOnly:(BOOL) foldersOnly sortBy:(ID_sort_by)sort_by    {

    NSURL *directoryURL = nil;
    if(folder == nil)   {
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
        NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory
        directoryURL = [NSURL fileURLWithPath:documentsDirectory];
    }
    else    {
        directoryURL = folder.url;
    }

    [self startMonitoringDirectory:directoryURL.path];

    NSArray *keys = [NSArray arrayWithObjects:
                     NSURLIsDirectoryKey, NSURLIsPackageKey, NSURLLocalizedNameKey, NSURLAttributeModificationDateKey, NSURLFileSizeKey, NSURLTypeIdentifierKey, nil];

    NSDirectoryEnumerator *enumerator = [[NSFileManager defaultManager]
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:(NSDirectoryEnumerationSkipsPackageDescendants |
                                                  NSDirectoryEnumerationSkipsHiddenFiles |
                                                  NSDirectoryEnumerationSkipsSubdirectoryDescendants)
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             warn(@"while enumerating Documents - %@ %@", [error description] , [error localizedDescription]);
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];


    NSMutableArray *arr_inodes = nil;


    for (NSURL *url in enumerator) {
        // Error-checking is omitted for clarity.

        NSNumber *isDirectory = nil;
        [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:NULL];

        assert([enumerator level] == 1);

        if ([isDirectory boolValue]) {
            NSString *localizedName = nil;
            [url getResourceValue:&localizedName forKey:NSURLLocalizedNameKey error:NULL];

            NSNumber *isPackage = nil;
            [url getResourceValue:&isPackage forKey:NSURLIsPackageKey error:NULL];

            NSDate *date_last_modify = nil;
            [url getResourceValue:&date_last_modify forKey:NSURLAttributeModificationDateKey error:NULL];

            if (![isPackage boolValue]) {
                if(!arr_inodes) {
                    arr_inodes = [NSMutableArray array];
                }
                Folder *folder = [[Folder alloc] initWithURL:url name:localizedName lastModifyDate:date_last_modify];
                [arr_inodes addObject:folder];
                //                logg(@"directory found: %@", localizedName);
            }
        }
        else    {
            if(foldersOnly) {
                continue;
            }
            NSString *localizedName = nil;
            [url getResourceValue:&localizedName forKey:NSURLLocalizedNameKey error:NULL];

            NSString *extension = nil;
            [url getResourceValue:&extension forKey:NSURLTypeIdentifierKey error:NULL];

            NSDate *date_last_modify = nil;
            [url getResourceValue:&date_last_modify forKey:NSURLAttributeModificationDateKey error:NULL];

            NSNumber *size_num = nil;
            [url getResourceValue:&size_num forKey:NSURLFileSizeKey error:NULL];

            if(!arr_inodes) {
                arr_inodes = [NSMutableArray array];
            }

            File *file = [[File alloc] initWithURL:url name:localizedName extension:extension size:[size_num floatValue] lastModifyDate:date_last_modify];
            [arr_inodes addObject:file];
        }
    }

    NSComparisonResult (^comparator)(id obj1, id obj2) = nil;
    switch (sort_by) {
        case ID_sort_by_date:
            comparator = comparatorDate;
            break;
        case ID_sort_by_name:
            comparator = comparatorName;
            break;
        case ID_sort_by_size:
            comparator = comparatorSize;
            break;
        case ID_sort_by_type:
            comparator = comparatorType;
            break;
        default:
            err(@"Unexpected sort_by parameter, defaulting to sort_by_name");
            comparator = comparatorName;
            break;
    }

    [arr_inodes sortUsingComparator:comparator];
    return arr_inodes;
}


- (BOOL) rootFolderExists:(NSString *)folderName    {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *folder_path = [documentsDirectory stringByAppendingPathComponent:folderName];

    return [fileManager fileExistsAtPath:folder_path];
}

- (BOOL) createRootFolder:(NSString *)folderName    {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES); //create an array and store result of our search for the documents directory in it
    NSString *documentsDirectory = [paths objectAtIndex:0]; //create NSString object, that holds our exact path to the documents directory

    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSString *folder_path = [documentsDirectory stringByAppendingPathComponent:folderName];

    NSError *error = nil;
    BOOL result = [fileManager createDirectoryAtPath:folder_path withIntermediateDirectories:NO attributes:0 error:&error];

    if(!result) {
        err(@"Failed to create directory at path ||%@|| - %@ %@", folder_path , error, [error localizedDescription]);
    }
    return result;
}

#pragma mark - Comparators
static NSComparisonResult(^comparatorName)(id obj1, id obj2) = ^NSComparisonResult(id obj1, id obj2)    {
    Inode *inode1 = (Inode *)obj1;
    Inode *inode2 = (Inode *)obj2;

    if([inode1 isFolder])   {
        if([inode2 isFolder])   {
            return [inode1.name caseInsensitiveCompare:inode2.name];
        }
        else    {
            return NSOrderedAscending;
        }
    }
    else    {
        if([inode2 isFolder])   {
            return NSOrderedDescending;
        }
        else    {
            NSString *ext1 = ((File *) inode1).extension;
            NSString *ext2 = ((File *) inode2).extension;
            return [ext1 caseInsensitiveCompare:ext2];
        }
    }
};

static NSComparisonResult(^comparatorDate)(id obj1, id obj2) = ^NSComparisonResult(id obj1, id obj2)    {
    Inode *inode1 = (Inode *)obj1;
    Inode *inode2 = (Inode *)obj2;

    return [inode2.date_last_modified compare:inode1.date_last_modified];
};

static NSComparisonResult(^comparatorType)(id obj1, id obj2) = ^NSComparisonResult(id obj1, id obj2)    {
    Inode *inode1 = (Inode *)obj1;
    Inode *inode2 = (Inode *)obj2;

    if([inode1 isFolder])   {
        if([inode2 isFolder])   {
            return [inode1.name caseInsensitiveCompare:inode2.name];
        }
        else    {
            return NSOrderedAscending;
        }
    }
    else    {
        if([inode2 isFolder])   {
            return NSOrderedDescending;
        }
        else    {
            return [inode1.name caseInsensitiveCompare:inode2.name];
        }
    }
};

static NSComparisonResult(^comparatorSize)(id obj1, id obj2) = ^NSComparisonResult(id obj1, id obj2)    {
    Inode *inode1 = (Inode *)obj1;
    Inode *inode2 = (Inode *)obj2;

    if([inode1 isFolder])   {
        if([inode2 isFolder])   {
            return [inode1.name caseInsensitiveCompare:inode2.name];
        }
        else    {
            return NSOrderedAscending;
        }
    }
    else    {
        if([inode2 isFolder])   {
            return NSOrderedDescending;
        }
        else    {
            float size1 = ((File *)inode1).size;
            float size2 = ((File *)inode2).size;
            if(size1 > size2)   {
                return NSOrderedAscending;
            }
            else if(size1 == size2) {
                return NSOrderedSame;
            }
            else    {
                return NSOrderedDescending;
            }
        }
    }
};

#pragma mark - Watching directory
// Code based on AVPlayerDemo sample, DirectoryWatcher class

- (void)kqueueFired
{
    assert(_kq >= 0);

    struct kevent   event;
    struct timespec timeout = {0, 0};
    int             eventCount;

    eventCount = kevent(_kq, NULL, 0, &event, 1, &timeout);
    assert((eventCount >= 0) && (eventCount < 2));

	// notify delegates about the change
    if(_arr_delegates)  {
        for(id delegate in _arr_delegates)  {
            if(delegate && [delegate respondsToSelector:@selector(directoryDidChange)]) {
                [delegate directoryDidChange];
            }
        }
    }

    CFFileDescriptorEnableCallBacks(_dirKQRef, kCFFileDescriptorReadCallBack);
}

static void KQCallback(CFFileDescriptorRef kqRef, CFOptionFlags callBackTypes, void *info)
{
    FileExplorer *obj;

    obj = (__bridge FileExplorer *)info;
    assert([obj isKindOfClass:[FileExplorer class]]);
    assert(kqRef == obj->_dirKQRef);
    assert(callBackTypes == kCFFileDescriptorReadCallBack);

    [obj kqueueFired];
}

- (BOOL)startMonitoringDirectory:(NSString *)dirPath
{
	// Check if already initialised
	if ((_dirKQRef == NULL) && (_dirFD == -1) && (_kq == -1))
	{
		// Open the directory we're going to watch
		_dirFD = open([dirPath fileSystemRepresentation], O_EVTONLY);
		if (_dirFD >= 0)    {
			// Create a kqueue for our event messages...
			_kq = kqueue();
			if (_kq >= 0)   {
				struct kevent eventToAdd;
				eventToAdd.ident  = _dirFD;
				eventToAdd.filter = EVFILT_VNODE;
				eventToAdd.flags  = EV_ADD | EV_CLEAR;
				eventToAdd.fflags = NOTE_WRITE;
				eventToAdd.data   = 0;
				eventToAdd.udata  = NULL;

				int errNum = kevent(_kq, &eventToAdd, 1, NULL, 0, NULL);
				if (errNum == 0)
				{
					CFFileDescriptorContext context = { 0, (__bridge void *)(self), NULL, NULL, NULL };
					CFRunLoopSourceRef      rls;

					// Passing true in the third argument so CFFileDescriptorInvalidate will close kq.
					_dirKQRef = CFFileDescriptorCreate(NULL, _kq, true, KQCallback, &context);
					if (_dirKQRef != NULL)  {
						rls = CFFileDescriptorCreateRunLoopSource(NULL, _dirKQRef, 0);
						if (rls != NULL)
						{
							CFRunLoopAddSource(CFRunLoopGetCurrent(), rls, kCFRunLoopDefaultMode);
							CFRelease(rls);
							CFFileDescriptorEnableCallBacks(_dirKQRef, kCFFileDescriptorReadCallBack);

							// If everything worked, return early and bypass shutting things down
							return YES;
						}
                        err(@"Couldn't create a runloop source for watching directory");
						// Couldn't create a runloop source, invalidate and release the CFFileDescriptorRef
						CFFileDescriptorInvalidate(_dirKQRef);
                        CFRelease(_dirKQRef);
						_dirKQRef = NULL;
					}
				}
                err(@"kq is active but something failed while trying to watch directory");
				// kq is active, but something failed, close the handle...
				close(_kq);
				_kq = -1;
			}
            err(@"file handle is open but something failed while trying to watch directory");
			// file handle is open, but something failed, close the handle...
			close(_dirFD);
			_dirFD = -1;
		}
        err(@"Couldn't open the directory to watch");
	}
    logg(@"directory watcher - already initialised");
	return NO;
}

@end
