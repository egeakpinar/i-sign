//
//  Inode.m
//  i-sign
//
//  Abstract class to represent File and Folder
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Inode.h"
#import "Folder.h"

@implementation Inode

- (BOOL) isFolder   {
    if([self isKindOfClass:[Folder class]]) {
        return YES;
    }
    return NO;
}




@end
