//
//  Folder.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Folder.h"

@implementation Folder

#pragma mark - Public methods
- (id) initWithURL:(NSURL *)url name:(NSString *)name lastModifyDate:(NSDate *)date {
    if(self = [super init]) {
        self.url = url;
        self.name = name;
        self.date_last_modified = date;
    }
    return self;
}

@end
