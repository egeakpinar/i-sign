//
//  Folder.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Inode.h"

@interface Folder : Inode

#pragma mark - Public methods
- (id) initWithURL:(NSURL *)url name:(NSString *)name lastModifyDate:(NSDate *)date;

@end
