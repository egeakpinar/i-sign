//
//  File.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "File.h"
#import "Debug.h"

@implementation File

#pragma mark - Public methods

- (id) initWithURL:(NSURL *)url name:(NSString *)name extension:(NSString *)extension size:(float)size lastModifyDate:(NSDate *)date  {
    if(self = [super init]) {
        self.url = url;
        self.name = name;
        self.extension = extension;
        self.size = size;
        self.date_last_modified = date;
        
        [self tryDetectType];
    }
    return self;
}

- (void) tryDetectType  {
    NSString *ext = [self.extension lowercaseString];
    if([ext isEqualToString:@"com.adobe.pdf"])    {
        self.type = PDF;
    }
    else    {
        self.type = Unrecognised;
    }
}
@end
