//
//  File.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Inode.h"

@interface File : Inode

#pragma mark - Public properties
@property(nonatomic, copy) NSString *extension;
@property(nonatomic, assign) file_types type;
@property(nonatomic, assign) float size;    // in bytes
#pragma mark - Public methods
- (id) initWithURL:(NSURL *)url name:(NSString *)name extension:(NSString *)extension size:(float)size lastModifyDate:(NSDate *)date;

@end
