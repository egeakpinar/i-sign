//
//  Inode.h
//  i-sign
//
//  Abstract class to represent File and Folder
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO: Prefix ID here
typedef enum    {
    PDF,
    Unrecognised
} file_types;

@interface Inode : NSObject

#pragma mark - Public properties
@property(nonatomic, copy) NSURL *url;
@property(nonatomic, copy) NSString *name;
@property(nonatomic, assign, readonly) BOOL isFolder;
@property(nonatomic, retain) NSDate *date_last_modified;

#pragma mark - Public methods

@end
