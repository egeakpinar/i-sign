//
//  AppDelegate.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "AppDelegate.h"
#import "Common.h"
#import "ExplorerVC.h"
#import "ViewPDFVC.h"
#import "lib/PDFTron/Headers/ObjC/PDFNetOBJC.h"
#import "Logger.h"

#if BETA_MODE
#import "TestFlight.h"
#endif

#if TARGET_IPHONE_SIMULATOR
#import "DCIntrospect.h"
#endif

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.

    // First - start logging
    [[Logger shared] start];
    
    ExplorerVC *vc_explorer = [[ExplorerVC alloc] initWithRootFolder:nil];
    UINavigationController *nav_controller = [[UINavigationController alloc] initWithRootViewController:vc_explorer];

    [nav_controller.navigationBar setBarStyle:UIBarStyleBlack];
    [nav_controller.toolbar setBarStyle:UIBarStyleBlack];
    [nav_controller.toolbar setTranslucent:NO];
    self.window.rootViewController = nav_controller;
    
    [self ensureSamplePDFs];
#ifdef DEBUG
    [self ensureSamplePDFs2];   // For testing, adds the same files to the root directory as well
//    [self inspectPDF];
#endif
    
    // Initialise PDFTron
    [PDFNet Initialize:@""];    
    [PDFNet SetDefaultDiskCachingEnabled:YES];
    
#if BETA_MODE
    [TestFlight setDeviceIdentifier:[[UIDevice currentDevice] uniqueIdentifier]];
//    [TestFlight takeOff:@"05ed0fdb-7593-47f6-a414-d03807049ffc"]; // Team token (Ege)
//    [TestFlight takeOff:@"75dae039815df5f114b41653521f9fa4_MjE4NzE5MjAxMy0wNS0wOCAwMjoyMDozMy43NjcwMDI"]; // Team token (i-dentity)

    [TestFlight takeOff:@"0f7f3ab5-b20d-46bc-9449-049f35ab78c0"];   // App token (i-dentity)
#endif
    
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];

#if TARGET_IPHONE_SIMULATOR
	// always insert this AFTER makeKeyAndVisible so statusBarOrientation is reported correctly.
	[[DCIntrospect sharedIntrospector] start];
#endif
    
    return YES;
}

- (void) ensureSamplePDFs   {
    NSArray *arr_sample_files = [NSArray arrayWithObjects:@"sample lettergen.pdf", @"mono mono mono.pdf", @"mono mono multi.pdf", @"mono multi mono.pdf", @"mono multi multi.pdf",
                                 @"multi mono multi.pdf", @"multi multi multi.pdf", nil];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for(NSString *str in arr_sample_files)  {
        NSString *default_pdf_path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:str];
        NSString *folder_path = [documentsDirectory stringByAppendingPathComponent:SAMPLES_FOLDER_NAME];
        NSString *writable_pdf_path = [folder_path stringByAppendingPathComponent:str];

        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        
        success = [fileManager fileExistsAtPath:writable_pdf_path];
        if (success)    {
            continue;
        }
        
        // First create /samples folder (if not already created)
        BOOL is_directory;
        BOOL folder_already_created = NO;
        folder_already_created = [fileManager fileExistsAtPath:folder_path isDirectory:&is_directory];
        if(!folder_already_created) {
            success = [fileManager createDirectoryAtPath:folder_path withIntermediateDirectories:NO attributes:nil error:&error];
            if(!success)    {
                err(@"Failed to create folder (%@) for sample PDF files  with message '%@'.", folder_path,[error localizedDescription]);
                continue;
            }
        }
        
        // Now copy
        success = [fileManager copyItemAtPath:default_pdf_path toPath:writable_pdf_path error:&error];
        if (!success) {
            err(@"Failed to create sample PDF file (%@) with message '%@'.", writable_pdf_path, [error localizedDescription]);
        }
    }
}

- (void) ensureSamplePDFs2   {
    NSArray *arr_sample_files = [NSArray arrayWithObjects:@"sample isign.pdf", @"sample lettergen.pdf", @"sample pdftron.pdf", @"sample pdf medium.pdf", @"sample pdf long.pdf",@"sample pdf very long.pdf", nil];
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    for(NSString *str in arr_sample_files)  {
        NSString *default_pdf_path = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:str];
        NSString *writable_pdf_path = [documentsDirectory stringByAppendingPathComponent:str];
        
        BOOL success;
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSError *error;
        
        success = [fileManager fileExistsAtPath:writable_pdf_path];
        if (success)    {
            continue;
        }
        
        success = [fileManager copyItemAtPath:default_pdf_path toPath:writable_pdf_path error:&error];
        if (!success) {
            err(@"Failed to create sample PDF file (%@) with message '%@'.", writable_pdf_path, [error localizedDescription]);
        }
    }
}

#ifdef DEBUG
// TODO: Remove this in production
// Convenience methods to inspect whether a PDF contains signature fields
- (void) inspectPDF {
    logg(@"+ inspectPDF");
    
    NSURL *url_pdf = [[NSBundle mainBundle] URLForResource:@"mono mono mono" withExtension:@"pdf"];
    NSData *pdf = [NSData dataWithContentsOfURL:url_pdf];
    PDFDoc *doc = [[PDFDoc alloc] initWithBuf:pdf buf_size:pdf.length];

    TRNField* sigField = nil;
    FieldIterator *iter = [doc GetFieldIterator];
    while ([iter HasNext]) {
        TRNField *field = [iter Current];
        if (e_signature == [field GetType]) {
            logg(@"%@", [field GetName]);
        }
        [iter Next];
    }
    if (!sigField) {
        logg(@"signature field not found");
    }
    else {
        logg(@"signature field found!");
    }
    
}
#endif

/* TODO: Move this into Common with ifdef(PDF_TRON) flag
- (NSString *) typeToStr:(FieldType) type   {
    switch(type)   {
        case e_button:
            return @"e_button";
        case e_check:
            return @"e_check";
        case e_radio:
            return @"e_radio";
        case e_text:
            return @"e_text";
        case e_choice:
            return @"e_choice";
        case e_signature:
            return @"e_signature";
        case e_f_null:
            return @"e_f_null";
        default:
            return @"unrecognised";
    }
}*/
 
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
