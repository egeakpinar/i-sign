//
//  ID_InputField.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ID_Keypad.h"

@interface ID_InputField : UIView

#pragma mark - Public properties
@property(nonatomic, retain) id<ID_KeypadProtocol>delegate;

@end

