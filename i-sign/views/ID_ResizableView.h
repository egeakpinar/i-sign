//
//  ID_ResizableView.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 06/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ID_ResizableView;

@protocol ID_ResizableViewDelegate <NSObject>

- (void) resizableViewHighlighted:(ID_ResizableView *)resizable;
- (void) resizableViewUnhighlighted:(ID_ResizableView *)resizable;
- (void) resizableViewAttemptedDelete:(ID_ResizableView *)resizable;

@end


@interface ID_ResizableView : UIView

#pragma mark - Public properties
@property(nonatomic, assign) BOOL is_highlighted;

#pragma mark - Public methods
- (id) initWithChild:(UIView *)child andDelegate:(id<ID_ResizableViewDelegate>)delegate;
- (void) highlight;
- (void) unhighlight;

@end
