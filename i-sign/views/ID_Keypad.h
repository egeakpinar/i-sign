//
//  ID_Keypad.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 30/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ID_Keypad;

@protocol ID_KeypadProtocol <NSObject>

- (void) keypad:(ID_Keypad *)keypad inputChangedTo:(NSString *)result;
- (void) keypadInputSubmitted;
- (void) keypadInputCancelled;

@end

@interface ID_Keypad : UIView

#pragma mark - Public methods
- (void) show;
- (void) hide;
- (void) reset;
- (void) disableSubmission:(BOOL)should_disable animated:(BOOL)animated;

#pragma mark - Public properties
@property(nonatomic, retain) id<ID_KeypadProtocol>delegate;
@property(nonatomic, assign, getter = isShown) BOOL isShown;

@end
