//
//  ID_InputField.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_InputField.h"

@interface ID_InputField()<UITextFieldDelegate,ID_KeypadProtocol> {
    ID_Keypad *_keypad;
}

// 4 layers
@property(nonatomic, retain) UITextField *txt_borders;  // Used for text input borders
@property(nonatomic, retain) UIView *view_progress;     // Progress bar view
@property(nonatomic, retain) UITextField *txt_secure;   // Secure dots
@property(nonatomic, retain) UITextField *txt_plain;    // Last added character only (plain)

@end

@implementation ID_InputField



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        // Add 4 layers
        CGRect frame_txt = CGRectMake(0, 0, frame.size.width, frame.size.height);
        _txt_borders = [[UITextField alloc] initWithFrame:frame_txt];
        _view_progress = [[UIView alloc] initWithFrame:CGRectMake(frame_txt.origin.x+1, frame_txt.origin.y+1, frame_txt.size.width-2, frame_txt.size.height-2)];
        _txt_secure = [[UITextField alloc] initWithFrame:frame_txt];
        _txt_plain = [[UITextField alloc] initWithFrame:frame_txt];
        
        [_txt_borders setBorderStyle:UITextBorderStyleBezel];
        [_txt_secure setBorderStyle:UITextBorderStyleNone];
        [_txt_plain setBorderStyle:UITextBorderStyleNone];
        
        [_txt_borders setBackgroundColor:[UIColor whiteColor]]; // debb
        [_txt_secure setBackgroundColor:[UIColor clearColor]];
        [_txt_plain setBackgroundColor:[UIColor clearColor]];
        
        [_txt_borders setTextAlignment:UITextAlignmentCenter];
        [_txt_secure setTextAlignment:UITextAlignmentCenter];
        [_txt_plain setTextAlignment:UITextAlignmentCenter];

        [_txt_borders setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [_txt_secure setFont:[UIFont boldSystemFontOfSize:16.0f]];
        [_txt_plain setFont:[UIFont boldSystemFontOfSize:16.0f]];
        
        
        [_txt_secure setSecureTextEntry:YES];
        [_txt_plain setDelegate:self];  // Only one text field should have a delegate
        
        // Note: Order of addition is important
        [self addSubview:_txt_borders];
        [self addSubview:_view_progress];
        [self addSubview:_txt_secure];
        [self addSubview:_txt_plain];
    }
    return self;
}

#pragma mark - UITextFieldDelegate methods

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField    {
    deb(@"+ text field should begin editing");
    // Show our custom keypad instead
    if(!_keypad) {
        _keypad = [ID_Keypad new];
        [_keypad setDelegate:self];
    }
    [_keypad show];
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField  {
    assert(false);
    return NO;
}

#pragma mark  - ID_KeypadProtocol methods

// Forward as is
- (void)keypad:(ID_Keypad *)keypad inputChangedTo:(NSString *)result    {
    
    // Update layers
    int len = [result length];
    
    NSString *str_secure;   // All except last character
    if(len > 1) {
        str_secure = [result substringToIndex:len-2];
    }
    else    {
        str_secure = @"";
    }
    [_txt_secure setText:str_secure];
    
    NSString *str_plain = @""; // Nothing but last character
    for(int i=0; i<len-2; i++)    {
        str_plain = [str_plain stringByAppendingString:@"+"];
    }
    if(len > 0) {
        str_plain = [str_plain stringByAppendingString:[result substringFromIndex:len-1]];
    }
    deb(@"str secure ||%@||", str_secure);
    deb(@"str plain  ||%@||", str_plain);
    
    [_txt_plain setText:str_plain];
    
    if(_delegate && [_delegate respondsToSelector:@selector(keypad:inputChangedTo:)])  {
        [_delegate keypad:keypad inputChangedTo:result];
    }
}

// Forward as is
- (void)keypadInputCancelled    {
    if(_delegate && [_delegate respondsToSelector:@selector(keypadInputCancelled)]) {
        [_delegate keypadInputCancelled];
    }
}

// Forward as is
- (void)keypadInputSubmitted    {
    if(_delegate && [_delegate respondsToSelector:@selector(keypadInputSubmitted)]) {
        [_delegate keypadInputSubmitted];
    }
}

@end
