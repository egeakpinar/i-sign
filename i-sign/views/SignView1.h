//
//  SignView1.h
//  TouchToSign1
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SignView1 : UIImageView

- (void) clear; // Clears drawing
- (UIImage *) getCroppedImage;  // Returns cropped signature as an image (automatically cropped)

@end
