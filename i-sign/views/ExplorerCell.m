//
//  ExplorerCell.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ExplorerCell.h"
#import "Debug.h"
#import "ID_ThumbPDF.h"
#import <QuartzCore/QuartzCore.h>
#import "Common.h"

@interface ExplorerCell()

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet ID_ThumbPDF *img_preview;
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

@end

@implementation ExplorerCell

#pragma mark - Properties
- (void) setInode:(Inode *)inode    {
    if(!inode)  {
        warn(@"Set inode is nil");
    }
    _inode = inode;

    BOOL show_extension = YES;   // Show extension by default
    NSUserDefaults *user_defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *num = [user_defaults objectForKey:SETTING_SHOW_FILE_EXTENSIONS];
    if(num && ![num boolValue]) {
        show_extension = NO;
    }
    if(show_extension)  {
        [_lbl_title setText:inode.name];
    }
    else    {
        [_lbl_title setText:[inode.name stringByDeletingPathExtension]];
    }
    [_img_preview configurePdfUrl:inode.url];
}

- (void) awakeFromNib   {
    [self.layer setShadowColor:[UIColor whiteColor].CGColor];
}
@end
