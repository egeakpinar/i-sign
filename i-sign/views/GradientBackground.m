//
//  GradientBackground.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "GradientBackground.h"
#import "Debug.h"

@implementation GradientBackground


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect   {
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    
    [self setAlpha:0.25f];
    
    CGGradientRef glossGradient;
    CGColorSpaceRef rgbColorspace;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.75,  // Start color
        1.0, 1.0, 1.0, 0.1 }; // End color
    
    rgbColorspace = CGColorSpaceCreateDeviceRGB();
    glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
    
    CGRect currentBounds = self.bounds;
    CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMidY(currentBounds));
    float radius = currentBounds.size.height > currentBounds.size.width ? currentBounds.size.height/1.5f : currentBounds.size.width/1.5f;
    CGContextDrawRadialGradient(currentContext, glossGradient, midCenter, 0, midCenter, radius, 0);
    
    CGGradientRelease(glossGradient);
    CGColorSpaceRelease(rgbColorspace);
}


@end
