//
//  TiledPDFView.h
//  PDF2
//
//  Created by Ismail Ege AKPINAR on 01/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TiledPDFView : UIView

- (id)initWithFrame:(CGRect)frame scale:(CGFloat)scale;
- (void)setPage:(CGPDFPageRef)newPage;
- (void) updateScale:(CGFloat)new_scale andFrame:(CGRect)new_frame;
@end
