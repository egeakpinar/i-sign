//
//  Cell_Switch.h
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Cell_Switch;
@protocol Cell_Switch_Delegate <NSObject>

- (void) cellSwitch:(Cell_Switch *)cell valueChanged:(id) sender;

@end
@interface Cell_Switch : UITableViewCell

#pragma mark - Properties
@property (nonatomic, retain) id<Cell_Switch_Delegate> delegate;
@property(nonatomic, copy) NSString *title;
@property(nonatomic, assign) BOOL is_on;

@end
