//
//  ID_ResizableView.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 06/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_ResizableView.h"
#import "Helper.h"

@interface ID_ResizableView()   {
    CGPoint point_start;    // Starting point for pan gesture (move)
    CGRect bounds_initial;   // Initial bounds before pan gesture (scale)
}


#pragma mark - Private properties
@property(nonatomic, assign) id<ID_ResizableViewDelegate> delegate;
@property(nonatomic, retain) UIButton *btn_resize;
@property(nonatomic, retain) UIButton *btn_delete;
@property(nonatomic, retain) UIView *view_child;

@end


@implementation ID_ResizableView

static float INSET = 15.0f;

#pragma mark - Public methods

- (id) initWithChild:(UIView *)child andDelegate:(id<ID_ResizableViewDelegate>)delegate   {
    CGRect rect_frame = CGRectInset(child.frame, -1 * INSET, -1 * INSET);
    _view_child = child;
    _delegate = delegate;
    
    self = [super initWithFrame:rect_frame];
    if (self) {
        // Initialization code
        
        // Add child view
        [child setFrame:CGRectMake(INSET, INSET, child.frame.size.width, child.frame.size.height)];
        [child setAutoresizingMask:UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight];
        [self addSubview:child];
        
        // Add gesture recognisers
        // Tap: Toggle highlight
        // Pan: Move
        // Long press: Activate delete button
        UITapGestureRecognizer *recogniser_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTap:)];
        [self addGestureRecognizer:recogniser_tap];

        UIPanGestureRecognizer *recogniser_pan_move = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanMove:)];
        [self addGestureRecognizer:recogniser_pan_move];
        
        UILongPressGestureRecognizer *recogniser_long_press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
        [self addGestureRecognizer:recogniser_long_press];

        // Add resize handle to the top left corner
        UIImage *icon_resize = [UIImage imageNamed:@"cursor-resize"];
        _btn_resize = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, INSET*2, INSET*2)];
        [_btn_resize setBackgroundImage:icon_resize forState:UIControlStateNormal];
        [_btn_resize setCenter:CGPointMake(INSET, INSET)];
        [_btn_resize setBackgroundColor:[UIColor clearColor]];
        [self addSubview:_btn_resize];
        
        UIPanGestureRecognizer *recogniser_pan_resize = [[UIPanGestureRecognizer alloc] initWithTarget:self action:@selector(handlePanResize:)];
        [_btn_resize addGestureRecognizer:recogniser_pan_resize];
        
        
        // Add delete icon to the top right corner
        UIImage *img_delete = [UIImage imageNamed:@"cursor-delete"];
        _btn_delete = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, INSET*2, INSET*2)];
        [_btn_delete setBackgroundImage:img_delete forState:UIControlStateNormal];
        [_btn_delete setCenter:CGPointMake(self.frame.size.width - INSET, INSET)];
        [_btn_delete setAutoresizingMask:UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleBottomMargin];
        [_btn_delete setBackgroundColor:[UIColor clearColor]];
        [_btn_delete setHidden:YES];    // Starts hidden
        [_btn_delete setUserInteractionEnabled:YES];
        [_btn_delete addTarget:self action:@selector(handleTapDelete:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_btn_delete];
                        
        [self setClipsToBounds:YES];

        [self highlight];
        
        // Note: Set autoresizing masks properly
        // Otherwise, it will break when superview is rotated
        [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin | UIViewAutoresizingFlexibleTopMargin];
    }
    return self;
}

- (void) highlight  {
    _is_highlighted = YES;
    
    [_btn_delete setHidden:YES];
    [_btn_resize setHidden:NO];
    [_view_child setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:1 alpha:0.1f]];
    
    // Notify delegate (if set)
    if(_delegate && [_delegate respondsToSelector:@selector(resizableViewHighlighted:)])    {
        [_delegate resizableViewHighlighted:self];
    }
}

- (void) unhighlight    {
    _is_highlighted = NO;
    
    [_btn_delete setHidden:YES];
    [_btn_resize setHidden:YES];
    [_view_child setBackgroundColor:[UIColor clearColor]];
    
    // Notify delegate (if set)
    if(_delegate && [_delegate respondsToSelector:@selector(resizableViewUnhighlighted:)])    {
        [_delegate resizableViewUnhighlighted:self];
    }    
}

#pragma mark - Custom methods

// Validates that bounds are not too small
- (BOOL) isBoundsAllowed:(CGRect) bounds  {
    if(bounds.size.width < INSET * 2)  {
        return NO;
    }
    else if(bounds.size.height < INSET * 2) {
        return NO;
    }
    return YES;
}

#pragma mark - Gesture recognisers

- (void) handlePanResize:(UIPanGestureRecognizer *)recogniser   {
    if(!_is_highlighted) {
        return;
    }
    if([recogniser state] == UIGestureRecognizerStateBegan) {
        bounds_initial = self.bounds;
    }
    else if([recogniser state] == UIGestureRecognizerStateChanged)  {
        CGPoint translate = [recogniser translationInView:self.superview];
        float perc_x = translate.x / bounds_initial.size.width;
        float perc_y = translate.y / bounds_initial.size.height;
        
        if(perc_x >0 && perc_y > 0)   {
            // Shrink
            float perc = perc_x > perc_y ? perc_y : perc_x;
            perc *= 2;  // Double effect, since happening from both edges
            perc = 1 - perc;
            if(perc < 0.4) perc = 0.4;
            
            CGAffineTransform tr = CGAffineTransformScale(CGAffineTransformIdentity, perc, perc);
            CGRect bounds_post_transform = CGRectApplyAffineTransform(bounds_initial, tr);

            // Validate post transform frame
            if([self isBoundsAllowed:bounds_post_transform])  {
                [self setBounds:bounds_post_transform];
            }
        }
        else if(perc_x < 0 && perc_y < 0) {
            // Expand
            float perc = perc_x > perc_y ? perc_x : perc_y;
            perc = (-1) * perc;
            perc *= 2;  // Double effect, since happening from both edges
            perc += 1;

            CGAffineTransform tr = CGAffineTransformScale(CGAffineTransformIdentity, perc, perc);
            CGRect bounds_post_transform = CGRectApplyAffineTransform(bounds_initial, tr);
            
            // Validate post transform bounds
            if([self isBoundsAllowed:bounds_post_transform])  {
                [self setBounds:bounds_post_transform];
            }
        }
    }
    else if([recogniser state] == UIGestureRecognizerStateEnded)    {
        // Do nothing
    }
}

- (void) handlePanMove:(UIPanGestureRecognizer *)recogniser   {
    if(!_is_highlighted)    {
        return;
    }
    if([recogniser state] == UIGestureRecognizerStateBegan) {
        point_start = self.center;
    }
    else if([recogniser state] == UIGestureRecognizerStateChanged)  {
        CGPoint translate = [recogniser translationInView:self.superview];
        
        CGPoint new_pos = CGPointMake(point_start.x + translate.x, point_start.y + translate.y);
        
        // Constrain it to safety bounds (10 px padding from edges)
        // TODO: Test with orientations
        int safety_padding = 10;
        float x_max = new_pos.x + (self.frame.size.width / 2.0f);
        float x_min = new_pos.x - (self.frame.size.width / 2.0f);
        float y_max = new_pos.y + (self.frame.size.height / 2.0f);
        float y_min = new_pos.y - (self.frame.size.height / 2.0f);
        if(x_max > self.superview.bounds.size.width - safety_padding)  {
            new_pos.x = (self.superview.bounds.size.width - safety_padding) - (self.frame.size.width / 2.0f);
        }
        else if(x_min < safety_padding) {
            new_pos.x = safety_padding + (self.frame.size.width / 2.0f);
        }
        
        if(y_max > self.superview.bounds.size.height - safety_padding)  {
            new_pos.y = (self.superview.bounds.size.height - safety_padding) - (self.frame.size.height / 2.0f);
        }
        else if(y_min < safety_padding) {
            new_pos.y = safety_padding + (self.frame.size.height / 2.0f);
        }
        
        [self setCenter:new_pos];
    }
    else if([recogniser state] == UIGestureRecognizerStateEnded)    {
        // Do nothing
    }
}

- (void) handleLongPress:(UILongPressGestureRecognizer *)recogniser {
    if(_is_highlighted) {
        [_btn_delete setHidden:NO];
        [_btn_resize setHidden:YES];
    }
}

// (Oddly, this is used by iPad whereas handleTap is used by iPhone)
- (void) handleTapDelete:(id) sender    {
    if(_delegate && [_delegate respondsToSelector:@selector(resizableViewAttemptedDelete:)])    {
        [_delegate resizableViewAttemptedDelete:self];
    }
}

- (void) handleTap:(UITapGestureRecognizer *) recogniser  {
    // Check if pressed on delete button
    // (Oddly, this is used by iPhone whereas handleTapDelete is used by iPad)
    BOOL should_delete = NO;
    if(!_btn_delete.isHidden)   {
        CGPoint point_touch = [recogniser locationInView:self];
        if(CGRectContainsPoint(_btn_delete.frame, point_touch))  {
            should_delete = YES;
        }
    }
    
    if(should_delete)   {
        if(_delegate && [_delegate respondsToSelector:@selector(resizableViewAttemptedDelete:)])    {
            [_delegate resizableViewAttemptedDelete:self];
        }
    }
    else    {
        // Toggle highlight / unhighlight
        if(_is_highlighted) {
            [self unhighlight];
        }
        else    {
            [self highlight];
        }
    }
}

@end
