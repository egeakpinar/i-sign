//
//  ID_KeypadButton.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 01/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_KeypadButton.h"
#import "Debug.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>
#import "Helper.h"

@interface ID_KeypadButton()

@property(nonatomic, retain) UILabel *lbl_number;
@property(nonatomic, retain) NSMutableArray *arr_lbl_letters;
@property(nonatomic, retain) NSMutableArray *arr_lbls;  // Container for all labels, declared for convenience
@property(nonatomic, assign) int index_letter;
@property(nonatomic, retain) id<ID_KeypadButtonProtocol> delegate;

@end

@implementation ID_KeypadButton

static ID_KeypadButton *_last_touched_button;

- (id)initWithKeyboardFrame:(CGRect)keyboard_frame index:(int)index delegate:(id<ID_KeypadButtonProtocol>)delegate {
    assert(index >= 0 && index <= 11);
    _delegate = delegate;
    
    // Calculate frame for this button
    float y_offset = keyboard_frame.origin.y;
    
    int but_width = ceil(keyboard_frame.size.width / 3.0f);
    int but_height = ceil(keyboard_frame.size.height / 4.0f);
    float but_x = (index %3) * but_width;
    float but_y = ((index /3) * but_height) + y_offset;
    CGRect but_frame = CGRectMake(but_x, but_y, but_width, but_height);
    
    self = [super initWithFrame:but_frame];
    if (self) {
        // Initialization code
        
        // Configure labels (based on index)

          // Configure number label
        CGRect frame_number = CGRectMake(0, 0, but_frame.size.width, 0.75f * but_frame.size.height);
        if(index >= 0 && index <= 8)    {
            _lbl_number = [[UILabel alloc] initWithFrame:frame_number];
            [_lbl_number setText:[NSString stringWithFormat:@"%d",index+1]];
        }
        else if(index == 10)    {
            frame_number.size.height = but_frame.size.height;   // Make 0 appear lower
            _lbl_number = [[UILabel alloc] initWithFrame:frame_number];
            [_lbl_number setText:@"0"];            
        }
          // Configure letter labels
        if(index > 0 && index <= 8) {
            NSMutableArray *arr = [NSMutableArray arrayWithCapacity:4];
            if(index == 1)  {
                [arr addObject:@"A"];
                [arr addObject:@"B"];
                [arr addObject:@"C"];
            }
            else if(index == 2) {
                [arr addObject:@"D"];
                [arr addObject:@"E"];
                [arr addObject:@"F"];
            }
            else if(index == 3) {
                [arr addObject:@"G"];
                [arr addObject:@"H"];
                [arr addObject:@"I"];
            }
            else if(index == 4) {
                [arr addObject:@"J"];
                [arr addObject:@"K"];
                [arr addObject:@"L"];
            }
            else if(index == 5) {
                [arr addObject:@"M"];
                [arr addObject:@"N"];
                [arr addObject:@"O"];
            }
            else if(index == 6) {
                [arr addObject:@"P"];
                [arr addObject:@"Q"];
                [arr addObject:@"R"];
                [arr addObject:@"S"];
            }
            else if(index == 7) {
                [arr addObject:@"T"];
                [arr addObject:@"U"];
                [arr addObject:@"V"];
            }
            else if(index == 8) {
                [arr addObject:@"W"];
                [arr addObject:@"X"];
                [arr addObject:@"Y"];
                [arr addObject:@"Z"];
            }

            float total_width = 0;
            for(NSString *letter in arr)    {
                total_width += [letter sizeWithFont:[UIFont systemFontOfSize:14.0f]].width;
            }

            float padding;
            float y;
            if([Helper isLandscapeOrientation]) {
                padding = (but_frame.size.width/2.0f) + 10;
                assert(_lbl_number);
                y = _lbl_number.center.y - 8;
            }
            else    {
                padding = (but_frame.size.width - total_width) / 2.0f;
                y = but_frame.size.height / 2.0f;
            }
            
            float x_current = padding;


            _arr_lbl_letters = [NSMutableArray arrayWithCapacity:4];
            for(NSString *letter in arr)    {
                UILabel *lbl_letter = [[UILabel alloc] initWithFrame:CGRectMake(x_current, y, 30, 20)];
                
                // UI - letters
                [lbl_letter setFont:[UIFont systemFontOfSize:14]];
                [lbl_letter setBackgroundColor:[UIColor clearColor]];
                [lbl_letter setTextColor:[UIColor colorWithWhite:0.5f alpha:1.0f]];
                [lbl_letter setText:letter];
                
                [_arr_lbl_letters addObject:lbl_letter];
                x_current += [letter sizeWithFont:[UIFont systemFontOfSize:14.0f]].width;
            }
        }
        
        _arr_lbls = [NSMutableArray arrayWithCapacity:4];
        if(_lbl_number) {
            // UI - number
            [_lbl_number setTextAlignment:UITextAlignmentCenter];
            [_lbl_number setBackgroundColor:[UIColor clearColor]];
            [_lbl_number setTextColor:[UIColor whiteColor]];
            [_lbl_number setFont:[UIFont systemFontOfSize:22]];
            [_lbl_number.layer setShadowOpacity:0.6f];

            [self addSubview:_lbl_number];
            [_arr_lbls addObject:_lbl_number];
        }
        if(_arr_lbl_letters)    {
            for(UILabel *lbl_letter in _arr_lbl_letters)    {
                [self addSubview:lbl_letter];
                [_arr_lbls addObject:lbl_letter];
            }
        }
        
        // Add tap recogniser (if tappable)
        if((index >=0 && index <9) || index == 10)  {
            [self addTarget:self action:@selector(btnTouchDown:) forControlEvents:UIControlEventTouchDown];
            [self addTarget:self action:@selector(btnTouch:) forControlEvents:UIControlEventTouchUpInside];
        }
        
        // Add logo
        if(index == 9)  {
            int logo_w = ceil(but_width / 3.0f);
            int logo_h = ceil(but_height/3.0f);
            UIImageView *img_view_logo = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, logo_w, logo_h)];
            [img_view_logo setContentMode:UIViewContentModeScaleAspectFit];
            [img_view_logo setImage:[UIImage imageNamed:@"logo"]];
            [img_view_logo setCenter:CGPointMake(but_width/2.0f, but_height/2.0f)];
            [self addSubview:img_view_logo];
        }
    
        // UI settings
        [self setBackgroundColor:[UIColor clearColor]];
        
        // Initialise properties
        _index_letter = 0;
    }
    return self;
}

#pragma mark - UI callbacks
- (void) btnTouchDown:(id)sender    {
    [self setBackgroundColor:[UIColor colorWithWhite:0.5f alpha:1.0f]];
}

- (void) btnTouch:(id)sender    {
    [self setBackgroundColor:[UIColor clearColor]];

    NSString *letter = ((UILabel *)[_arr_lbls objectAtIndex:_index_letter]).text;
    if(_delegate && [_delegate respondsToSelector:@selector(keypadButton:letterAppended:)]) {
        [_delegate keypadButton:self letterAppended:letter];
    }
    
    _last_touched_button = self;
    
    _index_letter = (_index_letter +1) % ([_arr_lbls count]);    
}

@end
