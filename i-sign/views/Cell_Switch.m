//
//  Cell_Switch.m
//  checkid
//
//  Created by Ismail Ege AKPINAR on 04/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "Cell_Switch.h"
#import "Debug.h"

@interface Cell_Switch()

#pragma mark - UI elements
@property (unsafe_unretained, nonatomic) IBOutlet UILabel *lbl;
@property (unsafe_unretained, nonatomic) IBOutlet UISwitch *switchOnOff;

#pragma mark - UI callbacks
- (IBAction)switchValueChanged:(id)sender;

@end

@implementation Cell_Switch

#pragma mark - Properties
- (void) setTitle:(NSString *)title {
    _title = title;
    [_lbl setText:title];
}

- (void) setIs_on:(BOOL)is_on   {
    _is_on = is_on;
    [_switchOnOff setOn:is_on];
}

#pragma mark - Default init methods
- (void) awakeFromNib   {
    [self setSelectionStyle:UITableViewCellSelectionStyleNone];
    
    // Configure autoresizing masks
    [_switchOnOff setAutoresizingMask:(UIViewAutoresizingFlexibleLeftMargin)];
    
    // Initialise properties
    _is_on = _switchOnOff.isOn;
}

- (IBAction)switchValueChanged:(id)sender {
    _is_on = _switchOnOff.isOn;
    if(_delegate && [_delegate respondsToSelector:@selector(cellSwitch:valueChanged:)])   {
        [_delegate cellSwitch:self valueChanged:sender];
    }
}
@end
