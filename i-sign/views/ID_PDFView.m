//
//  PDFView.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_PDFView.h"
#import "TiledPDFView.h"
#import "Debug.h"
#import "Common.h"
#import "Helper.h"
#import <QuartzCore/QuartzCore.h>
#import "ID_ResizableView.h"
#import <PDFTron/Headers/ObjC/PDFNetOBJC.h>

@interface ID_PDFView()<UIGestureRecognizerDelegate , UIScrollViewDelegate, ID_ResizableViewDelegate>    {
    CGPDFPageRef _PDFPage;
    
    // Current PDF zoom scale.
    CGFloat _PDFScale;
    CGRect _PDF_rect;
}

@property(nonatomic, retain) UIView *view_layer;    // Top layer that contains PDF and signatures

@property(nonatomic, retain) TiledPDFView *tiled_pdf_view;
@property (nonatomic, weak) UIImageView *img_back;

@property(nonatomic, retain) NSMutableArray *arr_signatures;
@property(nonatomic, retain) NSMutableDictionary *dict_highlight_fields;    // Dictionary of frames for highlights
@property(nonatomic, retain) NSMutableArray *arr_highlight_views;   // Array of highlight views themselves
@property(nonatomic, assign) BOOL are_highlights_ready; // YES if highlights are ready
@property(nonatomic, assign) BOOL is_dirty; // Whether document has been modified/signed

//@property(nonatomic, assign) CGPDFDocumentRef pdf_doc;

// Thumbnails
@property(nonatomic, retain) NSMutableArray *arr_thumbs;    // Array of thumbnail images
@property(nonatomic, retain) NSMutableArray *arr_thumb_views;  // Array of thumbnail imageview's (We need this to keep index-based reference to them, _scroll_view.subviews' indices are not reliable as they change with z-index re-ordering)
@property(nonatomic, retain) UIScrollView *scroll_thumbs;
@property(nonatomic, assign) float thumb_width; // Width of thumb images
@property(nonatomic, assign) float thumb_margin;    // Margin between thumb images
@property(nonatomic, assign) int highlighted_thumb; // Index of highlighted thumb image
@property(nonatomic, assign) double thumbnails_timestamp;    // Timestamp to avoid race condition
@property(nonatomic, assign) BOOL are_thumbs_ready;

@end

@implementation ID_PDFView

#pragma mark - Properties

// TODO: Add digital signature here

-(BOOL) is_dirty    {
    return _arr_signatures && [_arr_signatures count] > 0;
}

- (void) setPage_index:(int)page_index  {
    if(page_index < 0)  {
        warn(@"Invalid page index (%d), cannot be negative", page_index);
    }
    else if(page_index >= self.page_count)  {
        warn(@"Invalid page index (%d), cannot exceed page count" , page_index);
    }
    else    {
        if(page_index == _page_index)   {
            logg(@"Setting to same page index");
        }
        _page_index = page_index;
        
        // Hide all signatures
        for(UIView *view in _arr_signatures)   {
            [view setHidden:YES];
        }
        
        // Reset zoom and scroll to top
        [self setZoomScale:1.0f];
        [self setContentOffset:CGPointMake(0, 0)];
        
        

        
        CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)_pdf_url);
        
        CGPDFPageRef PDFPage = CGPDFDocumentGetPage(PDFDocument, page_index+1);
        
        [self setPDFPage:PDFPage];
        
        CGPDFDocumentRelease(PDFDocument);
        
        // Show signatures of this page
        // And reset signatures' highlight (so that scroll is not disabled due to an occluded signature)

        for(UIView *view in _arr_signatures)   {
            if(view.tag == _page_index) {
                [view setHidden:NO];
                [view setTransform:self.transform]; // Apply orientation (if any)
                [_view_layer bringSubviewToFront:view];
                [((ID_ResizableView *)view) unhighlight];
            }
        }
        
        // Highlight thumbnail
        // Ensure thumbnail is visible
        // Note: If thumbs are ready
        if(_are_thumbs_ready)    {
            UIImageView *img_active = (UIImageView *)[_arr_thumb_views objectAtIndex:_page_index];
            float active_x = (_thumb_width + _thumb_margin) * _page_index;
            float scroll_x_left = _scroll_thumbs.contentOffset.x;
            float scroll_x_right = _scroll_thumbs.contentOffset.x + _scroll_thumbs.frame.size.width;
            if(active_x > scroll_x_right)   {
                // Scroll to right
                [_scroll_thumbs setContentOffset:CGPointMake(active_x - _scroll_thumbs.frame.size.width, 0)];
            }
            else if(active_x < scroll_x_left)   {
                // Scroll to left
                [_scroll_thumbs setContentOffset:CGPointMake(active_x, 0)];
            }
            [UIView transitionWithView:_scroll_thumbs duration:0.1f options:UIViewAnimationOptionCurveLinear animations:^{
                [img_active setAlpha:0.2f];
            } completion:^(BOOL finished) {
                [img_active setAlpha:1.0f];
            }];
        }
        
        // Remove previous highlights (if any)
        if(_arr_highlight_views)    {
            [_arr_highlight_views makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [_arr_highlight_views removeAllObjects];
        }
        
        // Highlight signature fields
        [self highlightSignatureFields];

    }
}

- (void) setPdf_url:(NSURL *)pdf_url    {
    _pdf_url = pdf_url;
    
    // Reset properties
    _highlighted_thumb = -1;
    _page_index = 0;    // Reset page index

    // Clear signatures
    [self clearSignatures];
    
    CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)_pdf_url);
    
    int number_of_pages = CGPDFDocumentGetNumberOfPages(PDFDocument);
    _page_count = number_of_pages;
    
    CGPDFPageRef PDFPage = CGPDFDocumentGetPage(PDFDocument, 1);
    
    [self setPDFPage:PDFPage];
     
    CGPDFDocumentRelease(PDFDocument);
    
    _pdf_out_name = nil;
    _pdf_out_path = nil;
    
    [self resetScrollViewForThumbs];
    _are_thumbs_ready = NO;
    _are_highlights_ready = NO;
    [self performSelectorInBackground:@selector(generateThumbnails) withObject:nil];
    [self performSelectorInBackground:@selector(prepareHighlights) withObject:nil];
}




#pragma mark - Convenience methods

// Called in the background
- (void)generateThumbnails    {
    if([NSThread isMainThread]) {
        warn(@"Generating PDF thumbnails on the main thread, ill-advised");
    }
    _arr_thumbs = nil;  // Invalidate as soon as possible
    
    double current_timestamp = [Helper get_timestamp];
    _thumbnails_timestamp = current_timestamp;
    
    CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)_pdf_url);
    if(PDFDocument == NULL) {
        err(@"PDF doc null");
    }
    
    int number_of_pages = CGPDFDocumentGetNumberOfPages(PDFDocument);
    NSMutableArray *arr_thumbs_temp = [[NSMutableArray alloc] initWithCapacity:number_of_pages];
    
    for(int i=1; i<=number_of_pages; i++)    {
        // See if this should be cancelled (due to a later call to generateThumbnails)
        if(_thumbnails_timestamp != current_timestamp)  {
            return;
        }
        CGPDFPageRef pdf_page = CGPDFDocumentGetPage(PDFDocument, i);
        // Specify thumbnail size
        CGRect rect = CGRectMake(0, 0, 80, 120);
        UIGraphicsBeginImageContext(rect.size);
        
        CGContextRef context = UIGraphicsGetCurrentContext();
        
        // First fill the background with white.
        CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
        CGContextFillRect(context,rect);
        
        CGContextSaveGState(context);
        // Flip the context so that the PDF page is rendered right side up.
        CGContextTranslateCTM(context, 0.0, rect.size.height);
        CGContextScaleCTM(context, 1.0, -1.0);
        
        // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
        // Determine the size of the PDF page.
        CGSize pdf_size = CGPDFPageGetBoxRect(pdf_page, kCGPDFMediaBox).size;
        float scale_width = rect.size.width/pdf_size.width;
//        float scale_height = rect.size.height/pdf_size.height;
//        float scale = scale_width < scale_height ? scale_width : scale_height;
        float scale = scale_width;
        
        CGContextScaleCTM(context, scale,scale);
        CGContextDrawPDFPage(context, pdf_page);
        CGContextRestoreGState(context);
        
        UIImage *img_thumb = UIGraphicsGetImageFromCurrentImageContext();
        [arr_thumbs_temp addObject:img_thumb];
        UIGraphicsEndImageContext();        
    }
    CGPDFDocumentRelease(PDFDocument);

    // Configure thumbs
    // Check if view is visible
    if(self.superview)  {
        // Check if this is the most recent thumbnails call
        if(current_timestamp == _thumbnails_timestamp)   {
            [self performSelectorOnMainThread:@selector(configureScrollViewForThumbs:) withObject:arr_thumbs_temp waitUntilDone:NO];
        }
    }
}

- (void)setPDFPage:(CGPDFPageRef)PDFPage;
{
    // Reset zoom and scroll to top
    [self setZoomScale:1.0f];
    [self setContentOffset:CGPointMake(0, 0)];

    CGPDFPageRetain(PDFPage);
    CGPDFPageRelease(_PDFPage);
    _PDFPage = PDFPage;
    
    // Determine the size of the PDF page.
    _PDF_rect = CGPDFPageGetBoxRect(_PDFPage, kCGPDFMediaBox);
    float scale_width = self.frame.size.width/_PDF_rect.size.width;
//    float scale_height = self.frame.size.height/_PDF_rect.size.height;
//    _PDFScale = scale_width > scale_height ? scale_width : scale_height;
    _PDFScale = scale_width;
    
    // Remove existing PDF (if any)
    if(_tiled_pdf_view) {
        [_tiled_pdf_view removeFromSuperview];
        _tiled_pdf_view = nil;
    }
    
    
    /*
     Create a low resolution image representation of the PDF page to display before the TiledPDFView renders its content.
     */
    CGSize size_img = CGSizeMake(_PDF_rect.size.width * _PDFScale, _PDF_rect.size.height * _PDFScale);
    UIGraphicsBeginImageContext(size_img);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context,CGRectMake(0, 0, size_img.width, size_img.height));
    
    CGContextSaveGState(context);
    // Flip the context so that the PDF page is rendered right side up.
    CGContextTranslateCTM(context, 0.0, size_img.height);
    CGContextScaleCTM(context, 1.0, -1.0);
    
    // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
    CGContextScaleCTM(context, _PDFScale,_PDFScale);
    CGContextDrawPDFPage(context, _PDFPage);
    CGContextRestoreGState(context);
    
    UIImage *backgroundImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    
    if (self.img_back != nil) {
        [self.img_back  removeFromSuperview];
    }
    
    UIImageView *backgroundImageView = [[UIImageView alloc] initWithImage:backgroundImage];
    backgroundImageView.frame = CGRectMake(0, 0, size_img.width, size_img.height);
    backgroundImageView.contentMode = UIViewContentModeScaleAspectFit;
    [_view_layer addSubview:backgroundImageView];
    [_view_layer sendSubviewToBack:backgroundImageView];
    self.img_back = backgroundImageView;
    
    // e: Create a low resolution image
    
    // Create the TiledPDFView based on the size of the PDF page and scale it to fit the view.
    _tiled_pdf_view = [[TiledPDFView alloc] initWithFrame:CGRectMake(0, 0, _PDF_rect.size.width * _PDFScale, _PDF_rect.size.height * _PDFScale) scale:_PDFScale];
    [_tiled_pdf_view setPage:_PDFPage];

    [_view_layer addSubview:_tiled_pdf_view];
    
    [self setContentSize:CGSizeMake(_PDF_rect.size.width * _PDFScale, _PDF_rect.size.height * _PDFScale)];
    [_view_layer setFrame:CGRectMake(0,0,_PDF_rect.size.width * _PDFScale, _PDF_rect.size.height * _PDFScale)];
}

- (void) prepareHighlights {
    
    if(!_dict_highlight_fields) {
        _dict_highlight_fields = [NSMutableDictionary new];
    }
    else    {
        [_dict_highlight_fields removeAllObjects];
    }
    
    PDFDoc *doc = [[PDFDoc alloc] initWithFilepath:_pdf_url.path];
    
    FieldIterator *iter = [doc GetFieldIterator];
    while ([iter HasNext]) {
        TRNField *field = [iter Current];
        if (e_signature == [field GetType]) {
            
            // Generate widget (I don't know how to get field coordinates without a widget)
            Widget *widgetAnnot = [[Widget alloc] initWithD:[field GetSDFObj]];
            Page *page = [widgetAnnot GetPage];
            PDFRect *rect_page = [page GetMediaBox];
            int page_index = [page GetIndex];   // starts from 1
            PDFRect *rect_field = [field GetUpdateRect];
            
            // Translate to frame as percentage
            CGRect frame_highlight = CGRectMake(rect_field.GetX1, rect_field.GetY1, rect_field.Width, rect_field.Height);
            
            // Revert y (PDF starts as inverted)
            float y_prev = frame_highlight.origin.y;
            y_prev = rect_page.Height - y_prev;
            y_prev = y_prev - rect_field.Height;
            frame_highlight.origin.y = y_prev;

            // Scale will be applied when displaying
            
            NSString *key = [NSString stringWithFormat:@"%d",page_index];
            NSMutableArray *arr = [_dict_highlight_fields objectForKey:key];
            if(!arr) {
                arr = [NSMutableArray new];
            }
            [arr addObject:[NSValue valueWithCGRect:frame_highlight]];
            [_dict_highlight_fields setValue:arr forKey:key];
        }
        [iter Next];
    }
    _are_highlights_ready = YES;
    [self performSelectorOnMainThread:@selector(highlightSignatureFields) withObject:nil waitUntilDone:NO];
}

- (void) highlightSignatureFields   {
    if(![NSThread isMainThread])    {
        [self performSelectorOnMainThread:@selector(highlightSignatureFields) withObject:nil waitUntilDone:YES];
        return;
    }
    if(_are_highlights_ready && _dict_highlight_fields)  {
        NSString *dict_key = [NSString stringWithFormat:@"%d", _page_index+1];
        NSMutableArray *arr = [_dict_highlight_fields objectForKey:dict_key];
        if(arr && [arr count] > 0)  {
            if(!_arr_highlight_views)   {
                _arr_highlight_views = [[NSMutableArray alloc] initWithCapacity:[arr count]];
            }
            for(NSValue *val in arr)    {
                CGRect frame_highlight = val.CGRectValue;
                frame_highlight = CGRectApplyAffineTransform(frame_highlight, CGAffineTransformMakeScale(_PDFScale, _PDFScale));
                UIView *view_highlight = [[UIView alloc] initWithFrame:frame_highlight];
                [view_highlight setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:0.4f alpha:0.5f]];
                UITapGestureRecognizer *recogniser_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(clickedSignature:)];
                [view_highlight addGestureRecognizer:recogniser_tap];

                [_view_layer addSubview:view_highlight];
                [_view_layer bringSubviewToFront:view_highlight];
                [_arr_highlight_views addObject:view_highlight];
            }
        }
    }
}


- (void) clickedSignature:(UITapGestureRecognizer *)recogniser  {
    if(_delegate_pdfview && [_delegate_pdfview respondsToSelector:@selector(pdfViewSignatureClickedAtPoint:)])  {
        CGPoint point_touch = [recogniser locationInView:self];
        [_delegate_pdfview pdfViewSignatureClickedAtPoint:point_touch];
    }
}

#pragma mark - Public methods
- (void) nextPage   {
    [self setPage_index:self.page_index+1];
}

- (void) prevPage   {
    [self setPage_index:self.page_index-1];
}

- (void) clearSignatures    {
    for(UIView *view in _arr_signatures)   {
        [view removeFromSuperview];
    }
    [_arr_signatures removeAllObjects];
}

// TODO: This has to use PDFTron's methods instead, otherwise you're losing the PDF structure
- (BOOL) saveFileNamed:(NSString *)file_name toPath:(NSString *)file_path   {
    if(!self.is_dirty)    {
        err(@"Document not modified, won't save");
        return NO;
    }
    
    _pdf_out_name = file_name;
    _pdf_out_path = file_path;
    
    // Zoom out
    float prev_scale = self.zoomScale;
    CGRect prev_frame = self.frame;
    int prev_page_index = _page_index;
    // TODO: You might have to scroll to top here as well, but it's fine as we'll abandon this method soon
    
    [self setZoomScale:1.0f];

    
    UIGraphicsBeginPDFContextToFile(file_path, CGRectZero, nil);
    for(int i=0; i<_page_count; i++)    {
        UIGraphicsBeginPDFPage();
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(currentContext , 1.0f/_PDFScale , 1.0f/_PDFScale);
        if(!currentContext) {
            err(@"context is nil");
            return NO;
        }
        
        // Display page
        [self setPage_index:i];
        CGRect enlarged_frame = self.frame;
        enlarged_frame.size = self.contentSize;
        [self setFrame:enlarged_frame];
        
        CALayer *layer = self.layer;   // Whatever you see
        [layer renderInContext:currentContext]; // Add it to context
    }
    
    UIGraphicsEndPDFContext();  // Save PDF and end context

    [self setFrame:prev_frame];
    [self setZoomScale:prev_scale];
    [self setPage_index:prev_page_index];
    
    return YES;
}

- (NSData *)getSignedPDF    {
    if(!self.is_dirty)    {
        err(@"Document not modified, won't save");
        return nil;
    }
    
    // Zoom out
    float prev_scale = self.zoomScale;
    CGRect prev_frame = self.frame;
    int prev_page_index = _page_index;
    
    [self setZoomScale:1.0f];
    
    NSMutableData *data_out = [NSMutableData data];
    UIGraphicsBeginPDFContextToData(data_out, CGRectZero, nil);
    for(int i=0; i<_page_count; i++)    {
        UIGraphicsBeginPDFPage();
        CGContextRef currentContext = UIGraphicsGetCurrentContext();
        CGContextScaleCTM(currentContext , 1.0f/_PDFScale , 1.0f/_PDFScale);
        if(!currentContext) {
            err(@"context is nil");
            return NO;
        }
        
        // Display page
        [self setPage_index:i];
        CGRect enlarged_frame = self.frame;
        enlarged_frame.size = self.contentSize;
        [self setFrame:enlarged_frame];
        
        CALayer *layer = self.layer;   // Whatever you see
        [layer renderInContext:currentContext]; // Add it to context
    }
    
    UIGraphicsEndPDFContext();  // Save PDF and end context
    assert(data_out);
    [self setFrame:prev_frame];
    [self setZoomScale:prev_scale];
    [self setPage_index:prev_page_index];
    
    return data_out;
}

- (void) resetScrollViewForThumbs   {
    assert([NSThread isMainThread]);
    [[_scroll_thumbs subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    UIActivityIndicatorView *view_waiting = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    view_waiting.center = CGPointMake(_scroll_thumbs.frame.size.width / 2, _scroll_thumbs.frame.size.height / 2);
    [view_waiting startAnimating];
    
    [_scroll_thumbs addSubview:view_waiting];
}

- (void) configureScrollViewForThumbs:(NSMutableArray *)arr_imgs  {
    assert([NSThread isMainThread]);
    [[_scroll_thumbs subviews] makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    if(!arr_imgs || [arr_imgs count] <= 0 )   {
        err(@"no arr thumbs given");
        return;
    }
    assert(!_arr_thumbs);
    _arr_thumbs = arr_imgs;
    // Note: Assuming all thumbs have the same image size (important)
    // Note: Assuming scroll_thumbs's frame is configured properly
    if(!_arr_thumb_views)   {
        _arr_thumb_views = [[NSMutableArray alloc] initWithCapacity:_arr_thumbs.count];
    }
    else    {
        [_arr_thumb_views removeAllObjects];
    }
    
    float x =0;
    for(UIImage *img in _arr_thumbs)    {
        UIImageView *img_view = [[UIImageView alloc] initWithImage:img];
        float scale = img.size.height / _scroll_thumbs.frame.size.height;
        [img_view setFrame:CGRectMake(x, 0, img.size.width / scale, img.size.height / scale)];
        if(x == 0)  {
            _thumb_width = img.size.width / scale;
        }
        x += img.size.width / scale;
        x += _thumb_margin;

        // Add border around thumbs
        [img_view.layer setBorderWidth:0.4f];
        
        [_scroll_thumbs addSubview:img_view];
        [_arr_thumb_views addObject:img_view];
    }
    [_scroll_thumbs setContentSize:CGSizeMake(x, _scroll_thumbs.frame.size.height)];
    _are_thumbs_ready = YES;
}

- (void) parentFrameUpdatedTo:(CGRect)new_parent_frame    {
    float scale_width = self.frame.size.width/_PDF_rect.size.width;
//    float scale_height = self.frame.size.height/_PDF_rect.size.height;
//    _PDFScale = scale_width > scale_height ? scale_width : scale_height;
    _PDFScale = scale_width;
    
    // Force reload the current page
    // Note: This means initially the page is loaded twice but that's fine
    [self setPage_index:_page_index];
}

#pragma mark - Default init methods

- (id) initWithFrame:(CGRect)frame andScrollViewForThumbs:(UIScrollView *)scroll_view   {
    if(self = [super initWithFrame:frame])  {
        self.minimumZoomScale = 1.0f;
        self.maximumZoomScale = 5.0f;
        self.userInteractionEnabled = YES;
        self.delegate = self;
        self.scrollEnabled = YES;
        self.tag = 0;
        self.autoresizesSubviews = YES;
        self.bounces = YES;
        if(scroll_view) {
            _scroll_thumbs = scroll_view;
            _scroll_thumbs.tag = 1;
            [_scroll_thumbs setDelegate:self];
            [_scroll_thumbs setClipsToBounds:NO];   // So highlighted thumbs can be seen fully
            [_scroll_thumbs setScrollEnabled:YES];
            [_scroll_thumbs setBounces:NO];
            [_scroll_thumbs setDirectionalLockEnabled:YES];
            _thumb_margin = 2.0f;
            
            // Gesture recogniser (thumbs)
            UILongPressGestureRecognizer *recogniser_long_press = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
            [recogniser_long_press setDelegate:self];
            [_scroll_thumbs addGestureRecognizer:recogniser_long_press];
        }
        // Gesture recognisers
            // Swipe left
        UISwipeGestureRecognizer *recogniser_swipe_left = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeLeft:)];
        recogniser_swipe_left.numberOfTouchesRequired = 1;
        recogniser_swipe_left.direction = UISwipeGestureRecognizerDirectionLeft;
        [self addGestureRecognizer:recogniser_swipe_left];
            // Swipe right
        UISwipeGestureRecognizer *recogniser_swipe_right = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(handleSwipeRight:)];
        recogniser_swipe_right.numberOfTouchesRequired = 1;
        recogniser_swipe_right.direction = UISwipeGestureRecognizerDirectionRight;
        [self addGestureRecognizer:recogniser_swipe_right];
            // Double tap
        UITapGestureRecognizer *recogniser_double_tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(doubleTapped:)];
        recogniser_double_tap.numberOfTapsRequired = 2;
        [self addGestureRecognizer:recogniser_double_tap];

        _arr_signatures = [[NSMutableArray alloc] init];
        _scale_signature = 2.0f;
        
        _view_layer = [[UIView alloc] init];
        [_view_layer setClipsToBounds:YES];
        [self addSubview:_view_layer];
        
        
        // Initialise instance variables
        _highlighted_thumb = -1;
    }
    return self;
}

#pragma mark - UIScrollViewDelegate methods
- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView   {
    return _view_layer;
}
#pragma mark - UIGestureRecognizerDelegate methods

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRecognizeSimultaneouslyWithGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer {
    return YES;
}

#pragma mark - UI callbacks

- (void) doubleTapped:(UIGestureRecognizer *) recogniser    {
    
    if(recogniser.state == UIGestureRecognizerStateEnded)   {
        
        if(!_img_signature)  {
            warn(@"Signature image is nil, won't paste signature");
            return;
        }
        // Insert signature at the point of tap
        // We're using location in window to be zoom-invariant
        CGPoint point_window = [recogniser locationInView:nil];
        CGPoint point_in_view = [_view_layer convertPoint:point_window fromView:self.window];
        
        UIImageView *img_view_signature = [[UIImageView alloc] initWithImage:_img_signature];
        [img_view_signature setFrame:CGRectMake(point_in_view.x, point_in_view.y, _img_signature.size.width * _PDFScale * _scale_signature, _img_signature.size.height * _PDFScale * _scale_signature)];
        
        // Wrap it in a resizable view
        ID_ResizableView *resizable = [[ID_ResizableView alloc] initWithChild:img_view_signature andDelegate:self];
        [resizable setCenter:CGPointMake(point_in_view.x, point_in_view.y)];
        [resizable setTag:_page_index];    // Signatures belong to pages
        
        [_view_layer addSubview:resizable];
        [_view_layer bringSubviewToFront:resizable];
        
        [_arr_signatures addObject:resizable];
    }
    
}

- (void) handleSwipeLeft:(UISwipeGestureRecognizer *)recogniser {
    if([recogniser state] == UIGestureRecognizerStateEnded) {
        // Flip page only if zoomed out
        // This is a risky comparison
        if(self.zoomScale == 1) {
            [self nextPage];
        }
    }
}

- (void) handleSwipeRight:(UISwipeGestureRecognizer *)recogniser {
    if([recogniser state] == UIGestureRecognizerStateEnded) {
        // Flip page only if zoomed out
        if(self.zoomScale == 1) {
            [self prevPage];
        }
    }
}

- (void) handleLongPress:(UILongPressGestureRecognizer *)sender {
    if([sender state] == UIGestureRecognizerStateBegan) {
        [_scroll_thumbs setScrollEnabled:NO];
    }
    
    CGPoint location_touch = [sender locationInView:_scroll_thumbs];
    float touch_x = location_touch.x;
    
    // Highlight current thumb
    
    // Note: I feel there's an easier way for this
    // Determine which image is currently pressed
    float width = _thumb_width + _thumb_margin;
    int index = touch_x / width;
    if(index >= _arr_thumb_views.count) {   // At the far right end, they become equal
        index = _arr_thumb_views.count - 1;
    }
    if(index != _highlighted_thumb) {
        
        // De-highlight previously highlighted image
        if(_highlighted_thumb != -1)    {
            
            UIImageView *previous_pressed_view = [_arr_thumb_views objectAtIndex:_highlighted_thumb];
            [previous_pressed_view setTransform:CGAffineTransformIdentity];
        }
        _highlighted_thumb = index;
        
        // Highlight pressed view
        assert(index < _arr_thumb_views.count);
        UIImageView *pressed_view = [_arr_thumb_views objectAtIndex:index];
        CGAffineTransform transform_scale = CGAffineTransformScale(CGAffineTransformIdentity, 4.0f, 4.0f);
        CGAffineTransform transform_final = CGAffineTransformTranslate(transform_scale,0,-10);
        [pressed_view setTransform:transform_final];
        [_scroll_thumbs bringSubviewToFront:pressed_view];
    }
    
    if([sender state] == UIGestureRecognizerStateEnded) {
        [_scroll_thumbs setScrollEnabled:YES];
        
        // De-highlight pressed view
        // Flip to this page
        if(_highlighted_thumb != -1)    {
            
            UIImageView *previous_pressed_view = [_arr_thumb_views objectAtIndex:_highlighted_thumb];
            [previous_pressed_view setTransform:CGAffineTransformIdentity];
            
            [self setPage_index:_highlighted_thumb];
            
            _highlighted_thumb = -1;
        }
    }
    
}

#pragma mark - ID_ResizableViewDelegate methods

- (void)resizableViewHighlighted:(ID_ResizableView *)resizable  {
    // Lock scroll and zoom
    [self setScrollEnabled:NO];
    [self setMinimumZoomScale:self.zoomScale];
    [self setMaximumZoomScale:self.zoomScale];
}

- (void)resizableViewUnhighlighted:(ID_ResizableView *)resizable    {
    // Unlock scroll and zoom
    [self setScrollEnabled:YES];
    [self setMinimumZoomScale:1.0f];
    [self setMaximumZoomScale:5.0f];
}

- (void)resizableViewAttemptedDelete:(ID_ResizableView *)resizable  {
    [resizable removeFromSuperview];
    int prev_count = _arr_signatures.count;
    [_arr_signatures removeObject:resizable];
    int after_count = _arr_signatures.count;
    assert(after_count == prev_count - 1);
    
    // Unlock scroll and zoom
    [self setScrollEnabled:YES];
    [self setMinimumZoomScale:1.0f];
    [self setMaximumZoomScale:5.0f];
}

@end
