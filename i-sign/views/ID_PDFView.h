//
//  PDFView.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ID_PDFViewDelegate <NSObject>

- (void)pdfViewSignatureClickedAtPoint:(CGPoint)point;

@end

@interface ID_PDFView : UIScrollView

#pragma mark - Public properties
@property(nonatomic, copy) NSURL *pdf_url;
@property(nonatomic, assign) NSString *pdf_name;
@property(nonatomic, assign) int page_index;    // 0-based
@property(nonatomic, assign) int page_count;
@property(nonatomic, retain) UIImage *img_signature;
@property(nonatomic, assign) float scale_signature; // 1 by default (no scaling)
@property(nonatomic, assign) id<ID_PDFViewDelegate> delegate_pdfview;

@property(nonatomic, copy, readonly) NSString *pdf_out_name;    // Gets set when saveFile is called
@property(nonatomic, copy, readonly) NSString *pdf_out_path;    // Gets set when saveFile is called

#pragma mark - Public methods
// Note: Input scroll view's tag will be used, parent class should not try to assign it
- (id) initWithFrame:(CGRect)frame andScrollViewForThumbs:(UIScrollView *)scroll_view;

// Should be called when scroll view's frame is changed
- (void) configureScrollViewForThumbs;

// Should be called when frame of container view is changed (e.g. orientation change)
- (void) parentFrameUpdatedTo:(CGRect)new_parent_frame;

- (void) nextPage;
- (void) prevPage;
- (void) clearSignatures;

// Saves the PDF with signature(s) as a PDF file at the given path
// Returns YES if successful, NO otherwise
- (BOOL) saveFileNamed:(NSString *)file_name toPath:(NSString *)file_path;

// Returns signed PDF as data stream
- (NSData *) getSignedPDF;


@end
