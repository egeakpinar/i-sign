//
//  ID_ThumbPDF.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 24/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ID_ThumbPDF : UIImageView

- (id) initWithFrame:(CGRect)frame pdfUrl:(NSURL *)pdf_url;
- (id) initWithFrame:(CGRect)frame pdfUrl:(NSURL *)pdf_url pageIndex:(int)page_index;
- (void) configurePdfUrl:(NSURL *)pdf_url;

@end
