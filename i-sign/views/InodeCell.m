//
//  InodeCell.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 04/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "InodeCell.h"
#import "File.h"

@interface InodeCell()

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet UIImageView *img_icon;
@property (weak, nonatomic) IBOutlet UILabel *lbl_name;

@end


@implementation InodeCell

#pragma mark - Properties
- (void)setInode:(Inode *)inode {
    [_img_icon setHidden:NO];
    if([inode isFolder])    {
        [_img_icon setImage:[UIImage imageNamed:@"icon-folder"]];
    }
    else    {
        file_types file_type = ((File *)inode).type;
        if(file_type == PDF)    {
            [_img_icon setImage:[UIImage imageNamed:@"icon-pdf"]];
        }
        else    {
            [_img_icon setHidden:YES];
        }
    }
    
    [_lbl_name setText:inode.name];
}

#pragma mark - Default init methods
- (void) awakeFromNib   {
    [self setSelectionStyle:UITableViewCellSelectionStyleGray];
}

@end
