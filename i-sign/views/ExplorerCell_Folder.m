//
//  ExplorerCell_Folder.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ExplorerCell_Folder.h"
#import "Debug.h"

@interface ExplorerCell_Folder()

#pragma mark - UI elements
@property (weak, nonatomic) IBOutlet UILabel *lbl_title;

@end

@implementation ExplorerCell_Folder

#pragma mark - Properties
- (void) setInode:(Inode *)inode    {
    if(!inode)  {
        warn(@"Set inode is nil");
    }    
    [_lbl_title setText:inode.name];
}


@end
