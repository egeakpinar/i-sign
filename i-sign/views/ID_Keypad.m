//
//  ID_Keypad.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 30/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_Keypad.h"
#import "Helper.h"
#import "Debug.h"
#import "Logger.h"
#import <QuartzCore/QuartzCore.h>
#import "ID_KeypadButton.h"

@interface ID_Keypad()<ID_KeypadButtonProtocol>

#pragma mark - Private properties
@property(nonatomic, copy) NSString *input_result;
@property(nonatomic, retain) UINavigationItem *nav_item;
@property(nonatomic, retain) UINavigationBar *nav_bar;

@end

@implementation ID_Keypad

static const int height_top_menu = 32;  // Height of top menu (with Cancel/Done buttons)
static const int height_portrait_iphone = 216;
static const int height_landscape_iphone = 162;
static const int height_portrait_ipad = 264;
static const int height_landscape_ipad = 352;

#pragma mark - Public properties
static BOOL _is_shown = NO;

- (BOOL)isShown {
    return _is_shown;
}

#pragma mark - Convenience methods

// Returns visible frame the keypad should have
- (CGRect) getFrame {
    if(IS_IPAD) {
        if([Helper isLandscapeOrientation]) {
            float width = height_landscape_ipad + height_top_menu;
            float y = [Helper device_width] - width;
            return CGRectMake(y, 0, width, [Helper device_height]);
        }
        else    {
            float height = height_portrait_ipad + height_top_menu;
            float y = [Helper device_height] - height;
            return CGRectMake(0, y, [Helper device_width], height);
        }
    }
    else    {
        if([Helper isLandscapeOrientation]) {
            float width = height_landscape_iphone + height_top_menu;
            float y = [Helper device_width] - width;
            return CGRectMake(y, 0, width, [Helper device_height]);
        }
        else    {
            float height = height_portrait_iphone + height_top_menu;
            float y = [Helper device_height] - height;
            return CGRectMake(0, y, [Helper device_width], height);
        }        
    }
}

// Adjusts frames of subviews
// Should be called after a frame update
- (void) configureSubviewFrames    {
    // Note: Subviews do not have transform (unlike their parent view keypad)
    // Get width
    CGRect frame = [self getFrame];
    float visible_width = frame.size.height > frame.size.width ? frame.size.height : frame.size.width;
    
    // View top
    UINavigationBar *view_top = (UINavigationBar *)[self viewWithTag:1];
    assert(view_top);
    [view_top setFrame:CGRectMake(0, 0, visible_width, height_top_menu)];
    
    // Remove all keypad buttons
    for(int i=0; i<12; i++) {
        ID_KeypadButton *btn = (ID_KeypadButton *)[self viewWithTag:90+i];
        [btn removeFromSuperview];
        btn = nil;
    }
    
    // Re-add all keypad buttons
    CGRect frame_keyboard;
    if([Helper isLandscapeOrientation]) {
        frame_keyboard = CGRectMake(0, height_top_menu, frame.size.height, frame.size.width - height_top_menu);
    }
    else    {
        frame_keyboard = CGRectMake(0, height_top_menu, frame.size.width, frame.size.height - height_top_menu);
    }
    for(int i=0; i<12; i++) {
        ID_KeypadButton *btn = [[ID_KeypadButton alloc] initWithKeyboardFrame:frame_keyboard index:i delegate:self];
        btn.tag = i+90;
        [self addSubview:btn];
    }
    
    
}

#pragma mark - Public methods
- (void) show   {
    if(_is_shown)    {
        warn(@"Keypad already shown");
        return;
    }
    _is_shown = YES;

    // Update frame (in case there's an orientation change)
    CGRect frame_ideal = [self getFrame];
    if(self.frame.size.width != frame_ideal.size.width ||
       self.frame.size.height != frame_ideal.size.height ||
       self.frame.origin.x != frame_ideal.origin.x ||
       self.frame.origin.y != frame_ideal.origin.y)  {
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        
        UIViewController *vc_reference = window.rootViewController;
        if(vc_reference.presentedViewController)    {   // For when presented from a modal VC
            vc_reference = vc_reference.presentedViewController;
        }
        [self setTransform:vc_reference.view.transform];
        
        [self setFrame:[self getFrame]];
        [self configureSubviewFrames];
        [self setNeedsDisplay];
    }
    // e: Update frame
    
    // Reset state
    [self disableSubmission:NO animated:NO];
    
    float center_x_visible = frame_ideal.origin.x + (frame_ideal.size.width/2.0f);
    float center_y_visible = frame_ideal.origin.y + (frame_ideal.size.height/2.0f);
    
    [[[[UIApplication sharedApplication] windows] objectAtIndex:0] addSubview:self];
    
    // Pre-animation state
    CGPoint center_gone;
    if([Helper isLandscapeOrientation]) {
        center_gone = CGPointMake(center_x_visible + self.frame.size.width, center_y_visible);
    }
    else    {
        center_gone = CGPointMake(center_x_visible, center_y_visible + self.frame.size.height);
    }
    [self setCenter:center_gone];

    // Animate
    [UIView transitionWithView:self duration:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self setCenter:CGPointMake(center_x_visible, center_y_visible)];
    } completion:nil];
}

- (void) hide   {
    _is_shown = NO;
    
    CGRect frame_visible = [self getFrame];    
    float center_x_visible = frame_visible.origin.x + (frame_visible.size.width/2.0f);
    float center_y_visible = frame_visible.origin.y + (frame_visible.size.height/2.0f);
    
    CGPoint center_gone;
    if([Helper isLandscapeOrientation]) {
        center_gone = CGPointMake(center_x_visible + self.frame.size.width, center_y_visible);
    }
    else    {
        center_gone = CGPointMake(center_x_visible, center_y_visible + self.frame.size.height);
    }
    
    // Animate
    [UIView transitionWithView:self duration:0.3 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [self setCenter:center_gone];
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
    
    // TODO: Add a keypadCancelled callback here maybe
}

- (void) reset  {
    _input_result = nil;
}

- (void) disableSubmission:(BOOL)should_disable animated:(BOOL)animated {
    if(should_disable)  {
        UIActivityIndicatorView *view_waiting = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        [view_waiting startAnimating];

        UIBarButtonItem *btn_waiting = [[UIBarButtonItem alloc] initWithCustomView:view_waiting];
        UIBarButtonItem *btn_space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
        btn_space.width = 20;
        [_nav_item setRightBarButtonItems:[NSArray arrayWithObjects:btn_space, btn_waiting, nil]];
        [_nav_bar setItems:[NSArray arrayWithObject:_nav_item] animated:animated];
    }
    else    {
        UIBarButtonItem *btn_done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(btnDoneTouch:)];
        [_nav_item setRightBarButtonItems:[NSArray arrayWithObject:btn_done]];  // Note: setRightBarButtonItem doesn't get rid of the other items (e.g. waiting animation), use objectS instead
        [_nav_bar setItems:[NSArray arrayWithObject:_nav_item] animated:animated];
    }
}

#pragma mark - Initialiser
- (id) init {
    CGRect frame = [self getFrame];
    if(self = [super initWithFrame:frame]) {
        // Add top menu
        _nav_bar = [[UINavigationBar alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, height_top_menu)];
        [_nav_bar setBarStyle:UIBarStyleBlack];
        [_nav_bar setTranslucent:YES];
        _nav_bar.tag = 1;
        [self addSubview:_nav_bar];

        UIBarButtonItem *btn_cancel = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(btnCancelTouch:)];

        UIBarButtonItem *btn_done = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(btnDoneTouch:)];

        _nav_item = [UINavigationItem new];
        [_nav_item setHidesBackButton:NO];
        
        [_nav_item setLeftBarButtonItem:btn_cancel];
        [_nav_item setRightBarButtonItem:btn_done];
        
        [_nav_bar setItems:[NSArray arrayWithObject:_nav_item]];

        // Add all keypad buttons
        CGRect frame_keyboard = CGRectMake(0, height_top_menu, frame.size.width, frame.size.height - height_top_menu);
        for(int i=0; i<12; i++) {
            ID_KeypadButton *btn = [[ID_KeypadButton alloc] initWithKeyboardFrame:frame_keyboard index:i delegate:self];
            btn.tag = i+90;
            [self addSubview:btn];            
        }
        
        [self setAutoresizingMask:UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleTopMargin];
        
        [self setBackgroundColor:[UIColor clearColor]];
        
        [self setContentMode:UIViewContentModeRedraw];
    }
    return self;
}

#pragma mark - UI callbacks
- (void) btnCancelTouch:(id) sender {
    if(_delegate && [_delegate respondsToSelector:@selector(keypadInputCancelled)])   {
        [_delegate keypadInputCancelled];
    }
    [self hide];
}

- (void) btnDoneTouch:(id) sender   {
    if(_delegate && [_delegate respondsToSelector:@selector(keypadInputSubmitted)])   {
        [_delegate keypadInputSubmitted];
    }
    
}

#pragma mark - ID_KeypadButtonProtocol callbacks

- (void)keypadButton:(ID_KeypadButton *)keypad_button letterAppended:(NSString *)letter {
    if(!_input_result)  {
        _input_result = @"";
    }
    _input_result = [_input_result stringByAppendingString:letter];
    
    // Notify delegate
    if(_delegate && [_delegate respondsToSelector:@selector(keypad:inputChangedTo:)])   {
        [_delegate keypad:self inputChangedTo:_input_result];
    }
}

- (void) keypadButton:(ID_KeypadButton *)keypad_button letterChangedTo:(NSString *)letter   {
    assert(_input_result && _input_result.length > 0);
    // Change last character
    if(_input_result.length > 1)    {
        _input_result = [NSString stringWithFormat:@"%@%@",     [_input_result substringToIndex:_input_result.length-2],letter];
    }
    else    {
        _input_result = [NSString stringWithFormat:@"%@",letter];
    }

    // Notify delegate
    if(_delegate && [_delegate respondsToSelector:@selector(keypad:inputChangedTo:)])   {
        [_delegate keypad:self inputChangedTo:_input_result];
    }
}

#pragma mark - Default methods
- (void)drawRect:(CGRect)rect   {
    CGSize bounds = self.bounds.size;
    
    CGContextRef current_context = UIGraphicsGetCurrentContext();
    
    CGGradientRef gloss_gradient;
    CGColorSpaceRef rgb_space;
    size_t num_locations = 2;
    CGFloat locations[2] = { 0.0, 1.0 };
    CGFloat components[8] = { 1.0, 1.0, 1.0, 0.35,  // Start color
        1.0, 1.0, 1.0, 0.15 }; // End color
    rgb_space = CGColorSpaceCreateDeviceRGB();
    gloss_gradient = CGGradientCreateWithColorComponents(rgb_space, components, locations, num_locations);

/*    // Draw background (top)
    
        // First fill it with gray
    CGContextSetFillColorWithColor(current_context, [UIColor colorWithWhite:0.2f alpha:0.8f].CGColor);
    CGContextFillRect(current_context, CGRectMake(0, 0, current_bounds.size.width, height_top_menu));
    
        // Then apply gradient
    CGContextDrawLinearGradient(current_context, gloss_gradient, CGPointMake(0, 0), CGPointMake(0, 0 + height_top_menu), 0);
  */  
    
    // Draw background (keyboard)
    float y_offset_keypad = height_top_menu;
    float height_keypad = bounds.height - y_offset_keypad;
    
        // First fill it with black
    CGContextSetFillColorWithColor(current_context, [UIColor blackColor].CGColor);
    CGContextFillRect(current_context, CGRectMake(0, y_offset_keypad, bounds.width, height_keypad));

        // 4 lines with gradient
    float background_row_height = height_keypad / 4.0f;
    float y = y_offset_keypad;
    CGContextDrawLinearGradient(current_context, gloss_gradient, CGPointMake(0, y), CGPointMake(0, y+background_row_height), 0);
    y += background_row_height;
    CGContextDrawLinearGradient(current_context, gloss_gradient, CGPointMake(0, y), CGPointMake(0, y+background_row_height), 0);
    y += background_row_height;
    CGContextDrawLinearGradient(current_context, gloss_gradient, CGPointMake(0, y), CGPointMake(0, y+background_row_height), 0);
    y += background_row_height;    
    CGContextDrawLinearGradient(current_context, gloss_gradient, CGPointMake(0, y), CGPointMake(0, y+background_row_height), 0);
    
        // Draw vertical separators
    CGContextSetLineCap(current_context, kCGLineCapSquare);
    CGContextSetStrokeColorWithColor(current_context, [UIColor colorWithWhite:0.15f alpha:0.8f].CGColor);
    CGContextSetLineWidth(current_context, 1.0);
    float x = 0;
    y = y_offset_keypad;
    CGContextMoveToPoint(current_context, x+0.5f,y);
    CGContextAddLineToPoint(current_context, x+0.5f, y + height_keypad);
    CGContextStrokePath(current_context);
    x += bounds.width / 3.0f;
    CGContextMoveToPoint(current_context, x,y);
    CGContextAddLineToPoint(current_context, x, y + height_keypad);
    CGContextStrokePath(current_context);
    x += bounds.width / 3.0f;
    CGContextMoveToPoint(current_context, x,y);
    CGContextAddLineToPoint(current_context, x, y + height_keypad);
    CGContextStrokePath(current_context);
    x += bounds.width / 3.0f;
    CGContextMoveToPoint(current_context, x-0.5f,y);
    CGContextAddLineToPoint(current_context, x-0.5f, y + height_keypad);
    CGContextStrokePath(current_context);
    
        // Draw horizontal separator at the top
    CGContextSetStrokeColorWithColor(current_context, [UIColor colorWithWhite:0 alpha:0.8f].CGColor);
//    CGContextSetLineWidth(current_context, 3.0);
    CGContextMoveToPoint(current_context, 0.5f,y_offset_keypad-0.5f);
    CGContextAddLineToPoint(current_context, bounds.width - 0.5f, y_offset_keypad-0.5f);
    CGContextStrokePath(current_context);
}

@end
