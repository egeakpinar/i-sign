//
//  ID_ThumbPDF.m
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 24/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "ID_ThumbPDF.h"
#import <QuartzCore/QuartzCore.h>
#import "Debug.h"

@interface ID_ThumbPDF()

#pragma mark - Private properties
@property(nonatomic, copy) NSURL *pdf_url;
@property(nonatomic, assign) int page_index;

@end

@implementation ID_ThumbPDF

static NSLock *_lock_cache;
static NSCache *_cache_thumbs;
static NSOperationQueue *_op_q;

#pragma mark - Initialisers
- (void)awakeFromNib    {
    [self.layer setShadowRadius:1.5f];
    [self.layer setShadowOffset:CGSizeMake(-3.0, -3.0)];
}

- (id) initWithFrame:(CGRect)frame pdfUrl:(NSURL *)pdf_url  {
    return [self initWithFrame:frame pdfUrl:pdf_url pageIndex:1];
}

- (id) initWithFrame:(CGRect)frame pdfUrl:(NSURL *)pdf_url pageIndex:(int)page_index   {
    assert(pdf_url);
    _pdf_url = pdf_url;
    _page_index = page_index;
    if(self = [super initWithFrame:frame])  {
        [self setClipsToBounds:YES];
        [self.layer setShadowRadius:1.5f];
        [self.layer setShadowOffset:CGSizeMake(-3.0, -3.0)];

        [self configurePdfUrl:_pdf_url];
    }
    return self;
}

- (void) configurePdfUrl:(NSURL *)pdf_url   {
    if([pdf_url.path isEqualToString:_pdf_url.path]) {
        return;
    }

    _pdf_url = pdf_url;
    _page_index = 1;

    [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

    // Check if it exists in cache
    if(_cache_thumbs && _lock_cache)   {
        UIImage *cached_image = nil;
        @synchronized(_lock_cache)  {
            cached_image = [_cache_thumbs objectForKey:[NSString stringWithFormat:@"%@_%d",_pdf_url.absoluteString,_page_index]];
        }
        if(cached_image)    {
            [self setPreview:cached_image];
            return;
        }
    }

    // Show a waiting animation until preview is loaded
    // TODO: These waiting animations' frames will most probably break when orientation is changed while they're still visible
    UIActivityIndicatorView *view_waiting = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
    [view_waiting setFrame:self.frame];
    [view_waiting setCenter:CGPointMake(self.frame.size.width/2.0f, self.frame.size.height/2.0f)];
    [view_waiting startAnimating];
    [self addSubview:view_waiting];

    // Load preview in the background
    [self.layer setShadowOpacity:0.5f];
    if(!_op_q)  {
        _op_q = [[NSOperationQueue alloc] init];
        [_op_q setMaxConcurrentOperationCount:1];
    }
    NSInvocationOperation* theOp = [[NSInvocationOperation alloc]
                                    initWithTarget:self
                                    selector:@selector(loadPreview) object:nil];
    [_op_q addOperation:theOp];
}

#pragma mark - Convenience methods

- (void) loadPreview    {
    CGPDFDocumentRef PDFDocument = CGPDFDocumentCreateWithURL((__bridge CFURLRef)_pdf_url);
    if(PDFDocument == NULL) {
        NSLog(@"Error - PDF doc null (%@)",_pdf_url);
        return;
    }


    int number_of_pages = CGPDFDocumentGetNumberOfPages(PDFDocument);
    if(_page_index < 1 || _page_index > number_of_pages)    {
        NSLog(@"Error - PDF page index (%d) out of bounds (%d)",_page_index , number_of_pages);
        return;
    }

    CGPDFPageRef pdf_page = CGPDFDocumentGetPage(PDFDocument, 1);

    // Specify thumbnail size
    CGRect rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
    if(rect.size.width <= 0 || rect.size.height <= 0)   {
        NSLog(@"Error - Frame not set yet");
        return;
    }
    UIGraphicsBeginImageContextWithOptions(rect.size , YES, 1.0f);
    CGContextRef context = UIGraphicsGetCurrentContext();

    // TODO: Do I need background?
    // First fill the background with white.
    CGContextSetRGBFillColor(context, 1.0,1.0,1.0,1.0);
    CGContextFillRect(context,rect);

    CGContextSaveGState(context);
    // Flip the context so that the PDF page is rendered right side up.
    CGContextTranslateCTM(context, 0.0, rect.size.height);
    CGContextScaleCTM(context, 1.0, -1.0);

    // Scale the context so that the PDF page is rendered at the correct size for the zoom level.
    // Determine the size of the PDF page.
    CGSize pdf_size = CGPDFPageGetBoxRect(pdf_page, kCGPDFMediaBox).size;
    float scale_width = rect.size.width/pdf_size.width;
    float scale_height = rect.size.height/pdf_size.height;
    float scale = scale_width < scale_height ? scale_width : scale_height;

    CGContextScaleCTM(context, scale,scale);
    CGContextDrawPDFPage(context, pdf_page);
    CGContextRestoreGState(context);

    UIImage *img_thumb = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGPDFDocumentRelease(PDFDocument);

    if(img_thumb)   {
        [self performSelectorOnMainThread:@selector(setPreview:) withObject:img_thumb waitUntilDone:NO];

        // Add image to cache
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            _cache_thumbs = [[NSCache alloc] init];
            _lock_cache = [NSLock new];
        });
        @synchronized(_lock_cache)   {
            [_cache_thumbs setObject:img_thumb forKey:[NSString stringWithFormat:@"%@_%d",_pdf_url.absoluteString,_page_index]];
        }
    }
    else    {
        NSLog(@"Warning - Failed to generate PDF file preview");
    }
}

- (void) setPreview:(UIImage *)img_preview  {
    assert(img_preview);
    if(img_preview) {
        [self.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];

        [self setImage:img_preview];
        [self.layer setShadowOpacity:0.5f];
    }
}
@end
