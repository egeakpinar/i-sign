//
//  ID_KeypadButton.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 01/05/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ID_KeypadButton;

@protocol ID_KeypadButtonProtocol <NSObject>

- (void) keypadButton:(ID_KeypadButton *) keypad_button letterAppended:(NSString *)letter;
- (void) keypadButton:(ID_KeypadButton *) keypad_button letterChangedTo:(NSString *)letter;

@end
@interface ID_KeypadButton : UIButton

// Default initialiser
// keyboard_frame : Frame of entire keyboard, excluding top menu (calculates its frame automatically based on the index)
// index: index of button (0th -> 1, 1st -> 2 ABC)
// yOffset: y offset to move buttons up/down vertically
- (id)initWithKeyboardFrame:(CGRect)keyboard_frame index:(int)index delegate:(id<ID_KeypadButtonProtocol>)delegate;

@end
