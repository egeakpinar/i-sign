//
//  ExplorerCell.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 15/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Inode.h"

@interface ExplorerCell : UIView

#pragma mark - Properties
@property(nonatomic, retain) Inode *inode;

@end
