//
//  SignView1.m
//  TouchToSign1
//
//  Created by Ismail Ege AKPINAR on 05/03/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import "SignView1.h"
#import "Debug.h"
#import "Common.h"
#import <QuartzCore/QuartzCore.h>

@interface SignView1() {
    CGPoint point_curr;
    CGPoint point_prev;
    BOOL is_first_touch;
    float max_y, max_x, min_y, min_x;   // Edge points to crop image
}

@end

@implementation SignView1

#pragma mark - Public methods

- (void) clear  {
    self.image = nil;
    min_x = -1;
    min_y = -1;
    max_y = -1;
    max_x = -1;
}

- (UIImage *) getCroppedImage   {
    if(!self.image) {
        // No line drawn
        warn(@"No signature present");
        return nil;
    }
    UIGraphicsBeginImageContextWithOptions(self.frame.size , NO, 1.0f);
    CGContextRef ctx = UIGraphicsGetCurrentContext();
    if(!ctx)    {
        err(@"context is nil");
    }
    CALayer *layer = self.layer;
    [layer renderInContext:ctx];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    assert(img);
    
    // Crop to visible region
    float padding = 4.0f;
    UIImage *img_cropped = [Common image:img croppedAt:CGRectMake(min_x - padding, min_y - padding, (max_x - min_x) + (2 * padding), (max_y-min_y) + (2*padding))];
    
    return img_cropped;
}

#pragma mark - Touch callbacks

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event    {
    is_first_touch = YES;
    UITouch*	touch = [[event touchesForView:self] anyObject];
    point_curr = [touch locationInView:self];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event    {
	UITouch*			touch = [[event touchesForView:self] anyObject];
    
    if(is_first_touch)  {
        is_first_touch = NO;
        point_prev = point_curr;
        point_curr = [touch locationInView:self];
    }
    else    {
        point_curr = [touch locationInView:self];
        point_prev = [touch previousLocationInView:self];
    }
    
    // Draw line
    [self drawLineFrom:point_prev to:point_curr];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event    {
	UITouch*			touch = [[event touchesForView:self] anyObject];
    
    if(is_first_touch)  {
        is_first_touch = NO;
        point_prev = [touch previousLocationInView:self];
    }
    
    // Draw line
    [self drawLineFrom:point_prev to:point_curr];
}

#pragma mark - Convenience methods

- (void) drawLineFrom:(CGPoint) point_start to:(CGPoint) point_end   {
    UIGraphicsBeginImageContext(self.frame.size);
    [self.image drawInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    CGContextSetLineJoin(UIGraphicsGetCurrentContext(), kCGLineJoinMiter);
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), 5.0);
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), 0.0, 0.0, 0.0, 1.0);
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), point_end.x, point_end.y);
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), point_start.x, point_start.y);
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    self.image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    float current_min_x = point_start.x < point_end.x ? point_start.x : point_end.x;
    float current_min_y = point_start.y < point_end.y ? point_start.y : point_end.y;
    float current_max_x = point_start.x > point_end.x ? point_start.x : point_end.x;
    float current_max_y = point_start.y > point_end.y ? point_start.y : point_end.y;
    
    if(min_x == -1 || current_min_x <= min_x) {
        min_x = current_min_x;
    }
    if(min_y == -1 || current_min_y <= min_y) {
        min_y = current_min_y;
    }
    if(max_x == -1 || current_max_x >= max_x) {
        max_x = current_max_x;
    }
    if(max_y == -1 || current_max_y >= max_y) {
        max_y = current_max_y;
    }
}



#pragma mark - Default methods

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        // Initialize instance variables
        is_first_touch = NO;
        [self setUserInteractionEnabled:YES];
        [self clear];
    }
    return self;
}

- (id) init {
    if(self = [super init]) {
        // Initialize instance variables
        is_first_touch = NO;
        [self setUserInteractionEnabled:YES];
        [self clear];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    if(self = [super initWithCoder:aDecoder])   {
        // Initialize instance variables
        is_first_touch = NO;
        [self setUserInteractionEnabled:YES];
        [self clear];

    }
    return self;
}
@end
