//
//  AppDelegate.h
//  i-sign
//
//  Created by Ismail Ege AKPINAR on 02/04/2013.
//  Copyright (c) 2013 Ege AKPINAR. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
