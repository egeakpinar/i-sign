//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2012 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------

#import <Foundation/Foundation.h>


@interface NSObject (NSObjectInitWithCptr)
- (id)initWithCptr: (void*) cptr;
@end
