//
//  Copyright 2011-2012 PDFTron Systems Inc All rights reserved.
//

#import "NSObjectInitWithCptr.h"

@implementation NSObject (NSObjectInitWithCptr)

-(id)initWithCptr: (void*) cptr
{
    return self;
}

@end
