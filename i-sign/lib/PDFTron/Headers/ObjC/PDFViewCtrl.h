//---------------------------------------------------------------------------------------
// Copyright (c) 2001-2012 by PDFTron Systems Inc. All Rights Reserved.
// Consult legal.txt regarding legal and license information.
//---------------------------------------------------------------------------------------
//
//  IMPORTANT:
//  1. Call the method [PDFNet Initialize:@""] to initialize PDFNet in demo mode.
//  2. Read "About PDFNet SDK for iOS" readme.html in the Sample folder for a list of files
//     you must include in an app that uses PDFViewCtrl.


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "PDFNetOBJC.h"
#import <CoreText/CoreText.h>



@class PDFViewIOS;

@protocol PDFViewCtrlDelegate;
@protocol ToolDelegate;

typedef enum {
    e_trn_fit_page,
    e_trn_fit_width,
    e_trn_fit_height,
    e_trn_zoom
} TrnPageViewMode;

typedef enum {
    e_trn_single_page = 1, 	
    e_trn_single_continuous, 	
    e_trn_facing, 	
    e_trn_facing_continuous, 	
    e_trn_facing_cover, 	
    e_trn_facing_continuous_cover 
} TrnPagePresentationMode;

typedef enum {
    e_trn_zoom_limit_absolute = 1,
    e_trn_zoom_limit_relative,
    e_trn_zoom_limit_none
} TrnZoomLimitMode;


@interface PDFViewCtrl : UIView<UIScrollViewDelegate, UIGestureRecognizerDelegate, UIPrintInteractionControllerDelegate> {
    
    @public
    // required by Tools code only
    UIView* ContainerView;
    PDFDoc* m_annot_holder_doc;


    @private
    UIScrollView* innerScrollView;
    UIScrollView* outerScrollView;
    PDFViewIOS* IOSPDFView;
	CGRect Frame;
    BOOL midZoom;
    BOOL postZoom;
    BOOL delayRender;
    BOOL m_downloaded_doc;
    int touchedPageNumber;
    NSMutableDictionary* blankPages;
    NSMutableDictionary* tilesOnScreen;
    NSMutableArray* tilesPendingDeletion;
    double lastYPos;
    double lastXPos;
    BOOL searching;
    NSTimer* m_timer;
    BOOL m_new_doc;
    BOOL m_links_enabled;
    BOOL m_annotation_editing_enabled;
    BOOL m_progressive_rendering_enabled;
    double m_progressive_rendering_interval;
    BOOL m_progressive_render_override;
    double m_screenScale;
    UIColor* pageColor;
    int m_tool_switches;
    BOOL m_no_core_scroll;
    PDFDoc* m_current_doc;
    UIColor* m_Last_highlight_colour;
    BOOL m_request_rending_upon_doc_unlock;
    TrnZoomLimitMode m_zoom_mode;
    double m_zoom_limit_ref_zoom;
    BOOL m_freeze_tile;
    BOOL m_request_pending;
    unsigned int m_memoryInUse;
    NSMutableArray* m_highlights;
    PDFPoint* pagePtA;
    PDFPoint* pagePtB;
    int pageNumber;
    double totalZoom;
    double totalXExtra;
    double totalYExtra;
    double minimumZoom, maximumZoom;
    double m_min_scaled_zoom, m_max_scaled_zoom;
    BOOL m_zoom_enabled;
    int gapYExtra;
    int gapXExtra;
    NSMutableDictionary* thumbnails;
    PDFDoc* m_default_annots;
    NSDictionary* AnnotTypeID;
    BOOL m_rightToLeftLanguage;
}

@property (nonatomic, assign) id<PDFViewCtrlDelegate> delegate;
@property (nonatomic, retain) UIView<ToolDelegate>* tool;
@property (nonatomic, assign) BOOL scrollEnabled;
@property (nonatomic, assign) BOOL zoomEnabled;

//-- Extra iOS methods

/** 
 * Enables or disables the user's ability to create and interact with annotations, 
 * except for the ability to follow links (that controlled with EnableLinkActivation).
 *
 * Default is enabled.
 *
 * @param enable - If annotation editing is to be enabled.
 *
 */
-(void)SetAnnotationEditingEnabled:(BOOL)enable;

/**
 * Gets whether annotation editing is enabled.
 * 
 * @return true if annotation editing is enabled.
 */
-(BOOL)AnnotationEditingEnabled;


/**
 * Gets whether link activation is enabled.
 * 
 * @return true if link activation is enabled.
 */
-(BOOL)LinkActivationEnabled;

/**
 * Sets the interval upon which the device will refresh the display.
 * Note that on single core devices (iPad 1, iPhone 4 and earlier), updating the display will
 * slightly slow down the time to complete rendering.
 *
 * @param seconds - Number of seconds between refreshes.
 */
-(void)SetProgressiveRenderingInterval:(double)seconds;

/**
 * Removes all non-visible portions of document to reduce memory consumption.
 * To be used in response to a low memory warning.
 * slightly slow down the time to complete rendering.
 *
 */
-(void)PurgeCachedMemory;

/**
 * Returns the annotation present at screen coordinates (x, y). If no annotation is present,
 * callling IsValid on the returned annotation will return false.
 * You must lock the doc when retrieving an annotation.
 *
 * @param x - Number of seconds between refreshes.
 * @param y - Number of seconds between refreshes.
 *
 * @return the annotation present at screen coordinates (x, y).
 */
-(Annot*)GetAnnotationAt: (int)x y:  (int)y;

// These methods are called in response to gesture recognizers and are
// included in the header file so that subclasses may extend or override the functionality.

/**
 * Called in response to a single tap gesture and is
 * included here so that subclasses may extend or override the functionality.
 *
 * @param gestureRecognizer - The gesture recognizer that trigger the callback.
 */
-(void)handleTap:(UITapGestureRecognizer *)gestureRecognizer;

/**
 * Called in response to a long press gesture and is
 * included here so that subclasses may extend or override the functionality.
 *
 * @param gestureRecognizer - The gesture recognizer that trigger the callback.
 */
-(void)handleLongPress:(UITapGestureRecognizer *)gestureRecognizer;

/**
 * Used to add extra vertical scroll space to the PDF scroll view (inner scroll view) 
 * when the keyboard would otherwise block the bottom of the document when editing
 * interactive forms.
 */
-(void)setExtraVerticalContent:(int)points;

/**
 *
 *
 */
-(void)customEvent:(id)userData;

/**
 * Places a rectangle over text that has been selected
 *
 * @param color - The color of the drawn rectangle. Set the alpha value to less than 1.0 to give transparency.
 */
-(void)highlightSelectedTextWithColor:(UIColor*)color;

/**
 * Clears the rectangle drawn by highlightSelectedTextWithColor:
 */
-(void)hideSelectedTextHighlights;

/**
 * Locks the PDFDoc instance used by PDFViewCtrl.
 * Simultaneous access to a PDFDoc instance is not allowed. Since
 * PDFViewCtrl renders a PDFDoc in a rendering thread, UI access to the same
 * PDFDoc instance should lock the document first and then unlock
 * 
 * @param cancel_rendering - 
 *            If true, rendering thread is canceled first before trying to
 *            lock the document. This ensures a quick return from this
 *            function. Otherwise, this function can halt the UI and the app
 *            may be unresponsive before the rendering thread finishes. If
 *            the rendering thread is canceled, unlocking the document
 *            UnlockDoc will restart the rendering thread.
 */
-(void)LockDoc:(BOOL)cancelRendering;


/**
 * Tries to lock the PDFDoc instance used by PDFViewCtrl. Returns true if it
 * succeeds and false otherwise.
 */
-(BOOL)TryLockDoc;


/**
 * Unlocks the PDFDoc instance used by PDFViewCtrl and restarts the
 * rendering thread if it was canceled..
 */
-(void) UnlockDoc;

/**
 * Sets the minimum and maximum zoom bounds of PDFViewCtrl.
 *
 * @param min - the minimum zoom bound
 *
 * @param max - the maximum zoom bound
 *
 * @param mode - defines how the zoom bounds are to be used
 *
 * @note if the zoom limits are relative, 1.0 is defined as the zoom level
 * where the document is displayed in page fit mode, where the entire page
 * is visible on screen.
 */
-(void)SetZoomMinimum:(double)min Maxiumum:(double)max Mode:(TrnZoomLimitMode)mode;



//-- Standard PDFView methods



/** 
 * Associates this PDFViewCtrl with a given PDF document. 
 * 
 * @param doc - A document to be displayed in the view.
 *
 */
-(void)SetDoc:(PDFDoc*)doc;

/** 
 * @return Currently associated document with this PDFViewCtrl.
 */
-(PDFDoc*)GetDoc;

/** 
 * Closes the document currently opened in PDFViewCtrl.
 */
-(void)CloseDoc;

/**
 * @return the current page displayed in the view.
 */
-(int)GetCurrentPage;

/**
 * Sets the current page to the given page.
 * 
 * @return true if successful, false otherwise.
 */
-(bool)SetCurrentPage:(int)page_num;

/**
 * Sets the current page to the first page in the document.
 * 
 * @return true if successful, false otherwise.
 */
-(bool)GotoFirstPage;

/**
 * Sets the current page to the last page in the document.
 * 
 * @return true if successful, false otherwise.
 */
-(bool)GotoLastPage;

/**
 * Sets the current page to the next page in the document.
 * 
 * @return true if successful, false otherwise.
 */
-(bool)GotoNextPage;

/**
 * Sets the current page to the previous page in the document.
 * 
 * @return true if successful, false otherwise.
 */
-(bool)GotoPreviousPage;

/**
 * @return the total number of pages in the document.
 */
-(int)GetPageCount;

/**
 * Returns the current zoom factor.
 * 
 * @return current zoom (or scaling) component used to display the page content. 
 */
-(double)GetZoom;

/**
 * Sets the zoom factor to a new value. The function zooms to a point at the 
 * center of the rendering buffer.
 * 
 * @param zoom - new scaling component used to display the page content. 
 * @return true if successful, false otherwise.
 */
-(void)SetZoom:(double)zoom;

/**
 * Sets the zoom factor to a new value using the given pixel coordinate
 * (x,y) as a zoom center.
 * 
 * The zoom point (x,y) is represented in the screen coordinate system, which 
 * starts in the upper-left corner of the client window.
 * 
 * @param x - the horizontal coordinate to zoom in. 
 * @param y - the vertical coordinate to zoom in.
 * @param zoom - new scaling component used to display the page content. 
 * @return true if successful, false otherwise.
 */
-(void)SetZoomX:(int)x Y:(int)y Zoom:(double)zoom;

/**
 * Changes the viewing area to fit a rectangle rect on page page_num.
 * Rectangle must be specified in page coordinates.
 * This will adjust current page and zoom appropriately.
 * 
 * @return true if successful, false otherwise.
 */
-(BOOL)ShowRect: (int)page_num rect:  (PDFRect*)rect;

/**
 * Enables or disables whether links are activated when the user clicks on them 
 * @param enable if true enable link activation, otherwise disable it
 */
-(void)EnableLinkActivation:(BOOL)enable;

/**
 * Enable or disable anti-aliasing. 
 * 
 * Anti-Aliasing is a technique used to improve the visual 
 * quality of images when displaying them on low resolution 
 * devices (for example, low DPI computer monitors).
 * 
 * @default Anti-aliasing is enabled by default.
 */
-(void)SetAntiAliasing:(bool)enabled;

/**
 * Set thin line adjustment parameters.
 * 
 * @param pixel_grid_fit if true (horizontal/vertical) thin lines will be snapped to 
 * integer pixel positions. This helps make thin lines look sharper and clearer. This
 * option is turned off by default.
 *
 * @param stroke_adjust if true auto stroke adjustment is enabled. Currently, this would 
 * make lines with sub-pixel width to be one-pixel wide. This option is turned on by default.
 */
-(void)SetThinLineAdjustmentPixelGrid:(bool)pixel_grid StrokeAdjust:(bool)stroke_adjust;

/**
 * Enable or disable image smoothing. 
 * 
 * The rasterizer allows a trade-off between rendering quality and rendering speed. 
 * This function can be used to indicate the preference between rendering speed and quality.
 *
 * @note image smoothing option has effect only if the source image has higher resolution 
 * that the output resolution of the image on the rasterized page. PDFNet automatically 
 * controls at what resolution/zoom factor, 'image smoothing' needs to take effect. 
 *
 * @param smoothing_enabled True to enable image smoothing, false otherwise.
 * @default image smoothing is enabled.
 */
-(void)SetImageSmoothing:(bool)enabled;

/**
 * Sets the gamma factor used for anti-aliased rendering.
 * 
 * @param exp is the exponent value of gamma function. Typical values 
 * are in the range from 0.1 to 3. 
 *  
 * Gamma correction can be used to improve the quality of anti-aliased
 * image output and can (to some extent) decrease the appearance common 
 * anti-aliasing artifacts (such as pixel width lines between polygons).
 * 
 * @note Gamma correction is used only in the built-in rasterizer.
 */
-(void)SetGamma:(double)exp;

/**
 * Enable or disable annotation and forms rendering. By default, all annotations 
 * and form fields are rendered.
 *
 * @param render_annots True to draw annotations, false otherwise. 
 */
-(void)SetDrawAnnotations:(BOOL)render_annots;

/**
 * Sets the core graphics library used for rasterization and 
 * rendering. Using this method it is possible to quickly switch 
 * between different implementations. By default, PDFNet uses a 
 * built-in, high-quality, and platform independent rasterizer.
 *
 * @param type Rasterizer type. 
 */
-(void)SetRasterizerType: (RasterizerType)type;

/** 
 * Enable or disable support for overprint and overprint simulation. 
 * Overprint is a device dependent feature and the results will vary depending on 
 * the output color space and supported colorants (i.e. CMYK, CMYK+spot, RGB, etc). 
 * 
 * @default By default overprint is only enabled for PDF/X files.
 * 
 * @param op e_op_on: always enabled; e_op_off: always disabled; e_op_pdfx_on: enabled for PDF/X files only.
 */
-(void)SetOverprint: (OverprintPreviewMode)op;

/** 
 * Sets the Optional Content Group (OCG) context that should be used when 
 * viewing the document. This function can be used to change the current 
 * OCG context. Optional content (such as PDF layers) will be selectively 
 * rendered based on the states of optional content groups in the given 
 * context.
 * 
 * @param ctx Optional Content Group (OCG) context, or NULL if the rasterizer
 * should render all content on the page.
 * 
 * @note Unlike PDFDraw.SetOCGContext() this method copies the given context. 
 * As a result, in order to modify the state of OCG groups in the current 
 * context use GetOCGContext() to obtain the currently selected content
 * and then modify states.
 */
-(void)SetOCGContext: (Context*)ctx;

/**
 * @return the Optional Content Group (OCG) context associated with this
 * PDFViewCtrl, or NULL (i.e. context.IsValid()==false) if there is no OCG
 * context associated with the view. If an OCG context associated with the 
 * view, optional content (such as PDF layers) will be selectively rendered 
 * based on the states of optional content groups in the given context.
 */ 
-(Context*)GetOCGContext;

/**
 * Enables of disables caching of images, fonts, and other resources. Disabling caching 
 * can lower memory requirements at the expense of rendering speed.	 
 * 
 * @param enabled if true caching is enabled, if false caching is disabled.
 * @default by default caching is enabled
 */
-(void)SetCaching: (BOOL)enabled;

/**
 * Sets whether the control will render progressively or will just 
 * draw once the entire view has been rendered
 * @param progressive if true the view will be rendered progressively
 */
-(void)SetProgressiveRendering:(BOOL)progressive;

/**
 * Sets the selection mode used for text highlighting.
 * @param tm the text selection mode.
 */
-(void)SetTextSelectionMode: (TextSelectionMode)tm;

/**
 * @return the current page viewing mode
 */
-(TrnPageViewMode)GetPageViewMode;

/**
 * Sets the page viewing mode
 *
 * @param mode - the new page viewing mode.
 * @default The default PageView mode is e_fit_width.
 */
-(void)SetPageViewMode:(TrnPageViewMode)mode;

/**
 * @return the current page presentation mode.
 */
-(TrnPagePresentationMode)GetPagePresentationMode;

/**
 * Sets the current page presentation mode.
 *
 * @param mode - the new page presentation mode.
 * @default The default PagePresentationMode is e_single_continuous.
 */
-(void)SetPagePresentationMode:(TrnPagePresentationMode)mode;

/**
 * Selects text by searching for a given string of text.
 * 
 * @param search_str - 
 * @param match_case - 
 * @param match_whole_word - 
 * @param search_up  - 
 * @param reg_exp  - use regular expressions
 *
 * @note this function runs in asynchronous mode: it launches a search thread and returns immediately.
 * If a matched string is found, the corresponding text is highlighted; otherwise, a message box is
 * shown. Users can use SetCustomEventHandlers() to set a TextFindDoneHandler to customize a different
 * behavior.
 */
-(BOOL)FindText:(NSString*)searchString MatchCase:(BOOL)matchCase MatchWholeWord:(BOOL)matchWholeWord SearchUp:(BOOL)searchUp RegExp:(BOOL)regExp;

/**
 * Rotates all pages in the document 90 degrees clockwise.
 */
-(void)RotateClockwise;

/**
 * Rotates all pages in the document 90 degrees counter-clockwise.
 */
-(void)RotateCounterClockwise;

/**
 * @return  The current rotation of this PDFViewCtrl.
 */
-(Rotate)GetRotation;

/**
 * @return  the number of the page located under the given screen 
 * coordinate. The positive number indicates a valid page, whereas 
 * number less than 1 means that no page was found. 
 */
-(int)GetPageNumberFromScreenPt:(double)x y:(double)y;

/**
 * Converts a point expressed in screen coordinates to a point on canvas.
 */
-(PDFPoint*)ConvScreenPtToCanvasPt:(PDFPoint*)pt;

/**
 * Converts a point expressed in canvas coordinates to a point on screen.
 */
-(PDFPoint*)ConvCanvasPtToScreenPt:(PDFPoint*)pt;

/**
 * Converts a point expressed in canvas coordinates to a point on a given page.
 * 
 * @param page_num the page number for the page used as the origin of the destination 
 * coordinate system. Negative values are used to represent the current page.
 * Pages are indexed starting from one.
 */
-(PDFPoint*)ConvCanvasPtToPagePt:(PDFPoint*)pt page_num:(int)page_num;

/**
 * Converts a point from a coordinate system of a given page to a point on canvas.
 * 
 * @param page_num the page number for the page used as the origin of the destination 
 * coordinate system. Negative values are used to represent the current page.
 * Pages are indexed starting from one.
 */
-(PDFPoint*)ConvPagePtToCanvasPt:(PDFPoint*)pt page_num:(int)page_num;

/**
 * Converts a point expressed in screen coordinates to a point on a given page.
 * 
 * @param page_num the page number for the page used as the origin of the destination 
 * coordinate system. Negative values are used to represent the current page.
 * Pages are indexed starting from one.
 */
-(PDFPoint*)ConvScreenPtToPagePt:(PDFPoint*)pt page_num:(int)page_num;

/**
 * Converts a point from a coordinate system of a given page to a point on screen.
 * 
 * @param page_num the page number for the page used as the origin of the destination 
 * coordinate system. Negative values are used to represent the current page.
 * Pages are indexed starting from one.
 */
-(PDFPoint*)ConvPagePtToScreenPt:(PDFPoint*)pt page_num:(int)page_num;

/**
 * @return the device transformation matrix. The device transformation 
 * matrix maps the page coordinate system to screen (or device) coordinate 
 * system.
 * 
 * @param page_num same as for PDFViewCtrl.Conv???() methods.
 *
 * @note to obtain a transformation matrix that maps screen 
 * coordinates to page coordinates, you can invert the device matrix. 
 * For example: 
 * @code
 *   Common::Matrix2D scr2page(pdfviewctrl.GetDeviceTransform());
 *   scr2page.Inverse();
 * @endcode 
 */
-(Matrix2D*)GetDeviceTransform:(int)page_num;

/**
 * Returns the current canvas width.
 */
-(double)GetCanvasWidth;

/**
 * Returns the current canvas height.
 */
-(double)GetCanvasHeight;

/**
 * @return true if the rendering thread finished rendering the view, 
 * false if the rendering is still in progress.
 */
-(BOOL)IsFinishedRendering;

/**
 * Cancels rendering in progress. If PDFViewCtrl is not busy rendering the page, 
 * the function has no side effects.
 */
-(void)CancelRendering;

/**
 * @return return true if there is selection, false otherwise.
 */
-(BOOL)HasSelection;

/**
 * Remove any text selection.
 */
-(void)ClearSelection;

/**
 * @return Current text selection for a given page.
 * If there is only selection on one page, then page number
 * does not need to be provided.
 */
-(Selection*)GetSelection: (int)pagenum;

/**
 * @return the first page number that has text selection on it.
 * Useful when there are selections on multiple pages at the same time.
 */
-(int)GetSelectionBeginPage;

/**
 * @return the last page number that has text selection on it.
 * Useful when there are selections on multiple pages at the same time.
 */
-(int)GetSelectionEndPage;

/**
 * @return returns true if given page number has any text selection on it.
 * Useful when there are selections on multiple pages at the same time.
 */
-(BOOL)HasSelectionOnPage: (int)ipage;

/**
 * Enables or disables drawing of a thin border around each page.
 * @param border_visible - if true, the border will be visible.
 */
-(void)SetPageBorderVisibility: (BOOL)border_visible;

/**
 * Enables or disables the transparency grid (check board pattern) to reflect 
 * page transparency.
 * @param trans_grid_visible - if true, the grid is turned on.
 */
-(void)SetPageTransparencyGrid: (BOOL)trans_grid_visible;

/**
 * Sets the default 'paper' color used to draw background of each page.
 * @param r, g, b - RGB color specifying the default page color.
 */
-(void)SetDefaultPageColor: (unsigned char)r g:  (unsigned char)g b:  (unsigned char)b;

/**
 * Sets the default background color used to paint the area surrounding each page.
 * @param r, g, b, a - RGBA color specifying the default background color.
 */
-(void)SetBackgroundColor: (unsigned char)r g:  (unsigned char)g b:  (unsigned char)b a:(unsigned char)a; 

/**
 * Sets the horizontal alignment used for rendering pages within the view.
 * @param align an integer specifying the horizontal alignment. Depending 
 * of whether align is positive, negative, or zero - pages will be right, 
 * left or center aligned: 
 *   align<0  -> pages are left aligned.
 *   align==0 -> pages are centered.
 *   align>0  -> pages are right aligned.
 */
-(void)SetHorizontalAlign: (int)align;

/**
 * Sets the vertical alignment used for rendering pages within the view.
 * @param align an integer specifying the vertical alignment. Depending 
 * of whether align is positive, negative, or zero - pages will be bottom, 
 * top or center aligned: 
 *   align<0  -> pages are top aligned.
 *   align==0 -> pages are centered.
 *   align>0  -> pages are bottom aligned.
 */
-(void)SetVerticalAlign: (int)align;

/**
 * Sets the vertical and horizontal padding and column spacing between adjacent pages in the view. 
 * 
 * 
 * @param horiz_col_space horizontal column spacing (represented in pixels) between 
 * adjacent pages in the view. Default is 10.
 * @param vert_col_space vertical column spacing (represented in pixels) between adjacent 
 * pages in the view. Default is 10.
 * @param horiz_pad horizontal padding (represented in pixels) on the left and right side 
 * of the view. Default is 10.
 * @param vert_pad vertical padding (represented in pixels) on the top and bottom side 
 * of the view. Default is 10.
 */
-(void)SetPageSpacing: (int)horiz_col_space vert_col_space:  (int)vert_col_space horiz_pad:  (int)horiz_pad vert_pad:  (int)vert_pad;

/**
 * Redraws the contents of the buffer.
 */
-(void)Update;

/**
 * Redraws the area covered with a given annotation.
 * @param annot The annotation to update.
 * @param page_num The page number on which the annotation is located.
 */
-(void)UpdateWithAnnot: (Annot*)annot page_num: (int)page_num;

/**
 * Redraws the given area in the buffer.
 * @param update The rectangle to update expressed in screen coordinates.
 */
-(void)UpdateWithRect: (PDFRect*)update;


/**
 * Renders content that requires rendering. May be called to resume rendering after CancelRendering.
 */
-(void)RequestRendering;

/**
 * Selects text within the given region using the current text selection mode.
 * 
 * @return true if some text was selected, false otherwise.
 * @param x1, y1, x2, y2 - two points (screen coordinates, origin located at the upper-left corner of this view)
 * defining the opposite corners of a selection rectangle.
 */
-(BOOL)SelectX1:(double)x1 Y1:(double)y1 X2:(double)x2 Y2:(double)y2;

/**
 * Selects text using structural mode.
 * 
 * @return true if some texts were selected, false otherwise.
 * @param (x1, y1), page1 - the first selection point (in page coordinates space) on page page1
 * @param (x2, y2), page2 - the second selection point (in page coordinates space) on page page2
 */
-(BOOL)SelectX1:(double)x1 Y1:(double)y1 PageNumber1:(int)pageNumber1 X2:(double)x2 Y2:(double)y2 PageNumber2:(int)pageNumber2;

/**
 * Selects text within the given region using the current text selection mode.
 * 
 * @return true if some text was selected, false otherwise.
 * @param x1, y1, x2, y2 - two points (screen coordinates, origin located at the upper-left corner of this view)
 * defining the opposite corners of a selection rectangle.
 */
-(BOOL)SelectWithTextSelect: (double)x1 y1:  (double)y1 x2:  (double)x2 y2:  (double)y2;

/**
 * Selects texts using structural mode.
 * 
 * @return true if some texts were selected, false otherwise.
 * @param (x1, y1), page1 - the first selection point (in page coordinates space) on page page1
 * @param (x2, y2), page2 - the second selection point (in page coordinates space) on page page2
 */
-(BOOL)SelectWithStructure: (double)x1 y1:  (double)y1 page1:  (int)page1 x2:  (double)x2 y2:  (double)y2 page2:  (int)page2;

/**
 * Selects texts identified by Highlights.
 * 
 * @return true if some texts were selected, false otherwise.
 * @param highlights - an instance of Highlights class.
 */
-(BOOL)SelectWithHighlights: (Highlights*)highlights;

/**
 * Selects all text on the page.
 */
-(void)SelectAll;

/**
 * @return the current horizontal scroll position. The returned value is expressed in the 
 * canvas coordinate system. The canvas coordinate system is defined by a bounding box that 
 * surrounds all pages in the view.
 */
-(double)GetHScrollPos;

/**
 * @return the current vertical scroll position. The returned value is expressed in the 
 * canvas coordinate system. The canvas coordinate system is defined by a bounding box that 
 * surrounds all pages in the view.
 */
-(double)GetVScrollPos;

/**
 * Sets the horizontal scroll position.
 * 
 * @param the new horizontal scroll position. The position should be a number in the range
 * between 0 and GetCanvasWidth(). The 'pos' parameter is expressed in the canvas coordinate system. 
 * The canvas coordinate system is defined by a bounding box that surrounds all pages in the view.
 * If Animated is not specified, the default value is YES.
 */
-(void)SetHScrollPos: (double)pos;
-(void)SetHScrollPos: (double)pos Animated:(BOOL)animated;

/**
 * Sets the vertical scroll position.
 * 
 * @param the new vertical scroll position. The position should be a number in the range
 * between 0 and GetCanvasheight().  The 'pos' parameter is expressed in the canvas coordinate system. 
 * The canvas coordinate system is defined by a bounding box that surrounds all pages in the view.
 * If Animated is not specified, the default value is YES.
 */
-(void)SetVScrollPos: (double)pos;
-(void)SetVScrollPos: (double)pos Animated:(BOOL)animated;

/**
 * Updates the page layout withing the view. This function must be called  
 * after document page sequence is modified (such as when a page is being 
 * added to or removed from a document) or after changes to page dimensions
 * (e.g. after a page is rotated or resized).
 */
-(void)UpdatePageLayout;

/**
 * Requests action object to be executed by PDFViewCtrl. Action must belong to the document 
 * currently displayed in PDFViewCtrl.
 * @param action object that is to be executed.
 */
-(void)ExecuteAction: (Action*)action;

/**
 * Post a message to cause onCustomEvent:userDatato be called in the
 * next main message loop. This is useful if a Tool needs to be called by
 * PDFViewCtrl in the main message loop without a touch event being
 * triggered.
 *
 * @param userData An object that can be passed back in the onCustomEvent:(id)userData function.
 */
-(void)postCustomEvent:(id)userData;

/**
 * Enable or disable highlighting form fields. Default is disabled.
 *
 * @param highlight_fields YES to highlight, NO otherwise. 
 */
-(void)SetHighlightFields:(BOOL)highlight_fields;

/**
 * Enable or disable path hinting.
 * 
 * @param enabled if YES path hinting is enabled. Path hinting is used to slightly
 * adjust paths in order to avoid or alleviate artifacts of hair line cracks between
 * certain graphical elements. This option is turned on by default.
 *
 */
-(void)SetPathHinting:(BOOL)enabled;


/**
 * Set the suggested memory size of the rendered content.
 * 
 * <p>
 * PDFViewCtrl keeps off-screen content in order to achieve better viewing
 * experience; however, this increases memory usage. By default, PDFViewCtrl
 * will use 80% of available memory to render on-screen and off-screen content.
 * </p>
 * 
 * @param allowed_max the allowed heap memory usage in MB.
 * 
 * @note <p>
 *       If you want to minimize memory usage at the cost of
 *       viewing experience quality, you can set size_in_mb to 0 and
 *       PDFViewCtrl will not keep any off-screen content. Also note that
 *       this function only controls the memory used to store rendered content
 *       not the entire memory footprint of the control.
 *       </p>
 */
-(void)SetContentBufferSize:(long)allowedMax;

/**
 * Sets the PDF document from a URL. If the PDF is linearized, PDFViewCtrl will
 * display it incrementally while downloading it. If the PDF is not linearized,
 * it will display blank pages until the entire document is downloaded.
 *
 * @param url The URL of the document to open.
 *
 * @param password The password used to open the PDF document if it is password
 * protected.
 *
 * @param fullPath The path to a file that will be used to cache the downloaded file which
 * will be used to resume a download. If no cache file is specified, a file is created in 
 * the temporary directory.
 *
 * @note Currently, only the HTTP protocol is supported.
 */
-(void)OpenUrl:(NSString*)url WithPDFPassword:(NSString*)password;
-(void)OpenUrl:(NSString*)url WithPDFPassword:(NSString*)password WithCacheFile:(NSString*)fullPath;

@end


@protocol PDFViewCtrlDelegate<NSObject>

@optional

- (void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber;
- (void)textSearchStart;
- (void)textSearchResult:(NSMutableArray*) selectionQuads;
- (void)toolChanged:(PDFViewCtrl*)pdfViewCtrl; // notification for when tool changes (e.g. to update a toolbar)
- (void)downloadEventType:(DownloadedType)type pageNumber:(int)pageNum pageCount:(int)pageCount;
- (void)downloadError:(NSException*)exception;

// PDF scroll view delegates //

- (void)pdfScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)pdfScrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)pdfScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (BOOL)pdfScrollViewShouldScrollToTop:(UIScrollView *)scrollView; // will respect result returned by delegate
- (void)pdfScrollViewDidScrollToTop:(UIScrollView *)scrollView;
- (void)pdfScrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
- (void)pdfScrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)pdfScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view;
- (void)pdfScrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
- (void)pdfScrollViewDidZoom:(UIScrollView *)scrollView;
- (void)pdfScrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;
- (void)pdfScrollViewTap:(UITapGestureRecognizer *)gestureRecognizer;
- (void)pdfScrollViewLongPress:(UITapGestureRecognizer *)gestureRecognizer;

// Outer scroll view delegates - holds PDF scroll view when in single page mode. //

- (void)outerScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (BOOL)outerScrollViewShouldScrollToTop:(UIScrollView *)scrollView; // will respect result returned by delegate unless tool also responds
- (void)outerScrollViewDidScrollToTop:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view;
- (void)outerScrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
- (void)outerScrollViewDidZoom:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;

// Custom Event delegate //
- (void)customEvent:(id)userData;

@end


// tool must conform to this protocol.
@protocol ToolDelegate<NSObject>

@required
- (id)initWithPDFViewCtrl:(PDFViewCtrl*)in_pdfViewCtrl;
- (UIView<ToolDelegate>*)getNewTool;

@optional

// touch gestures (on pdfScrollView)
- (BOOL)onTouchesBegan:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesMoved:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesEnded:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)onTouchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event;
- (BOOL)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer;
- (BOOL)handleTap:(UITapGestureRecognizer *)gestureRecognizer;
- (BOOL)touchesShouldCancelInContentView:(UIView *)view;
- (BOOL)touchesShouldBegin:(NSSet *)touches withEvent:(UIEvent *)event inContentView:(UIView *)view;

// scroll view callbacks (for pdfScrollView)
- (void)scrollViewDidScroll:(UIScrollView *)scrollView;
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView; // will respect result returned by tool before asking pdfViewCtrlDelegate
- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView;
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view;
- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
- (void)scrollViewDidZoom:(UIScrollView *)scrollView;
- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;

// Outer scroll view delegates - holds scrollView when in single page mode. //

- (void)outerScrollViewDidScroll:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginDragging:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate;
- (BOOL)outerScrollViewShouldScrollToTop:(UIScrollView *)scrollView; // will respect result returned by delegate unless tool also responds
- (void)outerScrollViewDidScrollToTop:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginDecelerating:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndDecelerating:(UIScrollView *)scrollView;
- (void)outerScrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(UIView *)view;
- (void)outerScrollViewDidEndZooming:(UIScrollView *)scrollView withView:(UIView *)view atScale:(float)scale;
- (void)outerScrollViewDidZoom:(UIScrollView *)scrollView;
- (void)outerScrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView;

// other
- (void)onLayoutChanged; // chance for tool to reorganize layout in response to page layout changes (such as cover to cover facing)
- (void)pageNumberChangedFrom:(int)oldPageNumber To:(int)newPageNumber; // notification when page number changes
- (BOOL)onCustomEvent:(id)userData; //to allow a tool to change to different tool in response to a custom event, triggered by calling postCustomEvent on a PDFViewCtrl
- (void)onSetDoc; // called when the doc is changed in PDFViewCtrl

@end



