//
//  T0Command.h
//  iSmartSDK
//
//  Created by Chris Powell on 11/3/11.
//  Copyright (c) 2011 CHARGE Anywhere LLC. All rights reserved.
//

#import "ISO7816Command.h"

@interface T0Command : ISO7816Command{
  unsigned char classByte;
  APDUInstruction instructionByte;
  unsigned char parameter1Byte;
  unsigned char parameter2Byte;
  unsigned char lengthByte;
  unsigned char expectedResponseLengthByte;
  NSData *dataBytes;
  unsigned char status1Byte;
  unsigned char status2Byte;
}

@property(nonatomic, assign) unsigned char classByte;
@property(nonatomic, assign) APDUInstruction instructionByte;
@property(nonatomic, assign) unsigned char parameter1Byte;
@property(nonatomic, assign) unsigned char parameter2Byte;
@property(nonatomic, assign) unsigned char lengthByte;
@property(nonatomic, assign) unsigned char expectedResponseLengthByte;
@property(nonatomic, retain) NSData *dataBytes;
@property(nonatomic, readonly) unsigned char status1Byte;
@property(nonatomic, readonly) unsigned char status2Byte;

@end
