//
//  SAMError.h
//  SecureBrowser
//
//  Created by Olivier Michiels on 06/08/12.
//
//

#import <Foundation/Foundation.h>
#import "card.h"

@class Response;
@interface SAMError : NSError
@property(nonatomic) LONG rv;

-(id)initWithResponse:(Response*)response;
-(id)initWithResponse:(Response *)response andCode:(NSInteger)code;
@end
