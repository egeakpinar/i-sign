//
//  SAMUtil.h
//  SecureBrowser
//
//  Created by Olivier Michiels on 21/08/12.
//
//

#import <Foundation/Foundation.h>
#import <ExternalAccessory/ExternalAccessory.h>

@interface SAMUtil : NSObject
+ (NSString*)bytes2String:(const unsigned char*)bytes length:(NSInteger)length;
+(BOOL)isReaderSupported:(EAAccessory*)accessory readers:(NSArray*)readers;
+ (NSString *) string_trimmed:(NSString *)input;@end
