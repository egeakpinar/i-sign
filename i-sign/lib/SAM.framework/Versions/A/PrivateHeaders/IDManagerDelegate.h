//
//  IDManagerDelegate.h
//  SecureBrowser
//
//  Created by Olivier Michiels on 17/10/12.
//
//

#import <Foundation/Foundation.h>

@protocol IDManagerDelegate <NSObject>
@optional
-(void)performSelector:(SEL)selector withDictionary:(NSDictionary*)dict;
@end
