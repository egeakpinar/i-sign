//
//  IDManager.h
//  SAM Framework
//
//  Created by Olivier Michiels on 07/08/12.
//
//

#import <Foundation/Foundation.h>
#import "card.h"
#include "winscard.h"
#include "wintypes.h"
#import "SAMError.h"
#import "IDManagerDelegate.h"
#import "IDCodes.h"

static NSString *MessageKey = @"messageKey";
static NSString *StepKey = @"stepKey";
static NSString *StatusKey = @"card_status";
static NSString *TimeOutKey = @"timeOutKey";
static NSString *ReaderClassNameKey = @"readerClassNameKey";
static NSString *IsDemoKey = @"isDemoKey";
static NSString *IsPairingRequiredKey = @"isPairingRequired";

@interface IDManager : NSObject
@property(nonatomic, strong, readonly) NSString* atr;
@property(nonatomic, getter = isReaderPresent, readonly) BOOL readerPresent;
@property(nonatomic, getter = isCardPresent, readonly) BOOL cardPresent;
@property(nonatomic, strong, readonly) NSString* currentReader;
@property(nonatomic, getter = getCardStatus) ID_CODE cardStatus;
@property(nonatomic, readonly, getter = getReaderStatus) ID_CODE readerStatus;
+(void)setLicenseOk:(BOOL)licenseOk;

+(IDManager*)sharedManagerWithReaders:(NSArray*)readers andCards:(NSArray*)cards andParameters:(NSDictionary*)parameters error:(SAMError**)error;

+(IDManager*)sharedManager;
-(void)shutdown;
-(void)update;
-(NSData*)getPhoto:(NSString*)pin error:(SAMError**)error;
-(NSDictionary*)getData:(NSString*)pin error:(SAMError**)error;
-(BOOL)isDataSecured:(SAMError**)error;
-(NSData*)sign:(NSString*)pin pinStatus:(NSInteger*)pinStatus certificateIndex:(NSInteger)certificateIndex toSign:(NSData*)toSign error:(SAMError**)error;
-(NSData*)getCertificate:(NSInteger)type error:(SAMError**)error;
-(NSUInteger) getReaderStatus;
-(NSInteger)verifyPin:(NSString*)pin error:(SAMError**)error;
-(void*)getContext;
-(BOOL)isPinAlpha;
-(NSArray*)certificatesList:(SAMError**)error;
-(NSArray*)rootCertificatesList:(SAMError**)error;
-(NSArray*)getIdentityFields:(SAMError**)error;
-(BOOL)needsPairing;
-(void)prepareForPairing;
-(void)pairingDone;
-(void)updateParameterValue:(id)value forKey:(NSString*)key;
@end
