//
//  IDCodes.h
//
//  Created by i-dentity on 03/04/2013
//  Version 1.0
//

#ifndef checkid_IDCodes_h
#define checkid_IDCodes_h

typedef long ID_CODE;


#define ID_OK                           0x0000  // Added in this version
#define ID_ERROR                        0x0001  // Added in this version
#define ID_READER_PRESENT               0x1000
#define ID_READER_ABSENT                0x1001
#define ID_READER_TRYING                0x1002  // Added in this version
#define ID_READER_ACCESS_SUCCESSFUL     0x1003  // Added in this version
#define ID_READER_CONTEXT_ERROR         0x1004  // Added in this version
#define ID_CARD_DID_READ                0x0080
#define ID_START_PROGRESS               0x0081
#define ID_STOP_PROGRESS                0x0082
#define ID_MOVE_PROGRESS                0x0083
#define ID_ERROR_MESSAGE                0x0084
#define ID_MESSAGE                      0x0085
#define ID_ASK_MESSAGE                  0x0086

#define ID_INVALID_LICENSE              13100
#define ID_READER_NOT_VALID             13101
#define ID_READER_PROBLEM               13102

#define ID_CARD_ABSENT                  13200   // Added in this version
#define ID_CARD_PRESENT                 13300   // Added in this version
#define ID_CARD_NOT_VALID               13201
#define ID_CARD_CONNECT_ERROR           13202
#define ID_CARD_OPERATION_ERROR         13203
#define ID_CARD_INVALID_ATR             13204
#define ID_CARD_GET_CERTIFICATE_ERROR   13205
#define ID_CARD_KEY_SELECTION_ERROR     13206
#define ID_CARD_VERIFY_PIN_ERROR        13207
#define ID_CARD_SIGNATURE_ERROR         13208
#define ID_CARD_GET_SIGN_DATA_ERROR     13209
#define ID_CARD_GET_PHOTO_ERROR         13210
#define ID_CARD_GET_IDENTITY_ERROR      13211
#define ID_CARD_GET_ADDRESS_ERROR       13212
#define ID_CARD_GET_CA_CERTIFICATE_ERROR 13213

#define ID_CARD_TRYING                  13301   // Added in this version
#define ID_CARD_ACCESS_SUCCESSFUL       13302   // Added in this version


#endif
